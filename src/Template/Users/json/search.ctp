<?php
	$output_array = [];
	foreach($users as $user){
		array_push($output_array, array(
			'label' => $user->full_name,
			'value' => $user->id,
			'category' => ucwords($user->role->name),
			'url' => $this->Url->build(['controller' => 'users', 'action' => 'view', $user->id], true)
		));
	}
	echo json_encode($output_array);
?>