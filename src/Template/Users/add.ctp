

<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="container">
<div class="students form large-9 medium-8 columns content">
<h2>Add User</h2>
		<?= $this->Form->create($user) ?>
		<fieldset>
		
			<?php
			echo $this->Form->control('role_id');
			echo $this->Form->control('first_name',['type' => 'text', 'placeholder' => 'John']);
			echo $this->Form->control('last_name', ['type' => 'text', 'placeholder' => 'Smith']);
			echo $this->Form->control('user_email', ['type' => 'email', 'placeholder' => 'JohnSmith@gmail.com']);
			echo $this->Form->control('password', ['type' => 'password']);
			echo $this->Form->control('cPassword', ['minLength' => 6,'placeholder' => 'Confirm Password must match your password', 'label' => 'Confirm Password', 'type' => 'password']);
			echo $this->Form->control('phone_number', ['type' => 'tel', 'placeholder' => 'eg. 0400 000 123']);
			echo $this->Form->control('notes', ['placeholder' => 'Other Requirements or Concerns']);
			?>
		</fieldset>
		 <?php echo $this->Form->button(__('Add'), array('class' => 'btn btn-light')); ?>
		<button type="button" class = 'btn btn-light' onclick="goBack()">Go Back</button>

<script>
function goBack() {
    window.history.back();
}
</script>
		<?= $this->Form->end() ?>
	</div>

</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js" defer="defer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" />
