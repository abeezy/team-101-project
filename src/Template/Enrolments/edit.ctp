<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="container">
<nav class="large-3 medium-4 columns" id="actions-sidebar">
   
</nav>
<div class="row justify-content-center">
		<div class="col-md-4">

    <?= $this->Form->create($enrolment) ?>
    <fieldset>
        <legend><?= __('Edit Enrolment') ?></legend>
        <?php
            echo $this->Form->control('student_id', ['options' => $students]);
            echo $this->Form->control('class_id', ['options' => $classes]);
           	echo $this->Form->input('enrolment_date', ['class' => 'datepicker-input', 'data-date-start-date' => '-0d', 'type' => 'text', 'format' => 'Y-m-d', 'default' => date('Y-m-d'), 'value' => !empty($enrolment->enrolment_date) ? $enrolment->enrolment_date->format('Y-m-d') : date('Y-m-d')]); 

            echo $this->Form->control('no_of_weeks_enrolled');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js" defer="defer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" />
