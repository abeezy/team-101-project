<div class="container">
	<h1>Select a Student</h2>
	<?php echo $this->Form->create(); ?>
		<div class="card-deck">
			<?php foreach($parent->student_contact as $sc): ?>
				<div class="card">
					<div class="card-body">
						<h4 class="card-title"><?php echo $sc->student->full_name; ?></h4>
						<h6 class="card-subtitle mb-2 text-muted"><?php echo $sc->student->grade->name; ?><!-- at <?php //echo $sc->student->enrolments[0]->class->school->name; ?>--></h6>
						<p class="card-text"><?php echo $sc->student->notes; ?></p>
						<button type="submit" name="student_id" value="<?php echo $sc->student->id; ?>" class="card-link">Select</button>
					</div>
				</div>
			<?php endforeach; ?>
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">New Student</h4>
					<?php echo $this->Html->link('Add New Student', array('controller' => 'Students', 'action' => 'studentEnrolment'), array('class' => 'card-link')); ?>
				</div>
			</div>
		</div>
		
	<?php echo $this->Form->end(); ?>
<br>
<button onclick="goBack()">Go Back</button>

<script>
function goBack() {
    window.history.back();
}
</script>
</div>