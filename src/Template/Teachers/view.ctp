<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Teacher $teacher
  */
?>

<div class="container">
<button onclick="goBack()">Go Back</button>

<script>
function goBack() {
    window.history.back();
}
</script>
<div class="teachers view large-9 medium-8 columns content">
    <h3>Teacher Details</h3>
   <table class="table table-responsive table-striped bg-white">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= $teacher->has('user') ? $this->Html->link($teacher->user->first_name . ' ' . $teacher->user->last_name, ['controller' => 'Users', 'action' => 'view', $teacher->user->id]) : '' ?></td>
        </tr>
  			<tr>
  				<th scope="row"><?= __('Email') ?></th>
  				<td><?= h($teacher->user->user_email) ?></td>
  			</tr>
  			<tr>
  				<th scope="row"><?= __('Phone Number') ?></th>
  				<td><?= h($teacher->user->phone_number) ?></td>
  			</tr>
    </table>
    <div class="related">
        <h4><?= __('Classes') ?></h4>
        <?php if (!empty($teacher->classes)): ?>
     <table class="table table-responsive table-striped bg-white">
            <tr>
                <th scope="col"><?= __('School') ?></th>
                <th scope="col"><?= __('Class') ?></th>
                <th scope="col"><?= __('Class Day') ?></th>
                <th scope="col"><?= __('Class Time') ?></th>
                <th scope="col"><?= __('Class Term') ?></th>
                <th scope="col"><?= __('Start Date') ?></th>
                <th scope="col"><?= __('End Date') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($teacher->classes as $classes): ?>
            <tr>
                <td><?= h($classes->school_id) ?></td>
                <td><?= h($classes->name) ?></td>
                <td><?= h($classes->class_day) ?></td>
                <td><?= h($classes->class_time) ?></td>
                <td><?= h($classes->class_term) ?></td>
                <td><?= h($classes->start_date) ?></td>
                <td><?= h($classes->end_date) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Classes', 'action' => 'view', $classes->id]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Lessons') ?></h4>
        <?php if (!empty($teacher->lessons)): ?>
     <table class="table table-responsive table-striped bg-white">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Class Id') ?></th>
                <th scope="col"><?= __('Teacher Id') ?></th>
                <th scope="col"><?= __('Lesson Date') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($teacher->lessons as $lessons): ?>
            <tr>
                <td><?= h($lessons->id) ?></td>
                <td><?= h($lessons->class_id) ?></td>
                <td><?= h($lessons->teacher_id) ?></td>
                <td><?= h($lessons->lesson_date) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Lessons', 'action' => 'view', $lessons->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Lessons', 'action' => 'edit', $lessons->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Lessons', 'action' => 'delete', $lessons->id], ['confirm' => __('Are you sure you want to delete # {0}?', $lessons->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
		<div class="teachers view large-9 medium-8 columns content">
    </div>
    </div>
</div>
</div>
