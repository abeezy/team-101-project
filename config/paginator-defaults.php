<?php

	// Controls the default form templates - in this case, we're going to default the forms to Bootstrap's standard

	return [
		'prevActive' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>',
		'prevDisabled' => '<li class="page-item disabled"><a class="page-link" href="{{url}}">{{text}}</a></li>',
		'number' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>',
		'nextActive' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>',
		'nextDisabled' => '<li class="page-item disabled"><a class="page-link" href="{{url}}">{{text}}</a></li>',
		'current' => '<li class="page-item active"><a class="page-link" href="{{url}}">{{text}}</a></li>',
  ];

?>
