<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="container">

<div class="students form large-9 medium-8 columns content">
<h2>Add Class</h2>
    <?= $this->Form->create($class) ?>
    <fieldset>
        <?php
            echo $this->Form->control('school_id', ['options' => $schools]);
            echo $this->Form->control('class_term', ['options' => ['1' => '1', '2' => '2', '3' => '3', '4' => '4'],'label' => 'Term']);
            echo $this->Form->control('name');
			echo $this->Form->control('teacher_id', ['options' => $teachers, 'label' => 'Teacher']);
            echo $this->Form->input('start_date', ['class' => 'datepicker-input', 'data-date-start-date' => '-0d', 'type' => 'text', 'format' => 'Y-m-d', 'default' => date('Y-m-d'), 'value' => !empty($enrolment->enrolment_date) ? $enrolment->enrolment_date->format('Y-m-d') : date('Y-m-d')]);
		    echo $this->Form->input('end_date', ['class' => 'datepicker-input', 'data-date-start-date' => '-0d', 'type' => 'text', 'format' => 'Y-m-d', 'default' => date('Y-m-d'), 'value' => !empty($enrolment->enrolment_date) ? $enrolment->enrolment_date->format('Y-m-d') : date('Y-m-d')]);
            echo $this->Form->control('class_time', array('label' => 'Time','timeFormat' => 12, 'hour' => array('class' => 'form-control'), 'minute' => array('class' => 'form-control'), 'interval' => 5, 'meridian' => array('class' => 'form-control')));
            echo $this->Form->control('class_day', ['options' => ['Monday' => 'Monday', 'Tuesday' => 'Tuesday', 'Wednesday' => 'Wednesday', 'Thursday' => 'Thursday', 'Friday' => 'Friday'],'label' => 'Day']);
            echo $this->Form->control('class_address', ['label' => 'Class Location']);
			      echo $this->Form->control('price');

        ?>
    </fieldset>
				<?php echo $this->Form->button(__('Add'), array('class' => 'btn btn-light')); ?>
						<button type="button" class = 'btn btn-light' onclick="goBack()">Go Back</button>
                     
<script>
function goBack() {
    window.history.back();
}
</script>

    <?= $this->Form->end() ?>


</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js" defer="defer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" />
