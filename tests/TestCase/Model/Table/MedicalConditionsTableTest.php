<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MedicalConditionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MedicalConditionsTable Test Case
 */
class MedicalConditionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MedicalConditionsTable
     */
    public $MedicalConditions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.medical_conditions',
        'app.students',
        'app.schools',
        'app.classes',
        'app.teachers',
        'app.users',
        'app.roles',
        'app.student_contact',
        'app.contact_type',
        'app.lessons',
        'app.lesson_enrolment',
        'app.enrolments',
        'app.payment_history',
        'app.grades'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('MedicalConditions') ? [] : ['className' => MedicalConditionsTable::class];
        $this->MedicalConditions = TableRegistry::get('MedicalConditions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MedicalConditions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
