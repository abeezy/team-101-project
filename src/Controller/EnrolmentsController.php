<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\I18n\Date;
use Cake\Routing\Router;


/**
 * Enrolments Controller
 *
 * @property \App\Model\Table\EnrolmentsTable $Enrolments
 *
 * @method \App\Model\Entity\Enrolment[] paginate($object = null, array $settings = [])
 */
class EnrolmentsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Students', 'Classes']
        ];
        $enrolments = $this->paginate($this->Enrolments);

        $this->set(compact('enrolments'));
        $this->set('_serialize', ['enrolments']);
    }

    /**
     * View method
     *
     * @param string|null $id Enrolment id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $enrolment = $this->Enrolments->get($id, [
            'contain' => ['Students', 'Classes', 'PaymentHistoryEnrolment'=>['PaymentHistory']]
        ]);

        $this->set('enrolment', $enrolment);
        $this->set('_serialize', ['enrolment']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */

    public function add($student_id = null)
    {
        $enrolment = $this->Enrolments->newEntity();
        if ($this->request->is('post')) {
            $enrolment = $this->Enrolments->patchEntity($enrolment, $this->request->getData());
            if ($this->Enrolments->save($enrolment)) {
                $this->Flash->success(__('The enrolment has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The enrolment could not be saved. Please, try again.'));
        }
        $students = $this->Enrolments->Students->find('list', ['limit' => 200]);
        $classes = $this->Enrolments->Classes->find('list', ['limit' => 200]);

        $this->set(compact('enrolment', 'students', 'classes', 'student_id'));
        $this->set('_serialize', ['enrolment']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Enrolment id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $enrolment = $this->Enrolments->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $enrolment = $this->Enrolments->patchEntity($enrolment, $this->request->getData());
            if ($this->Enrolments->save($enrolment)) {
                $this->Flash->success(__('Enrolment successfully updated.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The enrolment could not be updated. Please, try again.'));
        }
        $students = $this->Enrolments->Students->find('list', ['limit' => 200]);
        $classes = $this->Enrolments->Classes->find('list', ['limit' => 200]);

        $this->set(compact('enrolment', 'students', 'classes'));
        $this->set('_serialize', ['enrolment']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Enrolment id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $enrolment = $this->Enrolments->get($id);
        if ($this->Enrolments->delete($enrolment)) {
            $this->Flash->success(__('The enrolment has been deleted.'));
        } else {
            $this->Flash->error(__('The enrolment could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

	/*
    // student enrol function
    public function enrol($class_id = null) {
          $enrolment = $this->Enrolments->newEntity();
  		if ($class_id !== null) {
  			$enrolment['class_id'] = $class_id;
  		} else {
  			$enrolment['class_id'] = $this->request->session()->consume("class_id");
  		}

//        $students = $this->Enrolments->Students->find('list', [
//            'valueField' => function ($student) {
//                return $student->get('full_name');
//            },
//            'limit' => 200
//        ]);
        $classes = $this->Enrolments->Classes->find('list', ['limit' => 200]);
		$weeks = $this->Enrolments->Classes->find('list', [
			'valueField' => function ($class) {
				return $class->get('class_term');
			},
			'limit' => 200
		]);
        if ($this->request->is('post')) {
	        $student_id = $this->request->session()->consume("student_id");

            $enrolment = $this->Enrolments->patchEntity($enrolment, $this->request->getData());
			$enrolment->student_id=$student_id;

            $enrolment->enrolment_date = new Date($enrolment->enrolment_date);

            if ($this->Enrolments->save($enrolment)) {

				$this->loadModel('Lessons');
				$this->loadModel('LessonEnrolment');

				for($i = 0; $i < $this->request->getData('no_of_weeks_enrolled'); $i++) {

					// Get the next lesson date after the start of the user's enrolment, then each week afterwards using the loop
					$lesson = $this->Lessons->find('all', array(
						'conditions' => array(
							'class_id' => $enrolment->class_id,
							'lesson_date <=' => $enrolment->enrolment_date->modify('+' . (7 * $i) . ' days'),
							'lesson_date >' => $enrolment->enrolment_date->modify('+' . (7 * ($i-1)) . ' days'),
						),
						'order' => array(
							'Lessons.lesson_date' => 'ASC'
						)
					))->first();

				  if ($lesson) {
					$le = $this->LessonEnrolment->newEntity();
					$le->lesson_id = $lesson->id;
					$le->enrolment_id = $enrolment->id;
					$this->LessonEnrolment->save($le);
					}

				}
				$enrolment_details = $this->Enrolments->get($enrolment->id, array(
					'contain' => array(
						'Classes' => array(
							'Schools'
						)
					)
				));

				$critical = false;
				$list_id = '75f7313bbd';
				$school_categories = '096357b049';
				$school_name = $enrolment_details['class']['school']['school_name'];


				$segment = $this->mailchimp_create_segment($critical, $list_id, $school_categories, $school_name);


                $this->Flash->success(__('The enrolment has been saved.'));

                //return $this->redirect(['controller' => 'pages', 'action' => 'display', 'payment']);

				$this->request->session()->delete("student_id");
				$this->request->session()->delete("claas_id");


                // return $this->redirect(['controller' => 'pages', 'action' => 'display', 'payment']);
				//return $this->redirect(['controller' => 'users', 'action' => 'display', 'parent']);
                if ($this->request->getData('submitButton') == "enrolNow") {
                  return $this->redirect(['controller' => 'users', 'action' => 'parent']);
                }
                else if($this->request->getData('submitButton') == "addAnother") {
                  return $this->redirect(['controller' => 'classes', 'action' => 'all']);
                }

            }

				// echo "<pre>";
				 // print_r($enrolment);
				 // echo "</pre>";

				 // exit;
            $this->Flash->error(__('The enrolment could not be saved. Please, try again.'));;

            }

        $this->set(compact('enrolment', 'student_id', 'classes'));
        $this->set('_serialize', ['enrolment']);


    }
	*/
	
	//Function called when a parent reenrolls their child through the parent dashboard. 
	public function reenroll($id = null) {
    $this->loadModel('StudentContact');

    $studentContact = $this->StudentContact->find('all', array(
      'conditions' => array(
        'student_id' => $id,
        'user_id' => $this->Auth->user('id')
      )
    ))->first(); //find's the related parent of the student being teenrolled

		if($id !== null && $studentContact !== null){
			$this->loadModel('Students');
			$student = $this->Students->get($id, [
				'contain' => []
			]);
			$grades = $this->Students->Grades->find('list', ['limit' => 200]);

			$old_enrolment = $this->Enrolments->find('all', array(
				'contain' => array(
					'Classes'
				),
				'conditions' => array(
					'student_id' => $id
				),
				'order' => array(
					'enrolment_date' => 'DESC'
				)
			))->first();

			$this->loadModel('Classes');
			$classes = $this->Classes->find('list', [
				'conditions' => [
					'school_id' => $old_enrolment['class']['school_id']
				],
				'contain' => [
					'lessons'
				],
				'limit' => 200
			]); //queries through classes->enrolments to find their past enrolment 

			//cart code has fields item which can be multiple, a total price and a discount rate.
			//grabs the student, the class which they selected and the number of lessons they chose (only if the lesson starts after the time they are enrolling)
			
			if ($this->request->is(['patch', 'post', 'put'])) {
				$student_data = $this->request->getData();

				if($this->request->session()->check('cart')) {
					$cart_data = $this->request->session()->read('cart');
				} else {
					$cart_data = array(
						'items' => array(),
						'total_price' => 0,
						'discountMultiplier' => 1
					);
				}
				$totalPrice = 0;

				$students = array();

				$cart_item = array();
				$cart_item['Student'] = $student;
				$cart_item['Class'] = $this->Classes->findAllById($student_data['class_id'])->contain('Lessons', function ($q) {
					return $q->where(['lesson_date >' => time()]);
				})->contain('Schools')->first();
				$cart_item['weeks_enrolled'] = explode(',', $student_data['weeks_enrolled']);

				$total_lesson_count = $this->Classes->Lessons->findAllByClassId($student_data['class_id'])->count();

				$cart_item['price'] = round($cart_item['Class']['price'] * count($cart_item['weeks_enrolled']), 2);

				$cart_item['action'] = 're-enroll';

				array_push($cart_data['items'], $cart_item);

				foreach($cart_data['items'] as $cart_item){
					$totalPrice += $cart_item['price'];
				}

				if (count($cart_data['items']) > 1 ){
					$cart_data['discountMultiplier'] = 0.9;
					$totalPrice = $totalPrice * $cart_data['discountMultiplier'];
				}

				$cart_data['total_price'] = $totalPrice;


				$this->request->session()->write("cart", $cart_data);

				return $this->redirect(['controller' => 'enrolments', 'action' => 'cart']);

				///////////////////////////////////////////
				/*
				$student_data = $this->request->getData();
				$student_data['weeks_enrolled'] = explode(',', $student_data['weeks_enrolled']);

				$student = $this->Students->patchEntity($student, $student_data);
				if ($this->Students->save($student)) {
					$this->loadModel('Enrolments');
					$enrolment = $this->Enrolments->newEntity();

					$this->loadModel('Lessons');

					$first_lesson = $this->Lessons->get($student['weeks_enrolled'][0]);

					$enrolment_data = array(
						'student_id' => $student->id,
						'class_id' => $student['class_id'],
						'enrolment_date' => $first_lesson->lesson_date,
						'no_of_weeks_enrolled' => count($student['weeks_enrolled'])
					);

					$enrolment = $this->Enrolments->patchEntity($enrolment, $enrolment_data);

					if ($this->Enrolments->save($enrolment)) {

						$this->loadModel('Lessons');
						$this->loadModel('LessonEnrolment');

						//for($i = 0; $i < $this->request->getData('no_of_weeks_enrolled'); $i++) {
						foreach($student['weeks_enrolled'] as $lesson_id) {

							// Get the next lesson date after the start of the user's enrolment, then each week afterwards using the loop
							/*$lesson = $this->Lessons->find('all', array(
								'conditions' => array(
									'class_id' => $enrolment->class_id,
									'lesson_date <=' => $enrolment->enrolment_date->modify('+' . (7 * $i) . ' days'),
									'lesson_date >' => $enrolment->enrolment_date->modify('+' . (7 * ($i-1)) . ' days'),
								),
								'order' => array(
									'Lessons.lesson_date' => 'ASC'
								)
							))->first();* /

							$lesson = $this->Lessons->get($lesson_id);

							if ($lesson) {
								$le = $this->LessonEnrolment->newEntity();
								$le->lesson_id = $lesson->id;
								$le->enrolment_id = $enrolment->id;
								$this->LessonEnrolment->save($le);
							}

						}

						$this->Flash->success(__('The student has been re-enrolled.'));

						//$this->request->session()->write("student_id", $student->id);

						$this->request->session()->delete("student_id");
						$this->request->session()->delete("claas_id");

						return $this->redirect(['controller' => 'users', 'action' => 'dashboard']);
					}
				}*/
			}

			$this->set(compact('student', 'grades', 'classes'));
		} else {
			$this->Redirect(['controller' => 'users', 'action' => 'dashboard']);
		}
	}


	public function cart() {
		$this->set('cart_data', $this->request->session()->read("cart"));
	}

	public function removeFromCart($index) {
		$cart_data = $this->request->session()->read('cart');
		unset($cart_data['items'][$index]);

		$totalPrice = 0;

		foreach($cart_data['items'] as $cart_item){
			$totalPrice += $cart_item['price'];
		}

		if (count($cart_data['items']) > 1 ){
			$cart_data['discountMultiplier'] = 0.9;
			$totalPrice = $totalPrice * $cart_data['discountMultiplier'];
		}

		$cart_data['total_price'] = $totalPrice;

		$this->request->session()->write("cart", $cart_data);
		$this->Redirect(['action' => 'cart']);
	}

	public function payment(){
		$price = $this->request->session()->read("cart.total_price");
		$this->set('price', $price);

		if ($this->request->is('post')) {
			$apiKey = env('EWAY_API_KEY');
			$apiPassword = env('EWAY_API_PASSWORD');

			if(env('EWAY_API_ENDPOINT') == 'production') {
				$apiEndpoint = \Eway\Rapid\Client::MODE_PRODUCTION;
			} else {
				$apiEndpoint = \Eway\Rapid\Client::MODE_SANDBOX;
			}

			$client = \Eway\Rapid::createClient($apiKey, $apiPassword, $apiEndpoint, new \Eway\Rapid\Service\Logger());

			$transaction = array(
				'Customer' => array(
					'CardDetails' => array(
						'Name' => $this->request->data('name'),
						'Number' => $this->request->data('EWAY_CARDNUMBER'),
						'ExpiryMonth' => $this->request->data('expiry_month'),
						'ExpiryYear' => $this->request->data('expiry_year'),
						'CVN' => $this->request->data('EWAY_CARDCVN'),
					)
				),
				'Payment' => array(
					'TotalAmount' => $price * 100
				),
				'TransactionType' => \Eway\Rapid\Enum\TransactionType::PURCHASE
			);
			//echo "Trying Payment<br>";
			
			$response = $client->createTransaction(\Eway\Rapid\Enum\ApiMethod::DIRECT, $transaction);
			//echo "Payment Processed<br>";
			
			if ($response->TransactionStatus) {
				// Payment successful
				//echo "Loading payment model<br>";
				$this->loadModel('PaymentHistory');
				$paymentHistory = $this->PaymentHistory->newEntity(array(
					'amount' => $price,
					'datetime' => date("Y-m-d H:i:s"),
					'payment_status' => \Eway\Rapid::getMessage($response->ResponseMessage),
				));

				$email_items = array();
				//echo "starting loop<br>";
				foreach($this->request->session()->read("cart.items") as $student) {
					if($student['action'] === 'enroll') {
						$this->loadModel('StudentContact');
						$sc = $this->StudentContact->newEntity();
						$sc->student_id = $student['Student']->id;
						$sc->user_id = $this->Auth->user('id');
						$this->StudentContact->save($sc);
					}

					$this->loadModel('Enrolments');
					$enrolment = $this->Enrolments->newEntity();

					$this->loadModel('Lessons');
					//echo "finding lessons<br>";
					//echo "<pre>";
					//print_r($student['weeks_enrolled']);
					//echo "</pre><br>";
					$first_lesson = $this->Lessons->get($student['weeks_enrolled'][0]);
					//echo "first lesson found<br>";
					$last_lesson = $this->Lessons->get($student['weeks_enrolled'][count($student['weeks_enrolled'])-1]);
					//echo "lessons found<br>";
					$enrolment_data = array(
						'student_id' => $student['Student']->id,
						'class_id' => $student['Class']['id'],
						'enrolment_date' => $first_lesson->lesson_date,
						'no_of_weeks_enrolled' => count($student['weeks_enrolled'])
					);

					$enrolment = $this->Enrolments->patchEntity($enrolment, $enrolment_data);
					//echo "trying to save enrolment<br>";
					if ($this->Enrolments->save($enrolment)) {
						//echo "Enrolment saved<br>";
						//$paymentHistory->enrolment_id = $enrolment->id;
						$this->PaymentHistory->save($paymentHistory);

						$this->loadModel('PaymentHistoryEnrolment');

						$phe = $this->PaymentHistoryEnrolment->newEntity();
						$this->PaymentHistoryEnrolment->patchEntity($phe, array(
							'payment_history_id' => $paymentHistory->id,
							'enrolment_id' => $enrolment->id
						));
						$this->PaymentHistoryEnrolment->save($phe);
						//echo "Payment saved<br>";

						array_push($email_items, array(
							'student_name'	=> $student['Student']['full_name'],
							'class_name'	=> $student['Class']['name'],
							'school_name'	=> $student['Class']['school']['school_name'],
							'teacher_name'	=> $student['Class']['teacher']['user']['first_name'] . ' ' . $student['Class']['teacher']['user']['last_name'],
							'first_name'	=> $student['Class']['school']['school_name'],
							'start_date'	=> $first_lesson->lesson_date->i18nFormat('dd/MM/yyyy'),
							'end_date'		=> $last_lesson->lesson_date->i18nFormat('dd/MM/yyyy'),
							'price'			=> $student['price']
						));
						
						$critical = false;
						$list_id = '75f7313bbd';
						$school_mc_id = $student['Class']['school']['mailchimp_id'];
						$user_email = $this->Auth->user('user_email');

						$segment = $this->mailchimp_subscribe_interest($critical, $list_id, $user_email, $school_mc_id);

						//echo "Loading lesson models<br>";
						$this->loadModel('Lessons');
						$this->loadModel('LessonEnrolment');

						foreach($student['weeks_enrolled'] as $lesson_id) {
							$lesson = $this->Lessons->get($lesson_id);

							if ($lesson) {
								$le = $this->LessonEnrolment->newEntity();
								$le->lesson_id = $lesson->id;
								$le->enrolment_id = $enrolment->id;
								$this->LessonEnrolment->save($le);
							}
						}
					}
					else {
						echo "<pre>";
						print_r($enrolment);
						echo "</pre>";
						exit;
					}
				}


				$to = array($this->Auth->user('user_email'));
				$subject = 'Drama Time Payment Receipt';
				$template = 'payment-receipt';
				$from = 'Drama Time';
				$merge_variables = array(
					'name'			=> $this->Auth->user('first_name'),
					'email'			=> $this->Auth->user('user_email'),
					'login_link'	=> Router::url(['controller' => 'Users', 'action' => 'login'], true),
					'items'			=> $email_items
				);
				//echo "Trying to send email<br>";
				$this->send_email($to, $from, $subject, $template, $merge_variables);
				//echo "Email sent<br>";
				
				//Send notification to Dramatime admin
				$subject = 'Drama Time Enrolment Notification';
				$template = 'enrolment-notification';
				$to = array('contact@dramatime.com.au');
				//$to = array('bwu31@student.monash.edu');
				$this->send_email($to, $from, $subject, $template, $merge_variables);

				
				$purchase_data = array(
					'cart' => $this->request->session()->read("cart"),
					'payment' => $response
				);
				$this->request->session()->write("purchase", $purchase_data);
				$this->request->session()->delete('cart');
				//echo "Trying to redirect<br>";
				$this->Redirect(['action' => 'success']);

			} else {
				// Payment unsuccessful
				$this->Flash->error('Payment Unsuccessful: ' . $response->ResponseMessage . ' Please, try again.' );
			}
		}
	}


	public function success() {
		$this->set('purchase_data', $this->request->session()->read("purchase"));
	}


	public function beforeFilter(Event $event){
		$this->Auth->allow(['enrol']);
		if(!$this->action_is_public($event->subject) && $this->get_current_user_role_name() === 'parent') {
			$this->Auth->allow(['enrol', 'reenroll', 'cart', 'removeFromCart', 'payment', 'success']);
		}
		parent::beforeFilter($event);
	}

}
