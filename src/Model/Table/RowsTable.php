<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Rows Model
 *
 * @property \App\Model\Table\RowTemplatesTable|\Cake\ORM\Association\BelongsTo $RowTemplates
 * @property \App\Model\Table\WebpagesTable|\Cake\ORM\Association\BelongsTo $Webpages
 * @property \App\Model\Table\ColumnsTable|\Cake\ORM\Association\HasMany $Columns
 *
 * @method \App\Model\Entity\Row get($primaryKey, $options = [])
 * @method \App\Model\Entity\Row newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Row[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Row|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Row patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Row[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Row findOrCreate($search, callable $callback = null, $options = [])
 */
class RowsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('rows');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('RowTemplates', [
            'foreignKey' => 'row_template_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Webpages', [
            'foreignKey' => 'webpage_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Columns', [
            'foreignKey' => 'row_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['row_template_id'], 'RowTemplates'));
        $rules->add($rules->existsIn(['webpage_id'], 'Webpages'));

        return $rules;
    }
}
