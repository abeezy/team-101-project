<?php
namespace App\Controller;
use Cake\Event\Event;
use App\Controller\AppController;

/**
 * Webpages Controller
 *
 * @property \App\Model\Table\WebpagesTable $Webpages
 *
 * @method \App\Model\Entity\Webpage[] paginate($object = null, array $settings = [])
 */
class WebpagesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $webpages = $this->paginate($this->Webpages);

        $this->set(compact('webpages'));
        $this->set('_serialize', ['webpages']);
    }

    /**
     * View method
     *
     * @param string|null $id Webpage id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $webpage = $this->Webpages->get($id, [
            'contain' => [
				'Rows' => [
					'RowTemplates',
					'Columns'
				],
				'CarouselItems'
			]
        ]);

        $this->set('webpage', $webpage);
        $this->set('_serialize', ['webpage']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $webpage = $this->Webpages->newEntity();
        if ($this->request->is('post')) {
            $webpage = $this->Webpages->patchEntity($webpage, $this->request->getData());
            if ($this->Webpages->save($webpage)) {
                $this->Flash->success(__('The webpage has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The webpage could not be saved. Please, try again.'));
        }
        $this->set(compact('webpage'));
        $this->set('_serialize', ['webpage']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Webpage id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $webpage = $this->Webpages->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $webpage = $this->Webpages->patchEntity($webpage, $this->request->getData());
            if ($this->Webpages->save($webpage)) {
                $this->Flash->success(__('The webpage has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The webpage could not be saved. Please, try again.'));
        }
        $this->set(compact('webpage'));
        $this->set('_serialize', ['webpage']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Webpage id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $webpage = $this->Webpages->get($id);
        if ($this->Webpages->delete($webpage)) {
            $this->Flash->success(__('The webpage has been deleted.'));
        } else {
            $this->Flash->error(__('The webpage could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

	public function beforeFilter(Event $event){
		parent::beforeFilter($event);
	}
}
