<div class="container">

	<div class="row justify-content-center">
		<div class="col-auto px-4 py-2 mx-3 progress-chevron active">
			Parent
		</div>
		<div class="col-auto px-4 py-2 mx-3 progress-chevron">
			Student
		</div>
		<div class="col-auto px-4 py-2 mx-3 progress-chevron">
			Cart
		</div>
		<div class="col-auto px-4 py-2 mx-3 progress-chevron">
			Payment
		</div>
		<div class="col-auto px-4 py-2 mx-3 progress-chevron">
			Summary
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-md-4">
			<h2 class="text-center">Please Enter Your Details</h2>
			<p> <?php echo $this->Html->link('If you already have an account, please click here', array('controller' => 'users', 'action' => 'signup_login'), array('class' => 'btn btn-primary text-center')); ?></p>
			<p class="text-center"><em> Or </em> </p>
			<p>If you are new to Drama Time, please fill out your details below.</p>

			<?php echo $this->Form->create($user, array('class' => 'form-signin', 'autocomplete' => 'disabled')); ?>


				<?php echo $this->Form->control('first_name',['type' => 'text', 'placeholder' => 'John',]); ?>
				<?php echo $this->Form->control('last_name', ['type' => 'text', 'placeholder' => 'Smith']); ?>
				<?php echo $this->Form->control('user_email', ['type' => 'email', 'placeholder' => 'JohnSmith@gmail.com']); ?>

				<?php echo $this->Form->control('password', ['minLength' => 6,'placeholder' => 'Password must be at least 6 characters']); ?> 
				
				<?php echo $this->Form->control('cPassword', ['minLength' => 6,'placeholder' => 'Confirm Password must match your password', 'label' => 'Confirm Password', 'type' => 'password']); ?> 
				
				<?php echo $this->Form->control('phone_number', ['type' => 'tel','minLength' => 10, 'placeholder' => 'eg. 0400000123']); ?>
				
				<div class="form-group">
					<button class="btn btn-lg btn-primary btn-block" type="submit">Next</button>
				</div>

			<?php echo $this->Form->end(); ?>
		</div>
	</div>
</div>







<!-- /container -->
