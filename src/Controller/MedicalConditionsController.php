<?php
namespace App\Controller;
use Cake\Event\Event;
use App\Controller\AppController;

/**
 * MedicalConditions Controller
 *
 * @property \App\Model\Table\MedicalConditionsTable $MedicalConditions
 *
 * @method \App\Model\Entity\MedicalCondition[] paginate($object = null, array $settings = [])
 */
class MedicalConditionsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $medicalConditions = $this->paginate($this->MedicalConditions);

        $this->set(compact('medicalConditions'));
        $this->set('_serialize', ['medicalConditions']);
    }

    /**
     * View method
     *
     * @param string|null $id Medical Condition id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $medicalCondition = $this->MedicalConditions->get($id, [
            'contain' => []
        ]);

        $this->set('medicalCondition', $medicalCondition);
        $this->set('_serialize', ['medicalCondition']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $medicalCondition = $this->MedicalConditions->newEntity();
        if ($this->request->is('post')) {
            $medicalCondition = $this->MedicalConditions->patchEntity($medicalCondition, $this->request->getData());
            if ($this->MedicalConditions->save($medicalCondition)) {
                $this->Flash->success(__('The medical condition has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The medical condition could not be saved. Please, try again.'));
        }
        $this->set(compact('medicalCondition'));
        $this->set('_serialize', ['medicalCondition']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Medical Condition id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $medicalCondition = $this->MedicalConditions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $medicalCondition = $this->MedicalConditions->patchEntity($medicalCondition, $this->request->getData());
            if ($this->MedicalConditions->save($medicalCondition)) {
                $this->Flash->success(__('The medical condition has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The medical condition could not be saved. Please, try again.'));
        }
        $this->set(compact('medicalCondition'));
        $this->set('_serialize', ['medicalCondition']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Medical Condition id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $medicalCondition = $this->MedicalConditions->get($id);
        if ($this->MedicalConditions->delete($medicalCondition)) {
            $this->Flash->success(__('The medical condition has been deleted.'));
        } else {
            $this->Flash->error(__('The medical condition could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
	
	public function beforeFilter(Event $event){
		parent::beforeFilter($event);
	}
}
