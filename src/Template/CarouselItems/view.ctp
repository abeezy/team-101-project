<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\carouselItem $carouselItem
  */
?>
<div class="row justify-content-between align-items-center mb-3">
	<div class="col-auto">
		<h3 class="mb-0"><?php echo $CarouselItem->name; ?></h3>
	</div>
	<div class="col-auto">
		<?php echo $this->Html->link('<i class="fas fa-pencil-alt"></i>&nbsp;&nbsp;Edit Carousel Picture', ['action' => 'edit', $CarouselItem->id], ['class' => 'btn btn-outline-primary', 'escape' => false]) ?></li>
	</div>
</div>
<div class="carouselItems view large-9 medium-8 columns content">
	<div class="card mb-3">
		<table class="table table-responsive table-striped mb-0">
			<tr>
				<th scope="row"><?= __('Name') ?></th>
				<td><?= h($CarouselItem->name) ?></td>
			</tr>
			<tr>
				<th scope="row"><?= __('Picture') ?></th>
				<td><?php echo $this->Html->image($CarouselItem->image, ['class' => 'd-block w-100']);?></td>
			</tr>
		</table>
	</div>
   <button type="button" class = 'btn btn-light' onclick="goBack()">Go Back</button>

<script>
function goBack() {
    window.history.back();
}
</script>
</div>
