<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="container">
	<div class="rows form large-9 medium-8 columns content">
	<h2>Add Row</h2>
		<?= $this->Form->create($row) ?>
		<fieldset>
			<?php
				echo $this->Form->control('row_template_id', ['options' => $rowTemplates]);
				echo $this->Form->control('webpage_id', ['options' => $webpages]);
				echo $this->Form->control('row_description');
			?>
		</fieldset>
		 <?php echo $this->Form->button(__('Add'), array('class' => 'btn btn-light')); ?>
		<button type="button" class = 'btn btn-light' onclick="goBack()">Go Back</button>

<script>
function goBack() {
    window.history.back();
}
</script>
		<?= $this->Form->end() ?>
	</div>
</div> 
