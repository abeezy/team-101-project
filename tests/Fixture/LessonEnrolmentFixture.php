<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * LessonEnrolmentFixture
 *
 */
class LessonEnrolmentFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'lesson_enrolment';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'lesson_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'enrolment_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'attanded' => ['type' => 'boolean', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'lesson_enrol' => ['type' => 'index', 'columns' => ['enrolment_id'], 'length' => []],
            'lesson_less' => ['type' => 'index', 'columns' => ['lesson_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'lesson_less' => ['type' => 'foreign', 'columns' => ['lesson_id'], 'references' => ['lessons', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'lesson_enrol' => ['type' => 'foreign', 'columns' => ['enrolment_id'], 'references' => ['enrolments', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'lesson_id' => 1,
            'enrolment_id' => 1,
            'attanded' => 1
        ],
    ];
}
