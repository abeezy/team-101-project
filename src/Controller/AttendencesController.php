<?php
namespace App\Controller;
use Cake\Event\Event;
use App\Controller\AppController;

/**
 * Attendences Controller
 *
 * @property \App\Model\Table\AttendencesTable $Attendences
 *
 * @method \App\Model\Entity\Attendence[] paginate($object = null, array $settings = [])
 */
class AttendencesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Students', 'Lessons']
        ];
        $attendences = $this->paginate($this->Attendences);

        $this->set(compact('attendences'));
        $this->set('_serialize', ['attendences']);
    }

    /**
     * View method
     *
     * @param string|null $id Attendence id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $attendence = $this->Attendences->get($id, [
            'contain' => ['Students', 'Lessons']
        ]);

        $this->set('attendence', $attendence);
        $this->set('_serialize', ['attendence']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $attendence = $this->Attendences->newEntity();
        if ($this->request->is('post')) {
            $attendence = $this->Attendences->patchEntity($attendence, $this->request->getData());
            if ($this->Attendences->save($attendence)) {
                $this->Flash->success(__('The attendence has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The attendence could not be saved. Please, try again.'));
        }
        $students = $this->Attendences->Students->find('list', ['limit' => 200]);
        $lessons = $this->Attendences->Lessons->find('list', ['limit' => 200]);
        $this->set(compact('attendence', 'students', 'lessons'));
        $this->set('_serialize', ['attendence']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Attendence id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $attendence = $this->Attendences->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $attendence = $this->Attendences->patchEntity($attendence, $this->request->getData());
            if ($this->Attendences->save($attendence)) {
                $this->Flash->success(__('The attendence has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The attendence could not be saved. Please, try again.'));
        }
        $students = $this->Attendences->Students->find('list', ['limit' => 200]);
        $lessons = $this->Attendences->Lessons->find('list', ['limit' => 200]);
        $this->set(compact('attendence', 'students', 'lessons'));
        $this->set('_serialize', ['attendence']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Attendence id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $attendence = $this->Attendences->get($id);
        if ($this->Attendences->delete($attendence)) {
            $this->Flash->success(__('The attendence has been deleted.'));
        } else {
            $this->Flash->error(__('The attendence could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
	
	public function beforeFilter(Event $event){
		parent::beforeFilter($event);
	}
}
