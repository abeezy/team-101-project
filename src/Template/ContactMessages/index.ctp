<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\ContactMessage[]|\Cake\Collection\CollectionInterface $contactMessages
  */
?>

<div class="container">

<div class="contactMessages index large-9 medium-8 columns content">
    <h3><?= __('Contact Messages') ?></h3>
    <table class="table table-responsive table-striped bg-white">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('user_email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($contactMessages as $contactMessage): ?>
            <tr>
                <td><?= h($contactMessage->user_email) ?></td>
                <td><?= h($contactMessage->created->i18nFormat('dd/MM/yyyy HH:mm')) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $contactMessage->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $contactMessage->id], ['confirm' => __('Are you sure you want to delete # {0}?', $contactMessage->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator text-center">
        <ul class="pagination justify-content-center">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('<i class="fa fa-chevron-left" aria-hidden="true"></i> ' . __('Previous'), ['escape' => false]) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Next') . ' <i class="fa fa-chevron-right" aria-hidden="true"></i>', ['escape' => false]) ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>

        </ul>
        <p class="text-muted"><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>

</div>
