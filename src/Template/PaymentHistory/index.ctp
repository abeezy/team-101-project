<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\PaymentHistory[]|\Cake\Collection\CollectionInterface $paymentHistory
  */
?>

<div class="container">

<div class="paymentHistory index large-9 medium-8 columns content">
    <h3><?= __('Payment History') ?></h3>
    <table class="table table-responsive table-striped bg-white">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('enrolment_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('amount') ?></th>
                <th scope="col"><?= $this->Paginator->sort('datetime') ?></th>
                <th scope="col"><?= $this->Paginator->sort('payment_status') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($payments as $paymentHistory): ?>
            <tr>
                <td><?= $this->Number->format($paymentHistory->id) ?></td>
                <td><?= $paymentHistory->has('enrolment') ? $this->Html->link($paymentHistory->enrolment->id, ['controller' => 'Enrolments', 'action' => 'view', $paymentHistory->enrolment->id]) : '' ?></td>
                <td><?= h($paymentHistory->amount) ?></td>
                <td><?= h($paymentHistory->datetime) ?></td>
                <td><?= h($paymentHistory->payment_status) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $paymentHistory->id]) ?>
                   
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator text-center">
        <ul class="pagination justify-content-center">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('<i class="fa fa-chevron-left" aria-hidden="true"></i> ' . __('Previous'), ['escape' => false]) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Next') . ' <i class="fa fa-chevron-right" aria-hidden="true"></i>', ['escape' => false]) ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p class="text-muted"><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
