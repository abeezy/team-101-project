<?php
	$lastColourIndex = null;
	$lastRotation = null;

	function randomBg() {
		global $lastColourIndex;
		$colours = ['menu-dark-orange', 'menu-orange', 'menu-yellow', 'menu-green', 'menu-light-blue', 'menu-dark-blue'];

		$random = rand(0, count($colours)-1);

		while($random === $lastColourIndex) {
			$random = rand(0, count($colours)-1);
		}

		$lastColourIndex = $random;
		echo $colours[$random];
	}

	function randomRotation() {
		global $lastRotation;

		$random = rand(-7, 7);

		while($random === $lastRotation || ($random < 2 && $random > -2)) {
			$random = rand(-7, 7);
		}

		$lastRotation = $random;
		echo ' menu-rotate' . $random;
	}

	function derotate() {
		global $lastRotation;
		return ' menu-rotate' . ($lastRotation * -1);
	}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-110655822-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-110655822-1');
	</script>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<?php echo $this->Html->tag('base', null, ['href'=>$this->Url->build('/', true)]); ?>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto|Open+Sans" rel="stylesheet">
		<script src="https://use.fontawesome.com/releases/v5.0.2/js/all.js" defer="defer"></script>
		<?php echo $this->Html->css("jquery-ui.min");?>

    <?php echo $this->Html->css("custom");?>
  </head>
  <body>
	<div class="headerBackground">
		<div class="container-fluid orangeBand">
			<div class="container">
				<div class="row justify-content-between">
					<div class="col-auto">
						<?php echo $this->Html->link('<i class="fas fa-home" aria-hidden="true"></i> Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('class' => 'btn btn-link text-white', 'escape' => false)); ?>
					</div>
					<div class="col-auto">
						<?php if ($loggedIn):
							if ($this->request->session()->check('cart')){
								echo $this->Html->link('<i class="fa fa-shopping-cart" aria-hidden="true"></i> Cart', array('controller' => 'enrolments', 'action' => 'cart'), array('class' => 'btn btn-link text-white', 'escape' => false));
							}
							echo $this->Html->link('<i class="fa fa-user" aria-hidden="true"></i> ' . $user_first_name . '\'s Account', array('controller' => 'users', 'action' => 'dashboard'), array('class' => 'btn btn-link text-white', 'escape' => false));
							echo $this->Html->link('<i class="fas fa-sign-out-alt" aria-hidden="true"></i> Log out', array('controller' => 'users', 'action' => 'logout'), array('class' => 'btn btn-link text-white', 'escape' => false));
						else:
							echo $this->Html->link('<i class="fas fa-sign-in-alt" aria-hidden="true"></i> Log In', array('controller' => 'users', 'action' => 'login'), array('class' => 'btn btn-link text-white', 'escape' => false));
						endif; ?>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid bg-secondary py-3">
			<div class="container">
				<div class="row">
					<div class="col-auto">
						<h1 class="text-white my-0">Drama Time</h1>
					</div>
					<div class="col">
						<div class="input-group input-group-lg">
							<span class="input-group-addon"><i class="fas fa-search"></i>&nbsp;&nbsp;Quick Search</span>
							<input type="text" class="form-control quickSearch" autocomplete="off" placeholder="Search by Class, School, Student or User" data-users-path="<?php echo $this->Url->build(['controller' => 'users', 'action' => 'search'], true); ?>" data-students-path="<?php echo $this->Url->build(['controller' => 'students', 'action' => 'search'], true); ?>" data-schools-path="<?php echo $this->Url->build(['controller' => 'schools', 'action' => 'search'], true); ?>" data-classes-path="<?php echo $this->Url->build(['controller' => 'classes', 'action' => 'search'], true); ?>"/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

    <?php echo $this->Flash->render(); ?>

	<div class="container">
		<div class="row">
			<div class="col-12 col-md-3">
				<div class="list-group mb-3">
					
					<?php echo $this->Html->link('Schools', array('controller' => 'schools', 'action' => 'index'), array('class' => 'list-group-item')); ?>
					<?php echo $this->Html->link('Classes', array('controller' => 'classes', 'action' => 'index'), array('class' => 'list-group-item')); ?>

					<?php echo $this->Html->link('Students', array('controller' => 'students', 'action' => 'index'), array('class' => 'list-group-item')); ?>
					
					<?php echo $this->Html->link('Messages', array('controller' => 'contactMessages', 'action' => 'index'), array('class' => 'list-group-item')); ?>
				</div>
				<div class="list-group">
					<?php echo $this->Html->link('Parents', array('controller' => 'users', 'action' => 'parents'), array('class' => 'list-group-item')); ?>
					<?php echo $this->Html->link('Teachers', array('controller' => 'users', 'action' => 'teachers'), array('class' => 'list-group-item')); ?>
					<?php echo $this->Html->link('Admin', array('controller' => 'users', 'action' => 'admins'), array('class' => 'list-group-item')); ?>
					<?php echo $this->Html->link('Site Content', array('controller' => 'webpages', 'action' => 'index'), array('class' => 'list-group-item')); ?>
				</div>
			</div><!--/span-->
			<div class="col-12 col-md-9">
				<?php echo $this->fetch('content'); ?>
			</div>
		</div>
	</div>


    <footer class="footer">
      <div class="container">
        <div class="row justify-content-between">
          <div class="col-auto">
			<h3 class="footer"> Visit </h3>
            <a href="https://www.facebook.com/Drama-Time-271445892993065/">Find us on Facebook!</a>

            <ul class="list-unstyled">
              <li class="nav-footer">
                <?php echo $this->Html->link('Enrolments', array('controller' => 'classes', 'action' => 'all')); ?>
              </li>
              <li class="nav-footer">
                <?php echo $this->Html->link('Programs', array('controller' => 'pages', 'action' => 'display', 'programs')); ?>
              </li>
              <li class="nav-footer">
                <?php echo $this->Html->link('Locations', array('controller' => 'pages', 'action' => 'display', 'locations')); ?>
              </li>
              <li class="nav-footer">
                <?php echo $this->Html->link('Our Team', array('controller' => 'pages', 'action' => 'display', 'our-team')); ?>
              </li>
              <li class="nav-footer">
                <?php echo $this->Html->link('Contact Us', array('controller' => 'contactMessages', 'action' => 'contactUs')); ?>
              </li>
              <li class="nav-footer">
                <?php echo $this->Html->link('Testimonials', array('controller' => 'pages', 'action' => 'display', 'testimonials')); ?>
              </li>
			  <li class="nav-footer">
                <?php echo $this->Html->link('Terms & Conditions', array('controller' => 'pages', 'action' => 'display', 'conditions')); ?>
              </li>
            </ul>
          </div>
          <div class="col-auto">
            <p>&nbsp;</p>
			<h3 class="contactUs"> Contact Us </h3>
            PO Box 2442<br>
            Caulfield Junction<br>
            Victoria 3162<br>
            Australia<br>
            <a href="mailto:contact@dramatime.com.au">contact@dramatime.com.au</a><br>
            03 9525 9977
          </div>
        </div>
		<div class="push-base clear"></div>

          <div class="col text-center">
            <span class="text-muted">&copy; Dramatime 2008-<?php echo date("Y");?></span>
          </div>

      </div>
    </footer>

    <div class="modal fade" id="messageModal">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="messageModalTitle"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" id="messageModalContent">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
    <?php echo $this->Html->script("custom", array('defer' => 'defer'));?>
		<?php echo $this->Html->script("jquery-ui.min", array('defer' => 'defer'));?>
  </body>
</html>
