<div class="container">

	<div class="row justify-content-center">
		<div class="col-md-4">
			<div class="row justify-content-between">
				<div class="col-auto px-4 py-2 progress-chevron">
					Step 1
				</div>
				<div class="col-auto px-4 py-2 progress-chevron">
					Step 2
				</div>
				<div class="col-auto px-4 py-2 progress-chevron active">
					Step 3
				</div>
			</div>

	</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-md-4">
			<h2 class="text-center">Enrol Now</h2>

			<?php echo $this->Form->create($enrolment, array('class' => 'form-enrol', 'autocomplete' => 'disabled')); ?>

				<?php // echo $this->Form->control('class_id',['type' => 'select','options' => $classes]); ?>
				<?php if($enrolment->class_id !== null){
					echo $this->Form->input('class_id', ['type' => 'hidden', 'value' => $enrolment->class_id]);
				} else {
					echo $this->Form->input('class_id', ['type' => 'select', 'options' => $classes, 'empty' => 'Please Select']);
				}
				?>


				<?php echo $this->Form->input('enrolment_date', ['class' => 'datepicker-input', 'data-date-start-date' => '-0d', 'type' => 'text', 'format' => 'Y-m-d', 'default' => date('Y-m-d'), 'value' => !empty($enrolment->enrolment_date) ? $enrolment->enrolment_date->format('Y-m-d') : date('Y-m-d')]); ?>

				<?php echo $this->Form->control('no_of_weeks_enrolled', ['type' => 'number', 'placeholder' => 'Number of Weeks']); ?>

				<div class="form-group">
					<button class="btn btn-lg btn-primary btn-block" type="submit" name="submitButton" value="enrolNow">Enrol now</button>
					<button class="btn btn-lg btn-secondary btn-block" type="submit" name="submitButton" value="addAnother">Add Another Child</button>
				</div>


			<?php echo $this->Form->end(); ?>
		</div>
	</div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js" defer="defer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" />
