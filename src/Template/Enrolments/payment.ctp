<div class="container">

	<div class="row justify-content-center">
		<div class="col-auto px-4 py-2 mx-3 progress-chevron">
			Parent
		</div>
		<div class="col-auto px-4 py-2 mx-3 progress-chevron">
			Student
		</div>
		<div class="col-auto px-4 py-2 mx-3 progress-chevron">
			Cart
		</div>
		<div class="col-auto px-4 py-2 mx-3 progress-chevron active">
			Payment
		</div>
		<div class="col-auto px-4 py-2 mx-3 progress-chevron">
			Summary
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-md-4">
			<h2 class="text-center">Pay Now </h2>

			<p><strong>Total Due (inc GST):</strong> <?php echo $this->Number->currency($price); ?></p>
			
			<?php echo $this->Form->create(null, array('autocomplete' => 'disabled', 'data-eway-encrypt-key' => env('EWAY_API_CLIENT_SIDE_KEY'))); ?>
				
				<input type="hidden" name="_method" value="POST">			
				<div class="form-group">
					<label>Name on card</label>
					<input type="text" name="name" class="form-control" id="name">
				</div>
				<div class="form-group">
					<label for="card-number">Card Number</label>
					<input type="text" data-eway-encrypt-name="EWAY_CARDNUMBER" class="form-control" id="card-number">
					<small class="form-text text-muted">Visa or MasterCard only.</small>
				</div>
				
				
				<div class="row">	
				
				<div class="col">
			
					<label for="expiry-month">Expiry Month</label>
					<select name="expiry_month" class="form-control" id="expiry-month">
						<option selected="selected" disabled="disabled" value=""> Month </option>
						<option value="01">January</option>
						<option value="02">February</option>
						<option value="03">March</option>
						<option value="04">April</option>
						<option value="05">May</option>
						<option value="06">June</option>
						<option value="07">July</option>
						<option value="08">August</option>
						<option value="09">September</option>
						<option value="10">October</option>
						<option value="11">November</option>
						<option value="12">December</option>
					</select>
		
				</div>
				
				<div class="col">
					<label for="expiry-year">Expiry Year</label>
					<select name="expiry_year" class="form-control" id="expiry-year">
						<option selected="selected" disabled="disabled" value=""> Year </option>
						<?php for($i = date('Y'); $i < date('Y') + 11; $i++): ?>
							<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
						<?php endfor; ?>
					</select>
				</div>
				</div>
				</br>
			    <div class="form-group">
					<label>CVN</label> <span data-toggle="tooltip" data-placement="top" title="Card Verification Number (The digits on the back of your card)"><i class="fas fa-question-circle"></i></span>
					<input type="text" data-eway-encrypt-name="EWAY_CARDCVN" class="form-control" id="cvn">
				</div>
				
				<div class="form-group">
					<button class="btn btn-lg btn-primary btn-block" type="submit" name="submitButton">Pay now&nbsp;&nbsp;<i class="far fa-credit-card"></i></button>
				</div>
			<?php echo $this->Form->end(); ?>
		</div>
	</div>
</div>

<script src="https://secure.ewaypayments.com/scripts/eCrypt.min.js" defer="defer"></script>
