<?php
namespace App\Controller;
use Cake\Event\Event;
use App\Controller\AppController;

/**
 * ContactType Controller
 *
 * @property \App\Model\Table\ContactTypeTable $ContactType
 *
 * @method \App\Model\Entity\ContactType[] paginate($object = null, array $settings = [])
 */
class ContactTypeController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['StudentContact']
        ];
        $contactType = $this->paginate($this->ContactType);

        $this->set(compact('contactType'));
        $this->set('_serialize', ['contactType']);
    }

    /**
     * View method
     *
     * @param string|null $id Contact Type id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $contactType = $this->ContactType->get($id, [
            'contain' => ['StudentContact']
        ]);

        $this->set('contactType', $contactType);
        $this->set('_serialize', ['contactType']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $contactType = $this->ContactType->newEntity();
        if ($this->request->is('post')) {
            $contactType = $this->ContactType->patchEntity($contactType, $this->request->getData());
            if ($this->ContactType->save($contactType)) {
                $this->Flash->success(__('The contact type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The contact type could not be saved. Please, try again.'));
        }
        $studentContact = $this->ContactType->StudentContact->find('list', ['limit' => 200]);
        $this->set(compact('contactType', 'studentContact'));
        $this->set('_serialize', ['contactType']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Contact Type id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $contactType = $this->ContactType->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $contactType = $this->ContactType->patchEntity($contactType, $this->request->getData());
            if ($this->ContactType->save($contactType)) {
                $this->Flash->success(__('The contact type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The contact type could not be saved. Please, try again.'));
        }
        $studentContact = $this->ContactType->StudentContact->find('list', ['limit' => 200]);
        $this->set(compact('contactType', 'studentContact'));
        $this->set('_serialize', ['contactType']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Contact Type id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $contactType = $this->ContactType->get($id);
        if ($this->ContactType->delete($contactType)) {
            $this->Flash->success(__('The contact type has been deleted.'));
        } else {
            $this->Flash->error(__('The contact type could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
	
	public function beforeFilter(Event $event){
		parent::beforeFilter($event);
	}
}
