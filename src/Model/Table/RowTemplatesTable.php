<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RowTemplates Model
 *
 * @property \App\Model\Table\RowsTable|\Cake\ORM\Association\HasMany $Rows
 *
 * @method \App\Model\Entity\RowTemplate get($primaryKey, $options = [])
 * @method \App\Model\Entity\RowTemplate newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\RowTemplate[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\RowTemplate|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RowTemplate patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\RowTemplate[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\RowTemplate findOrCreate($search, callable $callback = null, $options = [])
 */
class RowTemplatesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('row_templates');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany('Rows', [
            'foreignKey' => 'row_template_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->scalar('content')
            ->requirePresence('content', 'create')
            ->notEmpty('content');

        $validator
            ->integer('column_count')
            ->requirePresence('column_count', 'create')
            ->notEmpty('column_count');

        return $validator;
    }
}
