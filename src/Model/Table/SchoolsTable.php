<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Schools Model
 *
 * @property \App\Model\Table\ClassesTable|\Cake\ORM\Association\HasMany $Classes
 * @property \App\Model\Table\StudentsTable|\Cake\ORM\Association\HasMany $Students
 *
 * @method \App\Model\Entity\School get($primaryKey, $options = [])
 * @method \App\Model\Entity\School newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\School[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\School|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\School patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\School[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\School findOrCreate($search, callable $callback = null, $options = [])
 */
class SchoolsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('schools');
        $this->setDisplayField('school_name');
        $this->setPrimaryKey('id');

        $this->hasMany('Classes', [
            'foreignKey' => 'school_id'
        ]);
        $this->hasMany('Users', [
            'foreignKey' => 'school_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

		$validator
			->scalar('mailchimp_id')
			->allowEmpty('mailchimp_id', 'create');

        $validator
            ->scalar('school_name')
            ->requirePresence('school_name', 'create')
            ->notEmpty('school_name');

        $validator
            ->scalar('school_address')
            ->requirePresence('school_address', 'create')
            ->notEmpty('school_address');


        return $validator;
    }

    public function beforeFind ($event, $query, $options, $primary) {
      $order = $query->clause('order');
      if ($order === null || !count($order)) {
          $query->order( [$this->alias() . '.school_name' => 'ASC'] );
      }
    }
}
