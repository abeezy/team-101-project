<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Student $student
  */
?>
<div class="container">
	<div class="row justify-content-between align-items-center mb-3">
		<div class="col-auto">
			<h3 class="mb-0"><?php echo $student->full_name; ?></h3>
		</div>
	</div>

	<div class="schools view large-9 medium-8 columns content">
		<div class="card mb-3">
			<table class="table table-responsive table-striped mb-0">
				<tr>
					<th scope="row"><?= __('First Name') ?></th>
					<td><?= h($student->first_name) ?></td>
				</tr>
				<tr>
					<th scope="row"><?= __('Last Name') ?></th>
					<td><?= h($student->last_name) ?></td>
				</tr>
				<tr>
					<th scope="row"><?= __('Gender') ?></th>
					 <td><?= h($student->student_gender) ?></td>
				</tr>
				<tr>
					<th scope="row"><?= __('School') ?></th>
				<td><?php echo $student->enrolments[0]->class->school->school_name; ?></td>
				</tr>
				<tr>
					<th scope="row"><?= __('Year Level') ?></th>
					 <td><?= h($student->grade->name) ?></td>
				</tr>
				<tr>
				<th scope="row"><?= __('Signup Date') ?></th>
				<td><?= h($student->signup_date->i18nFormat('dd/MM/yyyy')) ?></td>
			</tr>
			<tr>
			  <th>Notes</th>
			  <td><?= $this->Text->autoParagraph(h($student->notes)); ?></td>
			  </tr>
		</table>

		</div>
		</div>

		<?php /*
			<div class="related mb-3">
			<div class="card">
				<div class="card-header">
					<div class="row justify-content-between align-items-center">
						<div class="col-auto">
							<h4 class="mb-0">Enrolments</h4>
						</div>
					</div>
				 </div>

				<?php if (!empty($student->enrolments)): ?>
					<table class="table table-responsive table-striped mb-0">
						<thead>
							<tr>
					 <th scope="col"><?= __('Class') ?></th>
					<th scope="col"><?= __('Enrolment Start Date') ?></th>
					<th scope="col"><?= __('Enrolment End Date') ?></th>
					<th scope="col"><?= __('No Of Weeks Enrolled') ?></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($student->enrolments as $enrolments): ?>
								<tr>
									<td><?= $this->Html->link($enrolments->class->name, ['controller' => 'Enrolments', 'action' => 'view', $enrolments->id]) ?></td>
									 <td><?= h($enrolments->enrolment_date->i18nFormat('dd/MM/yyyy')) ?></td>
										<td><?php if (count($enrolments->lessons)>0) {h($enrolments->lessons[count($enrolments->lessons)-1]->lesson_date->i18nFormat('dd/MM/yyyy'));}?></td>
										<td><?= h($enrolments->no_of_weeks_enrolled) ?></td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				<?php endif; ?>
			</div>
		</div>

		*/ ?>


		<div class="related mb-3">
			<div class="card">
				<div class="card-header">
					<div class="row justify-content-between align-items-center">
						<div class="col-auto">
							<h4 class="mb-0">Parent/Guardian</h4>
						</div>
					</div>
				 </div>
				<?php if (!empty($student->student_contact)): ?>
					<table class="table table-responsive table-striped mb-0">
						<thead>
							<tr>
								<th scope="col"><?= __('Parent Name') ?></th>
								<th scope="col"><?= __('Contact Number') ?></th>
							</tr>
						</thead>
						<tbody>
							  <?php foreach ($student->student_contact as $studentContact): ?>
								<tr>
									<td><?php echo $studentContact->user->full_name;  ?></td>
									<td><?php echo $studentContact->user->phone_number;  ?></td>

								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				<?php endif; ?>
			</div>
		</div>

		<?php
			$siblingCounter = 0;
			foreach ($student->student_contact as $studentContact){
				foreach ($studentContact->user->student_contact as $studentContact2){
					if ($studentContact2->student->id !== $student->id){
						$siblingCounter++;
					}
				}
			}
		?>
		<?php if($siblingCounter > 0): ?>
			<div class="related mb-3">
				<div class="card">
					<div class="card-header">
						<div class="row justify-content-between align-items-center">
							<div class="col-auto">
								<h4 class="mb-0">Sibling</h4>
							</div>
						</div>
						<?php if (!empty($student->student_contact)): ?>
							<table class="table table-responsive table-striped mb-0">
								<thead>
									<tr>
										<th scope="col"><?= __('Student') ?></th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($student->student_contact as $studentContact): ?>
										<?php foreach ($studentContact->user->student_contact as $studentContact2): ?>
											<?php if ($studentContact2->student->id !== $student->id): ?>
												<tr>
													<td><?php echo $this->Html->link($studentContact2->student->full_name, array('action' => 'teacher_view', $studentContact2->student->id)); ?></td>
												</tr>
											<?php endif; ?>
										<?php endforeach; ?>
									<?php endforeach; ?>
								</tbody>
							</table>
						<?php endif; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>


				<button type="button" class = 'btn btn-light' onclick="goBack()">Go Back</button>

	<script>
	function goBack() {
		window.history.back();
	}
	</script>
</div>