<?php
/**
  * @var \App\View\AppView $this
  * @var \Cake\Datasource\EntityInterface[]|\Cake\Collection\CollectionInterface $classes
  */
?>

<div class="container">
<div class="btn-group mb-3">
<?php echo $this->Html->link('<i class="fas fa-plus"></i>&nbsp;&nbsp;New Classes', ['action' => 'add'], ['class' => 'btn btn-outline-primary', 'escape' => false]) ?></li>
	</div>
<div class="classes index large-9 medium-8 columns content">
    <h3><?= __('Classes') ?></h3>
    <table class="table table-responsive table-striped bg-white">
        <thead>
            <tr>
                <th scope="col">School</th>
                <th scope="col">Class</th>
                <th scope="col">Teacher</th>
                <th scope="col">Time</th>
                <th scope="col">Day</th>
                <th scope="col">Term</th>
				<th scope="col">Price</th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($classes as $class): ?>
            <tr>
                <td><?= $class->has('school') ? $this->Html->link($class->school->school_name, ['controller' => 'Schools', 'action' => 'view', $class->school->id]) : '' ?></td>
                <td><?= h($class->name) ?></td>
                <td><?= $class->has('teacher') ? $this->Html->link($class->teacher->user->first_name . ' ' . $class->teacher->user->last_name, ['controller' => 'Teachers', 'action' => 'view', $class->teacher->id]) : '' ?></td>
                <td><?= $class->class_time->i18nFormat('hh:mm a') ?></td>
                <td><?= h($class->class_day) ?></td>
                <td><?= $this->Number->format($class->class_term) ?></td>
				     <td>$<?= h($class->price) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $class->id]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator text-center">
        <ul class="pagination justify-content-center">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('<i class="fa fa-chevron-left" aria-hidden="true"></i> ' . __('Previous'), ['escape' => false]) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Next') . ' <i class="fa fa-chevron-right" aria-hidden="true"></i>', ['escape' => false]) ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
			<button type="button" class = 'btn btn-light' onclick="goBack()">Go Back</button>

<script>
function goBack() {
    window.history.back();
}
</script>
        </ul>
        <p class="text-muted"><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
</div>
