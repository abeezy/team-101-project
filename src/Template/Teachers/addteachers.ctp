
<?php $this->loadHelper('Form', ['templates' => 'form-defaults']); ?>

<div class="container">
	<h2 class="text-center">Teacher Sign Up</h2>

	<?php echo $this->Form->create($user, array('class' => 'form-signin', 'autocomplete' => 'disabled')); ?>

		<?php echo $this->Form->control('first_name',['type' => 'text', 'placeholder' => 'John']); ?>
		<?php echo $this->Form->control('last_name', ['type' => 'text', 'placeholder' => 'Smith']); ?>
		<?php echo $this->Form->control('user_email', ['type' => 'email', 'placeholder' => 'JohnSmith@gmail.com']); ?>
		<?php echo $this->Form->control('password', ['type' => 'password', 'minLength' => 8]); ?>
		<?php echo $this->Form->control('phone_number', ['type' => 'tel', 'placeholder' => '0400 000 123']); ?>
		<?php echo $this->Form->textarea('notes', ['placeholder' => 'Notes']); ?>

		<div class="form-group">
			<button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>

		</div>

	<?php echo $this->Form->end(); ?>

 </div>

<!-- /container -->