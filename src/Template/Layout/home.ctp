<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css" />
  <link rel="stylesheet" type="text/css" href="../css/dramatime.css" />
  <title>Home|Drama Time</title>

</head>


<body>
  <div id="header" class="jumbotron text-center">
	  <?php echo $this->Html->image('DT-Edit.jpg', array('width' => '300px', 'height' => '105px')) ?>
  </div>

	 
  <div class="container">
    <!-- <h3>Inline List</h3> -->
    <ul class="list-inline">
      <li><a class="tabstyle" href="home.ctp">Home</a></li>
	  <li><a class="tabstyle" href="enrolment.ctp">Enrolments</a></li>
      <li><a class="tabstyle" href="#">Programs</a></li>
      <li><a class="tabstyle" href="#">Testimonials</a></li>
      <li><a class="tabstyle" href="#">Locations</a></li>
      <li><a class="tabstyle" href="#">Our Team</a></li>
      <li><a class="tabstyle" href="#">Contact Us</a></li>
    </ul>
	</div>

<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <MARQUEE   scrollAmount=10  loop=infinite  deplay="0"><img src="../img/slide4.jpg" alt="Chania"><img src="../img/slide3.jpg" alt="Chicago"><img src="../img/slide2.jpg" alt="New York"></MARQUEE>


  <!-- Left and right controls 
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>-->


<div class="container">
    <!-- <button type="button" class="btn btn-success">Success</button> -->
    <div class="row">
      <div class="col-sm-4">
        <h1><button type=button" class="btn btn-promotion" style="font-size:25px">Promotions</button></h1>
        <p>Lorem ipsum dolor..</p>
        <p>Ut enim ad..</p>
      </div>


      <div class="col-sm-4">
        <h1><button type=button" class="btn btn-upcoming" style="font-size:25px">Upcoming</button></h1>

        <p>Lorem ipsum dolor..</p>
        <p>Ut enim ad..</p>
     </div>
      <div class="col-sm-4">
        <h1><button type=button" class="btn btn-button" style="font-size:25px">Enrol</button></h1>
        <p>Lorem ipsum dolor..</p>
        <p>Ut enim ad..</p>
     
    </div>
 </div>

</body>
</html>

/* version 1.4 */


