<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Columns Model
 *
 * @property \App\Model\Table\RowsTable|\Cake\ORM\Association\BelongsTo $Rows
 *
 * @method \App\Model\Entity\Column get($primaryKey, $options = [])
 * @method \App\Model\Entity\Column newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Column[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Column|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Column patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Column[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Column findOrCreate($search, callable $callback = null, $options = [])
 */
class ColumnsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('columns');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Rows', [
            'foreignKey' => 'row_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('content')
            ->requirePresence('content', 'create')
            ->notEmpty('content');

        $validator
            ->integer('column_order')
            ->requirePresence('column_order', 'create')
            ->notEmpty('column_order');
			
		$validator 
			->scalar('column_order_name')
			->requirePresence('column_order_name', 'create')
			->allowEmpty('column_order_name'); 

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['row_id'], 'Rows'));

        return $rules;
    }
}
