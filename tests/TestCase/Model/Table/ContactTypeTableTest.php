<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ContactTypeTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ContactTypeTable Test Case
 */
class ContactTypeTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ContactTypeTable
     */
    public $ContactType;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.contact_type',
        'app.student_contact'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ContactType') ? [] : ['className' => ContactTypeTable::class];
        $this->ContactType = TableRegistry::get('ContactType', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ContactType);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
