<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="container">
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Payment History'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Enrolments'), ['controller' => 'Enrolments', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Enrolment'), ['controller' => 'Enrolments', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="paymentHistory form large-9 medium-8 columns content">
    <?= $this->Form->create($paymentHistory) ?>
    <fieldset>
        <legend><?= __('Add Payment History') ?></legend>
        <?php
            echo $this->Form->control('enrolment_id', ['options' => $enrolments]);
            echo $this->Form->control('amount');
            echo $this->Form->control('datetime');
            echo $this->Form->control('payment_status');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
