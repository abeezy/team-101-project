<?php
namespace App\Controller;
use Cake\Event\Event;
use App\Controller\AppController;

/**
 * LessonEnrolment Controller
 *
 * @property \App\Model\Table\LessonEnrolmentTable $LessonEnrolment
 *
 * @method \App\Model\Entity\LessonEnrolment[] paginate($object = null, array $settings = [])
 */
class LessonEnrolmentController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Lessons', 'Enrolments']
        ];
        $lessonEnrolment = $this->paginate($this->LessonEnrolment);

        $this->set(compact('lessonEnrolment'));
        $this->set('_serialize', ['lessonEnrolment']);
    }

    /**
     * View method
     *
     * @param string|null $id Lesson Enrolment id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $lessonEnrolment = $this->LessonEnrolment->get($id, [
            'contain' => ['Lessons', 'Enrolments']
        ]);

        $this->set('lessonEnrolment', $lessonEnrolment);
        $this->set('_serialize', ['lessonEnrolment']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $lessonEnrolment = $this->LessonEnrolment->newEntity();
        if ($this->request->is('post')) {
            $lessonEnrolment = $this->LessonEnrolment->patchEntity($lessonEnrolment, $this->request->getData());
            if ($this->LessonEnrolment->save($lessonEnrolment)) {
                $this->Flash->success(__('The lesson enrolment has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The lesson enrolment could not be saved. Please, try again.'));
        }
        $lessons = $this->LessonEnrolment->Lessons->find('list', ['limit' => 200]);
        $enrolments = $this->LessonEnrolment->Enrolments->find('list', ['limit' => 200]);
        $this->set(compact('lessonEnrolment', 'lessons', 'enrolments'));
        $this->set('_serialize', ['lessonEnrolment']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Lesson Enrolment id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $lessonEnrolment = $this->LessonEnrolment->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $lessonEnrolment = $this->LessonEnrolment->patchEntity($lessonEnrolment, $this->request->getData());
            if ($this->LessonEnrolment->save($lessonEnrolment)) {
                $this->Flash->success(__('The lesson enrolment has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The lesson enrolment could not be saved. Please, try again.'));
        }
        $lessons = $this->LessonEnrolment->Lessons->find('list', ['limit' => 200]);
        $enrolments = $this->LessonEnrolment->Enrolments->find('list', ['limit' => 200]);
        $this->set(compact('lessonEnrolment', 'lessons', 'enrolments'));
        $this->set('_serialize', ['lessonEnrolment']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Lesson Enrolment id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $lessonEnrolment = $this->LessonEnrolment->get($id);
        if ($this->LessonEnrolment->delete($lessonEnrolment)) {
            $this->Flash->success(__('The lesson enrolment has been deleted.'));
        } else {
            $this->Flash->error(__('The lesson enrolment could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
	
	public function beforeFilter(Event $event){
		parent::beforeFilter($event);
	}
}
