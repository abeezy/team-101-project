<h1>Welcome to Dramatime</h1>

<p>Hello <?php echo $user->first_name; ?>,</p>
<p>Welcome to Dramatime...</p>

<p>You can log in at any point <a href="<?php echo $login_link; ?>">here</a> using your email address (<?php echo $user->user_email; ?>).</p>