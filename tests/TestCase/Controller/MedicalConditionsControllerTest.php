<?php
namespace App\Test\TestCase\Controller;

use App\Controller\MedicalConditionsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\MedicalConditionsController Test Case
 */
class MedicalConditionsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.medical_conditions',
        'app.students',
        'app.schools',
        'app.classes',
        'app.teachers',
        'app.users',
        'app.roles',
        'app.student_contact',
        'app.contact_type',
        'app.lessons',
        'app.lesson_enrolment',
        'app.enrolments',
        'app.payment_history',
        'app.grades'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
