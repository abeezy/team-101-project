<?php
namespace App\Controller;
use Cake\Event\Event;
use App\Controller\AppController;

/**
 * RowTemplates Controller
 *
 * @property \App\Model\Table\RowTemplatesTable $RowTemplates
 *
 * @method \App\Model\Entity\RowTemplate[] paginate($object = null, array $settings = [])
 */
class RowTemplatesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $rowTemplates = $this->paginate($this->RowTemplates);

        $this->set(compact('rowTemplates'));
        $this->set('_serialize', ['rowTemplates']);
    }

    /**
     * View method
     *
     * @param string|null $id Row Template id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $rowTemplate = $this->RowTemplates->get($id, [
            'contain' => ['Rows']
        ]);

        $this->set('rowTemplate', $rowTemplate);
        $this->set('_serialize', ['rowTemplate']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $rowTemplate = $this->RowTemplates->newEntity();
        if ($this->request->is('post')) {
            $rowTemplate = $this->RowTemplates->patchEntity($rowTemplate, $this->request->getData());
            if ($this->RowTemplates->save($rowTemplate)) {
                $this->Flash->success(__('The row template has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The row template could not be saved. Please, try again.'));
        }
        $this->set(compact('rowTemplate'));
        $this->set('_serialize', ['rowTemplate']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Row Template id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $rowTemplate = $this->RowTemplates->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rowTemplate = $this->RowTemplates->patchEntity($rowTemplate, $this->request->getData());
            if ($this->RowTemplates->save($rowTemplate)) {
                $this->Flash->success(__('The row template has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The row template could not be saved. Please, try again.'));
        }
        $this->set(compact('rowTemplate'));
        $this->set('_serialize', ['rowTemplate']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Row Template id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $rowTemplate = $this->RowTemplates->get($id);
        if ($this->RowTemplates->delete($rowTemplate)) {
            $this->Flash->success(__('The row template has been deleted.'));
        } else {
            $this->Flash->error(__('The row template could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
	
	public function beforeFilter(Event $event){
		parent::beforeFilter($event);
	}
}
