<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="container">
<div class="students form large-9 medium-8 columns content">
<h2>Add Student <small> - to existing family </small></h2> 
    <?= $this->Form->create($student) ?>
    <fieldset>

        <?php
            echo $this->Form->control('user_id', [
              'options'=>$parents
            ]);
            echo $this->Form->control('first_name');
            echo $this->Form->control('last_name');
            echo $this->Form->control('student_gender', array('type'=>'select', 'options' => array('Male' => 'Male','Female'=>'Female'), 'default' => '', 'empty'=> array('' => '-- Please Select --'), 'disabled' => ['']));
			echo $this->Form->control('grade_id', ['type' => 'select', 'options' => $grades, 'class' => 'sGradeID']);
            echo $this->Form->control('notes');
			echo $this->Form->input('signup_date', ['class' => 'datepicker-input', 'data-date-start-date' => '-13y','data-date-end-date' => '-3y', 'type' => 'text', 'format' => 'Y-m-d', 'default' => date('Y-m-d'), 'value' => !empty($student->signup_date) ? $student->signup_date->format('Y-m-d') : date('Y-m-d')]);
        ?>
    </fieldset>
				<?php echo $this->Form->button(__('Add'), array('class' => 'btn btn-light')); ?>
		<button type="button" class = 'btn btn-light' onclick="goBack()">Go Back</button>

<script>
function goBack() {
    window.history.back();
}
</script>

    <?= $this->Form->end() ?>
</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js" defer="defer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" />
