<?php echo $this->Form->create($user, array('class' => 'form-signin', 'autocomplete' => 'disabled')); ?>
 <h2 class="text-center">Sign Up</h2>
 <div class="container">
  <div class="row">
	<div class="col-auto">
	  <?php echo $this->Form->text('first_name',['class' => 'Form-control']); ?>
	  <div class="invalid-feedback">
        Please provide a valid first name.
      </div>
	</div>
	<div class="col-auto">
	  <?php echo $this->Form->text('last_name', ['class' => 'Form-control']); ?>
	  <div class="invalid-feedback">
        Please provide a valid last name.
      </div>
	</div>
  </div>

  <div class="form-row align-items-center">
    <div class="col-auto">
      <label for="User_email">Email address</label>
      <?php echo $this->Form->Email('user_email', ['class' => 'Form-control']); ?>
	  <div class="invalid-feedback">
        Please provide a valid email.
      </div>
	</div>
  </div>

  <div class="form-row align-items-center">
    <div class="col-auto">
      <label for="Password">Password</label>
      <?php echo $this->Form->Password('password', ['class' => 'Form-control']); ?>
	  <div class="invalid-feedback">
        Please provide a valid password.
      </div>
	</div>
  </div>

  <div class="form-row align-items-center">
    <div class="col-auto">
      <label for="Phone_Number">Phone Number</label>
      <?php echo $this->Form->text('phone_number', ['class' => 'Form-control']); ?>
	  <div class="invalid-feedback">
        Please provide a valid phone number.
      </div>
	</div>
  </div>

  <div class="form-row align-items-center">
	<div class="col-auto">
	  <label for="NotesInput">Additional Notes</label>
	  <?php echo $this->Form->text('notes', ['class' => 'Form-control']); ?>
	</div>
  </div>
  
  <div class="form-row align-items-center">
	<div class="col-auto">
		<button class="btn btn-lg btn-primary btn-block type="submit">Register</button>
	</div>
  </div>
 </div>
<?php echo $this->Form->end(); ?>