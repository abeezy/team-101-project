<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\StudentContact[]|\Cake\Collection\CollectionInterface $studentContact
  */
?>
<div class="container">
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Student Contact'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="studentContact index large-9 medium-8 columns content">
    <h3><?= __('Student Contact') ?></h3>
     <table class="table table-responsive table-striped bg-white">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('student_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($studentContact as $studentContact): ?>
            <tr>
                <td><?= $this->Number->format($studentContact->id) ?></td>
                <td><?= $studentContact->has('student') ? $this->Html->link($studentContact->student->first_name, ['controller' => 'Students', 'action' => 'view', $studentContact->student->id]) : '' ?></td>
                <td><?= $studentContact->has('user') ? $this->Html->link($studentContact->user->first_name, ['controller' => 'Users', 'action' => 'view', $studentContact->user->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $studentContact->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $studentContact->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $studentContact->id], ['confirm' => __('Are you sure you want to delete # {0}?', $studentContact->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator text-center">
        <ul class="pagination justify-content-center">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('<i class="fa fa-chevron-left" aria-hidden="true"></i> ' . __('Previous'), ['escape' => false]) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Next') . ' <i class="fa fa-chevron-right" aria-hidden="true"></i>', ['escape' => false]) ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p class="text-muted"><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
		
    </div>
</div>
