<form>
  <div class="row">
	<div class="col-auto">
	  <input type="text" class="form-control" data-validation="required" placeholder="First name">
	  <div class="invalid-feedback">
        Please provide a valid first name.
      </div>
	</div>
	<div class="col-auto">
	  <input type="text" class="form-control" data-validation="required" placeholder="Last name">
	  <div class="invalid-feedback">
        Please provide a valid last name.
      </div>
	</div>
  </div>
  
  <div class="form-row align-items-center">
    <div class="col-auto">
      <label for="EmailInput">Email address</label>
      <input type="email" class="form-control" data-validation="email" id="EmailInput" placeholder="name@example.com">
	  <div class="invalid-feedback">
        Please provide a valid email.
      </div>
	</div>
  </div>
  
  <div class="form-row align-items-center">
    <div class="col-auto">
      <label for="PasswordInput">Password</label>
      <input type="password" class="form-control" data-validation="required" id="PasswordInput" placeholder="Password">
	  <div class="invalid-feedback">
        Please provide a valid password.
      </div>
	</div>
  </div>
  
  <div class="form-row align-items-center">
    <div class="col-auto">
      <label for="NumberInput">Phone Number</label>
      <input type="text" data-validation="number" class="form-control" data-validation="required" id="NumberInput" placeholder="Phone Number">
	  <div class="invalid-feedback">
        Please provide a valid phone number.
      </div>
	</div>
  </div>
  
  <div class="form-row align-items-center">
	<div class="col-auto">
      <label for="RoleName">What type of user are you?</label>
      <select class="form-control" data-validation="required" id="RoleName">
       	<option selected>Choose...</option>
		<option value="1">Parent</option>
        <option value="2">Teacher</option>
        <option value="3">Admin</option>
      </select>
	</div>
  </div>
  
  <div class="form-row align-items-center">
    <label class="form-check-label">
      <input class="form-check-input" type="checkbox" value="">
		Select to register a second Parent/Guardian
    </label>
  </div>
	
  <div class="form-row align-items-center">
	<div class="col-auto">
	  <label for="NotesInput">Additional Notes</label>
	  <textarea class="form-control" id="NotesInput" rows="3" placeholder="Medical Information or any other notes"></textarea>
	</div>
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>