<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * carouselItems Model
 *
 * @property \App\Model\Table\ClassesTable|\Cake\ORM\Association\HasMany $Classes
 * @property \App\Model\Table\StudentsTable|\Cake\ORM\Association\HasMany $Students
 *
 * @method \App\Model\Entity\carouselItem get($primaryKey, $options = [])
 * @method \App\Model\Entity\carouselItem newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\carouselItem[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\carouselItem|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\carouselItem patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\carouselItem[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\carouselItem findOrCreate($search, callable $callback = null, $options = [])
 */
class CarouselItemsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('carousel_items');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Webpages', [
            'foreignKey' => 'webpage_id'
        ]);
      
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        

        return $validator;
    }

    // public function beforeFind ($event, $query, $options, $primary) {
      // $order = $query->clause('order');
      // if ($order === null || !count($order)) {
          // $query->order( [$this->alias() . '.name' => 'ASC'] );
      // }
    // }
}
