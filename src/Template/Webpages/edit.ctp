<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $webpage->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $webpage->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Webpages'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<?= $this->Form->create($webpage) ?>
			<fieldset>
				<legend><?= __('Edit Webpage') ?></legend>
				<?php
					echo $this->Form->control('url');
				?>
			</fieldset>
			<?= $this->Form->button(__('Submit')) ?>
			<?= $this->Form->end() ?>
		</div>
	</div>
</div>