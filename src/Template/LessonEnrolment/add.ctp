<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Lesson Enrolment'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Lessons'), ['controller' => 'Lessons', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Lesson'), ['controller' => 'Lessons', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Enrolments'), ['controller' => 'Enrolments', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Enrolment'), ['controller' => 'Enrolments', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="lessonEnrolment form large-9 medium-8 columns content">
    <?= $this->Form->create($lessonEnrolment) ?>
    <fieldset>
        <legend><?= __('Add Lesson Enrolment') ?></legend>
        <?php
            echo $this->Form->control('lesson_id', ['options' => $lessons]);
            echo $this->Form->control('enrolment_id', ['options' => $enrolments]);
            echo $this->Form->control('attended');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
