<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Webpage[]|\Cake\Collection\CollectionInterface $webpages
  */
?>
<div class="container">

<div class="btn-group mb-3">
		<?php echo $this->Html->link('<i class="fas fa-plus"></i>&nbsp;&nbsp;New Webpage', ['action' => 'add'], ['class' => 'btn btn-outline-primary', 'escape' => false]) ?></li>
	</div>
<div class="webpages index large-9 medium-8 columns content">
    <h3><?= __('Webpages') ?></h3>
 <table class="table table-responsive table-striped bg-white ">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('url') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($webpages as $webpage): ?>
            <tr>
                <td><?= $this->Number->format($webpage->id) ?></td>
                <td><?= h($webpage->url) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $webpage->id]) ?>
                    <!--<?= $this->Html->link(__('Edit'), ['action' => 'edit', $webpage->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $webpage->id], ['confirm' => __('Are you sure you want to delete # {0}?', $webpage->id)]) ?>-->
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator text-center">
        <ul class="pagination justify-content-center">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('<i class="fa fa-chevron-left" aria-hidden="true"></i> ' . __('Previous'), ['escape' => false]) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Next') . ' <i class="fa fa-chevron-right" aria-hidden="true"></i>', ['escape' => false]) ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>

        </ul>
        <p class="text-muted"><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
</div>