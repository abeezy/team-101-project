<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Lesson[]|\Cake\Collection\CollectionInterface $lessons
  */
?>
<div class="container">

<div class="lessons index large-9 medium-8 columns content">
    <h3><?= __('Lessons') ?></h3>
    <table class="table table-responsive table-striped bg-white ">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('class_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('teacher_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('lesson_date') ?></th>
				<th scope="col"><?= $this->Paginator->sort('notes') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($lessons as $lesson): ?>
            <tr>
                <td><?= $this->Number->format($lesson->id) ?></td>
                <td><?= $lesson->has('class') ? $this->Html->link($lesson->class->name, ['controller' => 'Classes', 'action' => 'view', $lesson->class->id]) : '' ?></td>
                <td><?= $lesson->has('teacher') ? $this->Html->link($lesson->teacher->user->first_name . ' ' . $lesson->teacher->user->last_name, ['controller' => 'Teachers', 'action' => 'view', $lesson->teacher->id]) : '' ?></td>
                <td><?= $lesson->lesson_date->i18nFormat('dd/MM/yyyy') ?></td>
				<td><?= $lesson->notes ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'teacher-view', $lesson->id]) ?>
                    
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator text-center">
        <ul class="pagination justify-content-center">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('<i class="fa fa-chevron-left" aria-hidden="true"></i> ' . __('Previous'), ['escape' => false]) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Next') . ' <i class="fa fa-chevron-right" aria-hidden="true"></i>', ['escape' => false]) ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p class="text-muted"><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
