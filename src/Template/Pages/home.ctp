<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
	<ol class="carousel-indicators">
		<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
		<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
		<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
	</ol>
	<!--<div class="container">-->
	<div class="carousel-inner">
		<?php $first = true; ?>
		<?php foreach($webpage->carousel_items as $carouselItem): ?>
			<div class="carousel-item <?php if ($first){ echo "active"; $first = false; } ?>">
				<?php echo $this->Html->image($carouselItem['image'], ['class' => 'd-block w-100']);?>
				<div class="carousel-caption d-none d-md-block">
					<?php echo $this->Html->link('Enrol Now <i class="fa fa-chevron-right" aria-hidden="true"></i>', array('controller' => 'classes', 'action' => 'all'), array('class' => 'btn btn-lg btn-lg-round btn-warning', 'escape' => false)); ?>
				</div>
			</div>
		<?php endforeach; ?>
	<!--
		<div class="carousel-item active">
			<?php echo $this->Html->image('slide2.jpg', ['class' => 'd-block w-100']);?>
			<div class="carousel-caption d-none d-md-block">
				<?php echo $this->Html->link('Enrol Now <i class="fa fa-chevron-right" aria-hidden="true"></i>', array('controller' => 'classes', 'action' => 'all'), array('class' => 'btn btn-lg btn-lg-round btn-success', 'escape' => false)); ?>
			</div>
		</div>
		<div class="carousel-item">
			<?php echo $this->Html->image('slide1.jpg', ['class' => 'd-block w-100']);?>
			<div class="carousel-caption d-none d-md-block">
				<?php echo $this->Html->link('Enrol Now <i class="fa fa-chevron-right" aria-hidden="true"></i>', array('controller' => 'classes', 'action' => 'all'), array('class' => 'btn btn-lg btn-lg-round btn-warning', 'escape' => false)); ?>
			</div>
		</div>
		<div class="carousel-item">
			<?php echo $this->Html->image('slide4.jpg', ['class' => 'd-block w-100']);?>
			<div class="carousel-caption d-none d-md-block">
				<?php echo $this->Html->link('Enrol Now <i class="fa fa-chevron-right" aria-hidden="true"></i>', array('controller' => 'classes', 'action' => 'all'), array('class' => 'btn btn-lg btn-lg-round btn-info', 'escape' => false)); ?>
			</div>
		</div>-->
	</div>
	<!--</div>-->
	<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
		<span class="carousel-control-prev-icon" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>
	<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
		<span class="carousel-control-next-icon" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	</a>
</div>
<div class="container">
	<?php echo $this->cell('CMS', [$page]); ?>

</div><!-- /.container -->

