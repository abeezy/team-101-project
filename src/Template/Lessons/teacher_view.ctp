<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Lesson $lesson
  */
?>

<div class="container">

<div class="lessons view large-9 medium-8 columns content">	
	<?php echo $this->Form->create($lesson); ?>
	<h1><?php echo $lesson['class']['school']['school_name']; ?> - <?php echo $lesson['class']['name']; ?></h1>
	<h2>Student Attendance - <?php echo $lesson['lesson_date']->i18nFormat('dd/MM/yyyy'); ?></h2>
	<table class="table table-responsive table-striped bg-white">
		<thead>
			<tr>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Attendance</th>
				<th>Notes</th>
			</tr>
		</thead>
		<tbody>
	        <?php
				$index = 0;
				foreach ($lesson['lesson_enrolment'] as $lesson_enrolment):
              $student = $lesson_enrolment['enrolment']['student']; ?>
			  <tr>
			  <td><?php echo $student->first_name;?></td>
			  <td><?php echo $student->last_name;?></td>
				<td>
					<?php echo $this->Form->control('lesson_enrolment.' . $index . '.id', array('type' => 'hidden', 'value' => $lesson_enrolment->id)); ?>
					<?php echo $this->Form->control('lesson_enrolment.' . $index . '.attended', array(
						'type' => 'radio',
						'label' => false,
						'options' => array(
							['text' => 'Yes', 'value' => 1],
							['text' => 'No', 'value' => 0]
						),
						'value' => $lesson_enrolment->attended
					)); ?>
					
				</td>
				<td><?php echo $this->Form->control('lesson_enrolment.' . $index . '.notes', array('label' => false));?></td>
			  </tr>
			<?php
				$index++;
				endforeach;
			?>
		</tbody>
	</table>
	
	<div class="row">
		<div class="col">
			<?php echo $this->Form->control('notes', array('label' => 'Lesson Notes')); ?>
		</div>
	</div>
	<div class="text-right">
		<button type="submit" class="btn btn-light">Save</button>
	    <?php echo $this->Html->link('Go Back', array('controller' => 'classes', 'action' => 'teacherView', $lesson['class_id']), array('class' => 'btn btn-light')); ?>
	</div>
	<?php echo $this->Form->end(); ?>
</div>
</div>