<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="container">
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Teachers'), ['action' => 'index']) ?></li>
    </ul>
</nav>
	<div class="row justify-content-center">
		<div class="col-md-4">
    <?= $this->Form->create($teacher) ?>
    <fieldset>
        <legend><?= __('Add Teacher') ?></legend>
        
            	<?php echo $this->Form->control('first_name',['type' => 'text', 'placeholder' => 'John']); ?>
				<?php echo $this->Form->control('last_name', ['type' => 'text', 'placeholder' => 'Smith']); ?>
				<?php echo $this->Form->control('user_email', ['type' => 'email', 'placeholder' => 'JohnSmith@gmail.com']); ?>
				<?php echo $this->Form->control('password', ['type' => 'password', 'minLength' => 8]); ?>
				<?php echo $this->Form->control('phone_number', ['type' => 'tel', 'placeholder' => '0400 000 123']); ?>
				<?php echo $this->Form->control('bio', ['type' => 'textarea', 'placeholder'=>'Biography']); ?>
        
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
</div>
</div>
