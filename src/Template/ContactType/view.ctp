<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\ContactType $contactType
  */
?>

<div class="container">
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Contact Type'), ['action' => 'edit', $contactType->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Contact Type'), ['action' => 'delete', $contactType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $contactType->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Contact Type'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Contact Type'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Student Contact'), ['controller' => 'StudentContact', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Student Contact'), ['controller' => 'StudentContact', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="contactType view large-9 medium-8 columns content">
    <h3><?= h($contactType->name) ?></h3>
    <table class="table table-responsive table-striped bg-white">
        <tr>
            <th scope="row"><?= __('Student Contact') ?></th>
            <td><?= $contactType->has('student_contact') ? $this->Html->link($contactType->student_contact->id, ['controller' => 'StudentContact', 'action' => 'view', $contactType->student_contact->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($contactType->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($contactType->id) ?></td>
        </tr>
    </table>
</div>
