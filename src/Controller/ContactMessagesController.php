<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
/**
 * ContactMessages Controller
 *
 * @property \App\Model\Table\ContactMessagesTable $ContactMessages
 *
 * @method \App\Model\Entity\ContactMessage[] paginate($object = null, array $settings = [])
 */
class ContactMessagesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $contactMessages = $this->paginate($this->ContactMessages);

        $this->set(compact('contactMessages'));
        $this->set('_serialize', ['contactMessages']);
    }

    /**
     * View method
     *
     * @param string|null $id Contact Message id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $contactMessage = $this->ContactMessages->get($id, [
            'contain' => []
        ]);

        $this->set('contactMessage', $contactMessage);
        $this->set('_serialize', ['contactMessage']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $contactMessage = $this->ContactMessages->newEntity();
        if ($this->request->is('post')) {
            $contactMessage = $this->ContactMessages->patchEntity($contactMessage, $this->request->getData());
            if ($this->ContactMessages->save($contactMessage)) {
                $this->Flash->success(__('The contact message has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The contact message could not be saved. Please, try again.'));
        }
        $this->set(compact('contactMessage'));
        $this->set('_serialize', ['contactMessage']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Contact Message id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        // $contactMessage = $this->ContactMessages->get($id, [
            // 'contain' => []
        // ]);
        // if ($this->request->is(['patch', 'post', 'put'])) {
            // $contactMessage = $this->ContactMessages->patchEntity($contactMessage, $this->request->getData());
            // if ($this->ContactMessages->save($contactMessage)) {
                // $this->Flash->success(__('The contact message has been saved.'));

                // return $this->redirect(['action' => 'index']);
            // }
            // $this->Flash->error(__('The contact message could not be saved. Please, try again.'));
        // }
        // $this->set(compact('contactMessage'));
        // $this->set('_serialize', ['contactMessage']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Contact Message id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $contactMessage = $this->ContactMessages->get($id);
        if ($this->ContactMessages->delete($contactMessage)) {
            $this->Flash->success(__('The contact message has been deleted.'));
        } else {
            $this->Flash->error(__('The contact message could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
	
	public function contactUs() {
		$contactMessage = $this->ContactMessages->newEntity();

        if ($this->request->is('post')) {
            $messages = $this->ContactMessages->newEntity($this->request->getData());

			if($this->captcha($this->request->getData('g-recaptcha-response'))) {
				if ($this->ContactMessages->save($messages)) {
					$this->Flash->success(__('The message has been sent.'));
					
					return $this->redirect(['controller' => 'contactMessages', 'action' => 'contactUs']);
				}
				$this->Flash->error(__('The message could not be sent. Please, try again.'));
			} else {
				$this->Flash->error(__('The Recaptcha could not be verified. Please, try again.'));
			}
				// echo "<pre>";
				// print_r($user);
				// echo "</pre>";

				// exit;

        }
        $this->set(compact('contactMessage'));
		$this->set('page', 'Contact Us');
        $this->set('_serialize', ['contactMessage']);		
	}
	
	public function beforeFilter(Event $event){
		$this->Auth->allow(['contactUs']);
		parent::beforeFilter($event);
	}
}
