<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Column $column
  */
?>
<div class="container">
	
	<div class="columns view large-9 medium-8 columns content">
		
		<div class="row">
			<div class="col">
				<h2><?= __('Content Preview') ?></h2>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<div class="card">
					<?= $column->content; ?>
				</div>
			</div>
		</div>
	</div>
	<br>
		<button type="button" class = 'btn btn-light' onclick="goBack()">Go Back</button>

<script>
function goBack() {
    window.history.back();
}
</script>
</div>