$(function() {

	function getLessons(class_id) {
		$.ajax(document.getElementsByTagName('base')[0].href + 'classes/upcoming_lessons/' + class_id, {
			'dataType': 'json'
		}).done(function(class_info) {
			console.log('class_info:', class_info);
			var tempStore = [],
				i = 1;

			$('.sWeeksEnrolled').empty().append($('<option disabled="disabled" value="" selected="selected"> -- Please Select an Option -- </option>'));

			class_info.lessons.forEach(function(lesson) {

				tempStore.push(lesson);
			});
			
			var skipRest = false;

			class_info.lessons.forEach(function(lesson) {
				if(!skipRest) {
					if (new Date(tempStore[0].lesson_date) > new Date()) {
						//return true; 
						skipRest = true;
					}	
					//else {
						var formattedDate = new Date(tempStore[0].lesson_date).toLocaleDateString('en-AU', { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' });

						var tempStore2 = [];

						tempStore.forEach(function(lesson) {
							tempStore2.push(lesson.id);
						});

						$('.sWeeksEnrolled').append($('<option>', {
							value: tempStore2,
							text: formattedDate + ' Onwards - ' + tempStore2.length + ' lessons: $' + roundr(tempStore2.length * class_info.price_per_lesson) + ''
						}));

						i++;

						tempStore.splice(0, 1);
					//}
				}
			});
		});
	}

	$('.sClassID').on('change', function(e) {
		getLessons($(this).val());
	});

	$('.sClassID').each(function (e) {
		if($(this).val() !== "") {
			getLessons($(this).val());
		}
	});

	$('#addStudent').on('click', function(e) {
		var newNumber = Math.round( Math.random()*10000000 );
		var newId = 'students_' + newNumber;
		$('#studentContainer > .card:first-child').clone().attr('id', newId).appendTo('#studentContainer');

		$('#' + newId + ' input').val('');
		$('#' + newId + ' select').val('');
		$('#' + newId + ' textarea').val('');

		$('#' + newId + ' .sClassID').attr('name', 'students[' + newNumber + '][class_id]');
		$('#' + newId + ' .sWeeksEnrolled').attr('name', 'students[' + newNumber + '][weeks_enrolled]');
		$('#' + newId + ' .sFName').attr('name', 'students[' + newNumber + '][first_name]');
		$('#' + newId + ' .sLName').attr('name', 'students[' + newNumber + '][last_name]');
		$('#' + newId + ' .sGender').attr('name', 'students[' + newNumber + '][student_gender]');
		$('#' + newId + ' .sGradeID').attr('name', 'students[' + newNumber + '][grade_id]');
		$('#' + newId + ' .sNotes').attr('name', 'students[' + newNumber + '][notes]');
	});

	$(document).on('click', '.removeStudentButton', function(e) {
		$(e.currentTarget).closest('.card').remove();
	});
	
	function roundr(incoming) {
		// Rounds values to 2 decimal places, completely cutting off any excess, so it does not interfere with figures
		return ((Math.round(incoming*100)/100).toFixed(2));
	}
});
