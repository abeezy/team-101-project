<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="container">

<div class="row justify-content-center">
		<div class="col-md-4">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Edit User') ?></legend>
        <?php
            echo $this->Form->control('first_name');
            echo $this->Form->control('last_name');
            echo $this->Form->control('phone_number');
			echo $this->Form->control('notes');
        ?>
    </fieldset>

     <?php echo $this->Form->button(__('Save'), array('class' => 'btn btn-light')); ?>
		<button type="button" class = 'btn btn-light' onclick="goBack()">Go Back</button>

<script>
function goBack() {
    window.history.back();
}
</script>
    <?= $this->Form->end() ?>

</div>
</div>
</div>
