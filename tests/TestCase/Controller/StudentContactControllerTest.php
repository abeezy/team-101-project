<?php
namespace App\Test\TestCase\Controller;

use App\Controller\StudentContactController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\StudentContactController Test Case
 */
class StudentContactControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.student_contact',
        'app.students',
        'app.schools',
        'app.classes',
        'app.teachers',
        'app.enrolments',
        'app.payment_history',
        'app.lessons',
        'app.attendences',
        'app.users',
        'app.contact_type'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
