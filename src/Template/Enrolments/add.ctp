<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="container">
<div class="students form large-9 medium-8 columns content">
<h2>Add Enrolment</h2>
    <?= $this->Form->create($enrolment) ?>
    <fieldset>
        <legend><?= __('Add Enrolment') ?></legend>
        <?php
            echo $this->Form->control('student_id', ['options' => $students, 'value' => $student_id]);
            echo $this->Form->control('class_id', ['options' => $classes]);
     		echo $this->Form->input('enrolment_date', ['class' => 'datepicker-input', 'data-date-start-date' => '-0d', 'type' => 'text', 'format' => 'Y-m-d', 'default' => date('Y-m-d'), 'value' => !empty($enrolment->enrolment_date) ? $enrolment->enrolment_date->format('Y-m-d') : date('Y-m-d')]);
            echo $this->Form->control('no_of_weeks_enrolled');
        ?>
    </fieldset>
    <?php echo $this->Form->button(__('Add'), array('class' => 'btn btn-light')); ?>
		<button type="button" class = 'btn btn-light' onclick="goBack()">Go Back</button>

<script>
function goBack() {
    window.history.back();
}
</script>
    <?= $this->Form->end() ?>
</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js" defer="defer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" />
