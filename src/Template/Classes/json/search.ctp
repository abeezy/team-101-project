<?php
	$output_array = [];
	foreach($classes as $class){
		array_push($output_array, array(
			'label' => $class->name . ' (' . $class->school->school_name . ' - ' . $class->class_address . ')',
			'value' => $class->id,
			'category' => 'Classes',
			'url' => $this->Url->build(['controller' => 'classes', 'action' => 'view', $class->id], true)
		));
	}
	echo json_encode($output_array);
?>