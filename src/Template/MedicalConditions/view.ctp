<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\MedicalCondition $medicalCondition
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Medical Condition'), ['action' => 'edit', $medicalCondition->medid]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Medical Condition'), ['action' => 'delete', $medicalCondition->medid], ['confirm' => __('Are you sure you want to delete # {0}?', $medicalCondition->medid)]) ?> </li>
        <li><?= $this->Html->link(__('List Medical Conditions'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Medical Condition'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Students'), ['controller' => 'Students', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Student'), ['controller' => 'Students', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="medicalConditions view large-9 medium-8 columns content">
    <h3><?= h($medicalCondition->medid) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Medical Conditions') ?></th>
            <td><?= h($medicalCondition->medical_conditions) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Medical Id') ?></th>
            <td><?= $this->Number->format($medicalCondition->medical_id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Students') ?></h4>
        <?php if (!empty($medicalCondition->students)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('School Id') ?></th>
                <th scope="col"><?= __('First Name') ?></th>
                <th scope="col"><?= __('Last Name') ?></th>
                <th scope="col"><?= __('Dob') ?></th>
                <th scope="col"><?= __('Notes') ?></th>
                <th scope="col"><?= __('Signup Date') ?></th>
                <th scope="col"><?= __('Grade Id') ?></th>
                <th scope="col"><?= __('Medical Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($medicalCondition->students as $students): ?>
            <tr>
                <td><?= h($students->id) ?></td>
                <td><?= h($students->school_id) ?></td>
                <td><?= h($students->first_name) ?></td>
                <td><?= h($students->last_name) ?></td>
                <td><?= h($students->dob) ?></td>
                <td><?= h($students->notes) ?></td>
                <td><?= h($students->signup_date) ?></td>
                <td><?= h($students->grade_id) ?></td>
                <td><?= h($students->medical_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Students', 'action' => 'view', $students->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Students', 'action' => 'edit', $students->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Students', 'action' => 'delete', $students->id], ['confirm' => __('Are you sure you want to delete # {0}?', $students->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
