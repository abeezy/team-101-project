<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Webpage $webpage
  */
?>
<div class="container">

	<nav class="large-3 medium-4 columns" id="actions-sidebar">
		<ul class="side-nav">
			<li class="heading"><?= __('Actions') ?></li>
			<!--<li><?= $this->Html->link(__('Edit Webpage'), ['action' => 'edit', $webpage->id]) ?> </li>
			<li><?= $this->Form->postLink(__('Delete Webpage'), ['action' => 'delete', $webpage->id], ['confirm' => __('Are you sure you want to delete # {0}?', $webpage->id)]) ?> </li>
			-->
			<li><?= $this->Html->link(__('Add Rows'), ['controller' => 'Rows', 'action' => 'add', $webpage->id]) ?></li>
		</ul>
	</nav>
	<div class="webpages view large-9 medium-8 columns content">
		<h3><?= h($webpage->url) ?></h3>
		<table class="table table-responsive table-striped bg-white">
			<thead>
				<tr>
					<th>Row Number</th>
					<th>Row Description</th>
					<th>Template</th>
					<th>Columns</th>
					<th>&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				<?php $i = 1; ?> 
				<?php foreach($webpage->rows as $row): ?>
					<tr>
						<td><?php echo $i; ?></td>
						<td><?php echo $row->row_description; ?></td>
						<td><?php echo $row->row_template->name; ?></td>
						<td><?php echo $row->row_template->column_count; ?></td>
						<td><?php echo $this->Html->link('View', array('controller' => 'Rows', 'action' => 'view', $row->id)); ?></td>
						<td><?php echo $this->Html->link('Edit',array('controller' => 'Rows', 'action' => 'edit', $row->id)); ?></td>
					</tr>
				<?php $i++; endforeach; ?>
			</tbody>
		</table>
	</div>
	<div class="related mb-3">
        <div class="card">
			<div class="card-header">
				<div class="row justify-content-between align-items-center">
					<div class="col-auto">
						<h4 class="mb-0">Carousel Items</h4>
					</div>
					<div class="col-auto">
						<?php echo $this->Html->link('<i class="fas fa-plus"></i>&nbsp;&nbsp;Add New', ['controller' => 'carouselItems', 'action' => 'add', $webpage->id], ['class' => 'btn btn-outline-primary', 'escape' => false]) ?></li>
					</div>
				</div>
			</div>
			<table class="table table-responsive table-striped mb-0">
				<thead>
					<tr>
						<th scope="col"><?= __('Name') ?></th>
						<th scope="col"><?= __('Image') ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($webpage->carousel_items as $carouselItem): ?>
							<tr>
								<td><?php echo $carouselItem->name; ?></td>
								<td><?php if($carouselItem->image): ?><em>Yes</em><?php endif; ?></td>
								<td><?php echo $this->Html->link('View', array('controller' => 'carouselItems', 'action' => 'view', $carouselItem->id)); ?></td>
							</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
			<button type="button" class = 'btn btn-light' onclick="goBack()">Go Back</button>

<script>
function goBack() {
    window.history.back();
}
</script>
</div>