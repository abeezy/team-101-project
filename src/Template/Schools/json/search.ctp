<?php
	$output_array = [];
	foreach($schools as $school){
		array_push($output_array, array(
			'label' => $school->school_name . ' (' . $school->school_address . ')',
			'value' => $school->id,
			'category' => 'Schools',
			'url' => $this->Url->build(['controller' => 'schools', 'action' => 'view', $school->id], true)
		));
	}
	echo json_encode($output_array);
?>