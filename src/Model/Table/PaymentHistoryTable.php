<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PaymentHistory Model
 *
 * @property \App\Model\Table\EnrolmentsTable|\Cake\ORM\Association\BelongsTo $Enrolments
 *
 * @method \App\Model\Entity\PaymentHistory get($primaryKey, $options = [])
 * @method \App\Model\Entity\PaymentHistory newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PaymentHistory[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PaymentHistory|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PaymentHistory patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PaymentHistory[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PaymentHistory findOrCreate($search, callable $callback = null, $options = [])
 */
class PaymentHistoryTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('payment_history');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('PaymentHistoryEnrolment');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('amount')
            ->requirePresence('amount', 'create')
            ->notEmpty('amount');

        $validator
            ->dateTime('datetime')
            ->requirePresence('datetime', 'create')
            ->notEmpty('datetime');

        $validator
            ->scalar('payment_status')
            ->requirePresence('payment_status', 'create')
            ->notEmpty('payment_status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {

        return $rules;
    }
}
