<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Row $row
  */
?>
<div class="container">
	<nav class="large-3 medium-4 columns" id="actions-sidebar">
		<ul class="side-nav">
			<li class="heading"><?= __('Actions') ?></li>
			<li><?= $this->Form->postLink(__('Delete Row'), ['action' => 'delete', $row->id], ['confirm' => __('Are you sure you want to delete # {0}?', $row->id)]) ?> </li>
		</ul>
	</nav>
	<div class="rows view large-9 medium-8 columns content">
		<table class="table table-responsive table-striped bg-white">
			<tr>
				<th scope="row"><?= __('Webpage') ?></th>
				<td><?= $row->has('webpage') ? $this->Html->link($row->webpage->url, ['controller' => 'Webpages', 'action' => 'view', $row->webpage->id]) : '' ?></td>
			</tr>
			<tr>
				<th scope="row"><?= __('Row Template') ?></th>
				<td><?= $row->has('row_template') ?></td>
			</tr>
			<tr>
				<th scope="row"><?= __('Row Description') ?></th>
				<td><?= $row->has('row_description') ?></td>
			</tr>
		</table>
		<div class="related">
			<h4><?= __('Related Columns') ?></h4>
			<?php if (!empty($row->columns)): ?>
			<table class="table table-responsive table-striped bg-white">
				<tr>
					<th scope="col"><?= __('Column Order') ?></th>
					<th scope="col" class="actions"><?= __('Actions') ?></th>
				</tr>
				<?php foreach ($row->columns as $columns): ?>
					<tr>
						<td><?= h($columns->column_order) ?></td>
						<td class="actions">
							<?= $this->Html->link(__('View'), ['controller' => 'Columns', 'action' => 'view', $columns->id]) ?>
							<?= $this->Html->link(__('Edit'), ['controller' => 'Columns', 'action' => 'edit', $columns->id]) ?>
						</td>
					</tr>
				<?php endforeach; ?>
			</table>
			<?php endif; ?>
		</div>
	</div>
	 
			<button type="button" class = 'btn btn-light' onclick="goBack()">Go Back</button>

<script>
function goBack() {
    window.history.back();
}
</script>
</div>