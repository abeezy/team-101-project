<?php
namespace App\Controller;
use Cake\Event\Event;
use App\Controller\AppController;

/**
 * StudentContact Controller
 *
 * @property \App\Model\Table\StudentContactTable $StudentContact
 *
 * @method \App\Model\Entity\StudentContact[] paginate($object = null, array $settings = [])
 */
class StudentContactController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Students', 'Users']
        ];
        $studentContact = $this->paginate($this->StudentContact);

        $this->set(compact('studentContact'));
        $this->set('_serialize', ['studentContact']);
    }

    /**
     * View method
     *
     * @param string|null $id Student Contact id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $studentContact = $this->StudentContact->get($id, [
            'contain' => ['Students', 'Users', 'ContactType']
        ]);

        $this->set('studentContact', $studentContact);
        $this->set('_serialize', ['studentContact']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $studentContact = $this->StudentContact->newEntity();
        if ($this->request->is('post')) {
            $studentContact = $this->StudentContact->patchEntity($studentContact, $this->request->getData());
            if ($this->StudentContact->save($studentContact)) {
                $this->Flash->success(__('The student contact has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The student contact could not be saved. Please, try again.'));
        }
        $students = $this->StudentContact->Students->find('list', ['limit' => 200]);
        $users = $this->StudentContact->Users->find('list', ['limit' => 200]);
        $this->set(compact('studentContact', 'students', 'users'));
        $this->set('_serialize', ['studentContact']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Student Contact id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $studentContact = $this->StudentContact->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $studentContact = $this->StudentContact->patchEntity($studentContact, $this->request->getData());
            if ($this->StudentContact->save($studentContact)) {
                $this->Flash->success(__('The student contact has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The student contact could not be saved. Please, try again.'));
        }
        $students = $this->StudentContact->Students->find('list', ['limit' => 200]);
        $users = $this->StudentContact->Users->find('list', ['limit' => 200]);
        $this->set(compact('studentContact', 'students', 'users'));
        $this->set('_serialize', ['studentContact']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Student Contact id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $studentContact = $this->StudentContact->get($id);
        if ($this->StudentContact->delete($studentContact)) {
            $this->Flash->success(__('The student contact has been deleted.'));
        } else {
            $this->Flash->error(__('The student contact could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
	
	public function beforeFilter(Event $event){
		parent::beforeFilter($event);
	}
}
