<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="container">
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    
</nav>
<div class="row justify-content-center">
		<div class="col-md-4">
    <?= $this->Form->create($teacher) ?>
    <fieldset>
        <legend><?= __('Edit Teacher') ?></legend>
        <?php
            echo $this->Form->control('user_id', ['options' => $users]);
            echo $this->Form->control('bio');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
</div>