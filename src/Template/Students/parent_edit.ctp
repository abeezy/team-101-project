<div class="container">
<div class="row justify-content-center">
		<div class="col-md-4">
	<div class="students form large-9 medium-8 columns content">
		<?= $this->Form->create($student) ?>
		<fieldset>
			<legend><?= __('Edit Student') ?></legend>
			<?php
				//echo $this->Form->control('school_id', ['options' => $schools]);
				echo $this->Form->control('first_name');
				echo $this->Form->control('last_name');
				echo $this->Form->control('student_gender', array('type'=>'select', 'options' => array('Male','Female'), 'default' => '', 'empty'=> array('' => '-- Please Select --'), 'disabled' => ['']));
				echo $this->Form->control('notes');


			?>


		<?= $this->Form->button (__('Update')) ?>
			<button type="button" class = 'btn btn-light' onclick="goBack()">Go Back</button>

<script>
function goBack() {
    window.history.back();
}
</script>

		<?= $this->Form->end() ?>
	</div>
</div>
</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js" defer="defer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" />
