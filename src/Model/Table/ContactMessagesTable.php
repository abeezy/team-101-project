<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ContactMessages Model
 *
 * @method \App\Model\Entity\ContactMessage get($primaryKey, $options = [])
 * @method \App\Model\Entity\ContactMessage newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ContactMessage[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ContactMessage|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ContactMessage patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ContactMessage[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ContactMessage findOrCreate($search, callable $callback = null, $options = [])
 */
class ContactMessagesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('contact_messages');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
		
		$this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('first_name')
            ->requirePresence('first_name', 'create')
            ->notEmpty('first_name');

        $validator
            ->scalar('last_name')
            ->requirePresence('last_name', 'create')
            ->notEmpty('last_name');

        $validator
            ->scalar('user_email')
            ->requirePresence('user_email', 'create')
            ->notEmpty('user_email');

        $validator
            ->scalar('message_details')
            ->requirePresence('message_details', 'create')
            ->notEmpty('message_details');

        return $validator;
    }
}
