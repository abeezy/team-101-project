<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Enrolment Entity
 *
 * @property int $id
 * @property int $student_id
 * @property int $class_id
 * @property \Cake\I18n\FrozenDate $enrolment_date
 * @property string $year
 * @property string $term
 * @property int $no_of_weeks_enrolled
 *
 * @property \App\Model\Entity\Student $student
 * @property \App\Model\Entity\Class $class
 * @property \App\Model\Entity\PaymentHistory[] $payment_history
 */
class Enrolment extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
