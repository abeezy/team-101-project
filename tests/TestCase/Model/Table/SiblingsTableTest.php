<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SiblingsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SiblingsTable Test Case
 */
class SiblingsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SiblingsTable
     */
    public $Siblings;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.siblings',
        'app.students',
        'app.attendences',
        'app.lessons',
        'app.teachers',
        'app.enrolments',
        'app.student_contact',
        'app.contacts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Siblings') ? [] : ['className' => SiblingsTable::class];
        $this->Siblings = TableRegistry::get('Siblings', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Siblings);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
