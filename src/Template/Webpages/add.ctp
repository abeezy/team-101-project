<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="container">
	<h2>Add Webpages</h2>
	<div class="webpages form large-9 medium-8 columns content">
		<?= $this->Form->create($webpage) ?>
		<fieldset>
		
			<?php
				echo $this->Form->control('url');
			?>
		</fieldset>
		 <?php echo $this->Form->button(__('Add'), array('class' => 'btn btn-light')); ?>
		<button type="button" class = 'btn btn-light' onclick="goBack()">Go Back</button>

<script>
function goBack() {
    window.history.back();
}
</script>
		<?= $this->Form->end() ?>
	</div>
</div>