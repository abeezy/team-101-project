<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="container">
<div class="students form large-9 medium-8 columns content">

			<h2>Add School</h2>
			<?php echo $this->Form->create($school, ['enctype' => 'multipart/form-data']); ?>
				<?php
					echo $this->Form->control('school_name');
					echo $this->Form->control('school_address');
					echo $this->Form->control('image', ['type' => 'hidden']);
				?>
				<div class="form-group">
					<label>Image</label>
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#imageModal">Select Image</button>
				</div>
				<?php echo $this->Form->button(__('Add'), array('class' => 'btn btn-light')); ?>
				<button type="button" class = 'btn btn-light' onclick="goBack()">Go Back</button>

<script>
function goBack() {
    window.history.back();
}
</script>
			<?php echo $this->Form->end() ?>
		</div>
	</div>

<div class="modal" tabindex="-1" role="dialog" id="imageModal">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Select Image</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		<iframe style="width: 100%; height: 500px;" src="/responsive_filemanager/filemanager/dialog.php?type=1&field_id=image"></iframe>
		<script type="text/javascript">
			function responsive_filemanager_callback(field_id){
				console.log(field_id);
				var url=jQuery('#'+field_id).val();
				//alert('update '+field_id+" with "+url);
				//your code
			}
		</script>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</div>
