<?php
/**
  * @var \App\View\AppView $this
  * @var \Cake\Datasource\EntityInterface $class
  */
?>

<div class="container">


<div class="classes view large-9 medium-8 columns content">
    <h3><?= h($class->name) ?></h3>
    <table class="table table-responsive table-striped bg-white">
        <tr>
            <th scope="row"><?= __('Class Name') ?></th>
            <td><?= h($class->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('School') ?></th>
            <td><?php echo $class->school->school_name; ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Class Location') ?></th>
            <td><?= h($class->class_address) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Teacher') ?></th>
            <td><?php echo $class->teacher->user->full_name; ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Class Time') ?></th>
            <td><?= $class->class_time->i18nFormat('HH:mm a'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Class Day') ?></th>
            <td><?= h($class->class_day) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Start Date') ?></th>
            <td><?= h($class->start_date->i18nFormat('dd/MM/yyyy')) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('End Date') ?></th>
            <td><?= h($class->end_date->i18nFormat('dd/MM/yyyy')) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Class Term') ?></th>
            <td><?= $this->Number->format($class->class_term) ?></td>
        </tr>
    </table>
	 <div class="card mb-3">
      <div class="card-header" role="tab" id="headingLessons">
        <div class="row justify-content-between align-items-center">
          <div class="col" data-toggle="collapse" href="#collapseLessons" aria-expanded="false" aria-controls="collapseLessons">
            <h5 class="mb-0">
              Lessons
            </h5>
          </div>

        </div>
      </div>
    <div id="collapseLessons" class="collapse" role="tabpanel" aria-labelledby="headingLessons">
      <?php if (!empty($class->lessons)): ?>
         <table class="table table-responsive table-striped bg-white mb-0">
                <tr>
                    <th scope="col"><?= __('Teacher') ?></th>
                    <th scope="col"><?= __('Lesson Date') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($class->lessons as $lessons): ?>
                <tr>
                    <td><?= h($lessons->teacher->user->first_name . ' ' . $lessons->teacher->user->last_name) ?></td>
                    <td><?= h($lessons->lesson_date->i18nFormat('dd/MM/yyyy')) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['controller' => 'Lessons', 'action' => 'TeacherView', $lessons->id]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            <?php endif; ?>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="headingEnrolments">
		<div class="row justify-content-between align-items-center">
          <div class="col" data-toggle="collapse" href="#collapseEnrolments" aria-expanded="false" aria-controls="collapseEnrolments">
            <h5 class="mb-0">
              Enrolments
            </h5>
          </div>
        </div>
    </div>
    <div id="collapseEnrolments" class="collapse" role="tabpanel" aria-labelledby="headingEnrolments">
            <?php if (!empty($class->enrolments)): ?>
            <table class="table table-responsive table-striped bg-white">
                <tr>
                    <th scope="col"><?= __('Student') ?></th>
                    <th scope="col"><?= __('Enrolment Date') ?></th>
                    <th scope="col"><?= __('No Of Weeks Enrolled') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($class->enrolments as $enrolments): ?>
                <tr>
                    <td><?= h($enrolments->student->first_name . ' ' . $enrolments->student->last_name) ?></td>
                    <td><?= h($enrolments->enrolment_date->i18nFormat('dd/M/yy')) ?></td>
                    <td><?= h($enrolments->no_of_weeks_enrolled) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['controller' => 'students', 'action' => 'teacher_view', $enrolments->student->id]) ?>
                          </td>
                </tr>
                 <?php endforeach; ?>
            </table>
			<?php else: ?>
				<table class="table table-responsive table-striped bg-white mb-0">
					<tr>
                    <th>There are currently no enrolments for this class.</th>
					</tr>
				</table>
            <?php endif; ?>
        </div>
      </div>
    </div>


</br>
<button type="button" class = 'btn btn-light' onclick="goBack()">Go Back</button>

<script>
function goBack() {
    window.history.back();
}
</script>
</div>
