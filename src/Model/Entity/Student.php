<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Student Entity
 *
 * @property int $id
 * @property int $school_id
 * @property string $first_name
 * @property string $last_name
 * @property string $dob
 * @property string $notes
 * @property \Cake\I18n\FrozenDate $signup_date
 *
 * @property \App\Model\Entity\School $school
 * @property \App\Model\Entity\Attendence[] $attendences
 * @property \App\Model\Entity\Enrolment[] $enrolments
 * @property \App\Model\Entity\StudentContact[] $student_contact
 */
class Student extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    protected $_virtual = ['full_name'];

    protected function _getFullName() {
        return $this->_properties['first_name'] . '  ' .
            $this->_properties['last_name'];
    }
}
