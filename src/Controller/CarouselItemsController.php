<?php
namespace App\Controller;
use Cake\Event\Event;
use App\Controller\AppController;

/**
 * CarouselItems Controller
 *
 * @property \App\Model\Table\SchoolsTable $CarouselItems
 *
 * @method \App\Model\Entity\CarouselItems[] paginate($object = null, array $settings = [])
 */
class CarouselItemsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $CarouselItems = $this->paginate($this->CarouselItems);

        $this->set(compact('CarouselItems'));
        $this->set('_serialize', ['CarouselItems']);
    }

    /**
     * View method
     *
     * @param string|null $id CarouselItems id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $CarouselItem = $this->CarouselItems->get($id, [
            'contain' => [
              'Webpages'
            ]
        ]);

        $this->set('CarouselItem', $CarouselItem);
        $this->set('_serialize', ['CarouselItem']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($webpage_id)
    {
        $CarouselItems = $this->CarouselItems->newEntity(array('webpage_id' => $webpage_id));
        if ($this->request->is('post')) {
            $CarouselItems = $this->CarouselItems->patchEntity($CarouselItems, $this->request->getData());
            if ($this->CarouselItems->save($CarouselItems)) {

				$school_details = $this->CarouselItems->get($CarouselItems->id);

				$critical = false;
				$list_id = '75f7313bbd';
				$school_categories = '096357b049';
				$school_name = $school_details->school_name;

				$segment = $this->mailchimp_create_segment($critical, $list_id, $school_categories, $school_name);


				$CarouselItems->mailchimp_id = $segment;
				$this->CarouselItems->save($CarouselItems);

                $this->Flash->success(__('The CarouselItems has been saved.'));

                return $this->redirect(['controller' => 'webpages', 'action' => 'view', $webpage_id]);
            }
            $this->Flash->error(__('The CarouselItems could not be saved. Please, try again.'));
        }
        $this->set(compact('CarouselItems', 'webpage_id'));
        $this->set('_serialize', ['CarouselItems']);
    }

    /**
     * Edit method
     *
     * @param string|null $id CarouselItems id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $CarouselItems = $this->CarouselItems->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $CarouselItems = $this->CarouselItems->patchEntity($CarouselItems, $this->request->getData());
            if ($this->CarouselItems->save($CarouselItems)) {
                $this->Flash->success(__('The CarouselItems has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The CarouselItems could not be saved. Please, try again.'));
        }
        $this->set(compact('CarouselItems'));
        $this->set('_serialize', ['CarouselItems']);
    }

    /**
     * Delete method
     *
     * @param string|null $id CarouselItems id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $CarouselItems = $this->CarouselItems->get($id);
        if ($this->CarouselItems->delete($CarouselItems)) {
            $this->Flash->success(__('The CarouselItems has been deleted.'));
        } else {
            $this->Flash->error(__('The CarouselItems could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
	
	public function search() {
		$this->viewBuilder()->setLayout('ajax');
		$CarouselItems = $this->CarouselItems->find()
			->where(['LOWER(school_name) LIKE' => '%' . strtolower($this->request->getQuery('term')) . '%'])
			->orWhere(['LOWER(school_address) LIKE' => '%' . strtolower($this->request->getQuery('term')) . '%'])
			->all();
			
		$this->set(compact('CarouselItems'));
		$this->set('_serialize', false);
	}
	
	public function beforeFilter(Event $event){
		parent::beforeFilter($event);
	}
}
