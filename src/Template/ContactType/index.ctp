<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\ContactType[]|\Cake\Collection\CollectionInterface $contactType
  */
?>

<div class="container">
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Contact Type'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Student Contact'), ['controller' => 'StudentContact', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Student Contact'), ['controller' => 'StudentContact', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="contactType index large-9 medium-8 columns content">
    <h3><?= __('Contact Type') ?></h3>
    <table class="table table-responsive table-striped bg-white">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('student_contact_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($contactType as $contactType): ?>
            <tr>
                <td><?= $this->Number->format($contactType->id) ?></td>
                <td><?= $contactType->has('student_contact') ? $this->Html->link($contactType->student_contact->id, ['controller' => 'StudentContact', 'action' => 'view', $contactType->student_contact->id]) : '' ?></td>
                <td><?= h($contactType->name) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $contactType->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $contactType->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $contactType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $contactType->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator text-center">
        <ul class="pagination justify-content-center">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('<i class="fa fa-chevron-left" aria-hidden="true"></i> ' . __('Previous'), ['escape' => false]) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Next') . ' <i class="fa fa-chevron-right" aria-hidden="true"></i>', ['escape' => false]) ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p class="text-muted"><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
