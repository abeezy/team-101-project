<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $row->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $row->id)]
            )
        ?></li>
    </ul>
</nav>
<div class="rows form large-9 medium-8 columns content">
    <?= $this->Form->create($row) ?>
    <fieldset>
        <legend><?= __('Edit Row') ?></legend>
        <?php
            echo $this->Form->control('row_template_id', ['options' => $rowTemplates]);
            echo $this->Form->control('webpage_id', ['options' => $webpages]);
			echo $this->Form->control('row_description');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
