<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\carouselItem[]|\Cake\Collection\CollectionInterface $carouselItems
  */
?>
<div class="container">

	<div class="btn-group mb-3">
		<?php echo $this->Html->link('<i class="fas fa-plus"></i>&nbsp;&nbsp;New Carousel Picture', ['action' => 'add'], ['class' => 'btn btn-outline-primary', 'escape' => false]) ?></li>
	</div>
	<div class="carouselItems index large-9 medium-8 columns content">
		<h3><?= __('carouselItems') ?></h3>
		<table class="table table-responsive table-striped bg-white ">
			<thead>
				<tr>
					<th scope="col"><?= $this->Paginator->sort('name') ?></th>
					<th scope="col" class="actions"><?= __('Actions') ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($carouselItems as $carouselItem): ?>
				<tr>
					<td><?= h($carouselItem->name) ?></td>
					<td class="actions">
						<?= $this->Html->link(__('View'), ['action' => 'view', $carouselItem->id]) ?>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		<div class="paginator text-center">
			<ul class="pagination justify-content-center">
				<?= $this->Paginator->first('<< ' . __('first')) ?>
				<?= $this->Paginator->prev('<i class="fa fa-chevron-left" aria-hidden="true"></i> ' . __('Previous'), ['escape' => false]) ?>
				<?= $this->Paginator->numbers() ?>
				<?= $this->Paginator->next(__('Next') . ' <i class="fa fa-chevron-right" aria-hidden="true"></i>', ['escape' => false]) ?>
				<?= $this->Paginator->last(__('last') . ' >>') ?>
			</ul>

			<p class="text-muted"><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>		
		</div>
	</div>
</div>
