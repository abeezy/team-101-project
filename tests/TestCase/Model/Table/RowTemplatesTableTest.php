<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RowTemplatesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RowTemplatesTable Test Case
 */
class RowTemplatesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RowTemplatesTable
     */
    public $RowTemplates;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.row_templates',
        'app.rows',
        'app.webpages',
        'app.columns'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('RowTemplates') ? [] : ['className' => RowTemplatesTable::class];
        $this->RowTemplates = TableRegistry::get('RowTemplates', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RowTemplates);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
