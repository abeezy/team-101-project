<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Students Controller
 *
 * @property \App\Model\Table\StudentsTable $Students
 *
 * @method \App\Model\Entity\Student[] paginate($object = null, array $settings = [])
 */
class StudentsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => array(
				'StudentContact' => array(
					'Users' => array(
						'Schools'
					)
				),
				'Enrolments'
			)
        ];
        $students = $this->paginate($this->Students);

        $this->set(compact('students'));
        $this->set('_serialize', ['students']);
    }

    public function past()
    {
		$students = $this->Students->find('all')->matching('Enrolments.Classes', function ($q) {
			return $q->where([
				'Classes.end_date <' => time()
			]);
		});

        $students = $this->paginate($students);

        $this->set(compact('students'));
        $this->set('_serialize', ['students']);
    }

    public function current()
    {
		$students = $this->Students->find('all')->matching('Enrolments.Classes', function ($q) {
			return $q->where([
				'Classes.end_date >' => time()
			]);
		});

        $students = $this->paginate($students);

        $this->set(compact('students'));
        $this->set('_serialize', ['students']);
    }



	public function studentsByClassCsv($class_id) {
		$this->viewBuilder()->setLayout('ajax');
		$students = $this->Students->find('all')->leftJoinWith('Enrolments.Classes')->where(['Classes.id' => $class_id])->leftJoinWith('StudentContact.Users')->leftJoinWith('Grades')->select(array(
			'Students.first_name',
			'Students.last_name',
			'Students.student_gender',
			'Students.notes',
			'Students.signup_date',
			'Grade' => 'Grades.name',
			'Parent' => 'GROUP_CONCAT(Users.first_name)',
			'Email' => 'GROUP_CONCAT(Users.user_email)'
		))->group('Students.id');
		
		//echo "<pre>";
		//print_r($students);
		//echo "</pre>";
		//exit;

		$this->response = $this->create_csv($students->all(), 'students_by_class', $this->response);
		return $this->response;
	}

	public function currentStudentsBySchoolCsv($school_id) {
		$this->viewBuilder()->setLayout('ajax');
		$students = $this->Students->find();
		
		$students = $this->Students->find('all')->leftJoinWith('Enrolments.Classes.Schools')->where(['Classes.end_date >' => time(), 'Classes.school_id' => $school_id])->leftJoinWith('StudentContact.Users')->leftJoinWith('Grades')->select(array(
			'Students.first_name',
			'Students.last_name',
			'Students.student_gender',
			'Students.notes',
			'Students.signup_date',
			'Grade' => 'Grades.name',
			'School' => 'Schools.school_name',
			'Parent' => 'GROUP_CONCAT(Users.first_name)',
			'Email' => 'GROUP_CONCAT(Users.user_email)'
		))->group('Students.id');

		$this->response = $this->create_csv($students->all(), 'current_students_by_school', $this->response);
		return $this->response;
	}

	public function pastStudentsBySchoolCsv($school_id) {
		$this->viewBuilder()->setLayout('ajax');
		$students = $this->Students->find();
		$students = $this->Students->find('all')->leftJoinWith('Enrolments.Classes.Schools')->where(['Classes.end_date <' => time(), 'Classes.school_id' => $school_id])->leftJoinWith('StudentContact.Users')->leftJoinWith('Grades')->select(array(
			'Students.first_name',
			'Students.last_name',
			'Students.student_gender',
			'Students.notes',
			'Students.signup_date',
			'Grade' => 'Grades.name',
			'School' => 'Schools.school_name',
			'Parent' => 'GROUP_CONCAT(Users.first_name)',
			'Email' => 'GROUP_CONCAT(Users.user_email)'
		))->group('Students.id');

		$this->response = $this->create_csv($students->all(), 'past_students_by_school', $this->response);
		return $this->response;
	}




    /**
     * View method
     *
     * @param string|null $id Student id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $student = $this->Students->get($id, [
            'contain' => [
				'Grades',
				'Enrolments' => [
				  'Classes' => [
					'Schools'
				  ],
				  'LessonEnrolment' => [
					'Lessons'
				  ]
				],
				'StudentContact' => [
				  'Users' => [
					'StudentContact' => [
					  'Students'
					]
				  ]
				]
			]
        ]);

        $this->set('student', $student);
        $this->set('_serialize', ['student']);
    }

	
	public function teacherView($id = null)
    {
        $student = $this->Students->get($id, [
            'contain' => [
				'Grades',
				'Enrolments' => [
				  'Classes' => [
					'Schools'
				  ]
				],
				'StudentContact' => [
				  'Users' => [
					'StudentContact' => [
					  'Students'
					]
				  ]
				]
			]
        ]);

        $this->set('student', $student);
        $this->set('_serialize', ['student']);
    }
	
	
    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $student = $this->Students->newEntity();
	  $grades = $this->Students->Grades->find('list', ['limit' => 200]);

        if ($this->request->is('post')) {
            $student = $this->Students->patchEntity($student, $this->request->getData());
            if ($this->Students->save($student)) {
                $this->Flash->success(__('The student has been saved.'));
						$this->loadModel('StudentContact');
						$sc = $this->StudentContact->newEntity();
						$sc->student_id = $student->id;
						$sc->user_id = $this->request->data('user_id');
						$this->StudentContact->save($sc);
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The student could not be saved. Please, try again.'));
        }

        $this->loadModel('Users');

        $parents = $this->Users->find('list', [
          'contain' => [
            'roles'
          ],
          'conditions'=> [
            'Roles.name' => 'parent'
          ]
        ]);

        $this->set(compact('student', 'parents','grades'));
        $this->set('_serialize', ['student']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Student id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
	 $grades = $this->Students->Grades->find('list', ['limit' => 200]);

        $student = $this->Students->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $student = $this->Students->patchEntity($student, $this->request->getData());
            if ($this->Students->save($student)) {
                $this->Flash->success(__('The student has been saved.'));

                return $this->redirect(['action' => 'view', $student->id]);
            }
            $this->Flash->error(__('The student could not be saved. Please, try again.'));
        }


        $this->set(compact('student','grades'));
        $this->set('_serialize', ['student']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Student id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $student = $this->Students->get($id);
        if ($this->Students->delete($student)) {
            $this->Flash->success(__('The student has been deleted.'));
        } else {
            $this->Flash->error(__('The student could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

	public function select(){
		$this->loadModel('Users');
		$parent = $this->Users->get($this->Auth->user('id'), array(
			'contain' => array(
				'StudentContact' => array(
					'Students' => array(
						'Enrolments' => array(
							'Classes' => array(
								'Schools'
							)
						),
						'Grades'
					)
				)
			)
		));

		if ($this->request->is('post')) {
			$this->request->session()->write("student_id", $this->request->getData('student_id'));

			if($this->request->session()->check('class_id')) {
				return $this->redirect(['controller' => 'enrolments', 'action' => 'enrol']);
			} else {
				return $this->redirect(['controller' => 'classes', 'action' => 'all']);
			}
		}

		$this->set(compact('parent'));

	}











    //add students before enrolment
	public function studentEnrolment() {
		$students = $this->Students->newEntity();
        $grades = $this->Students->Grades->find('list', ['limit' => 200]);

		
		
		$this->loadModel('Classes');
		$class = $this->Classes->get($this->request->session()->read('class_id'));
		
		$classes = $this->Classes->find('list', [
			'conditions' => [
				'school_id' => $class->school_id,
				'Classes.end_date >' => time()
			],
			'contain' => [
				'lessons'
			],
			'limit' => 200
		]);

		$weeks = $this->Classes->find('list', [
			'valueField' => function ($class) {
				return $class->get('class_term');
			},
			'limit' => 200
		]);

		
        if ($this->request->is('post')) {

			$this->loadModel('Lessons');
		
			if($this->request->session()->check('cart')) {
				$cart_data = $this->request->session()->read('cart');
			} else {
				$cart_data = array(
					'items' => array(),
					'total_price' => 0,
					'discountMultiplier' => 1
				);
			}

			$totalPrice = 0;

            $students = array();

			foreach($this->request->getData('students') as $student_data) {
				$student_data['signup_date'] = date('Y-m-d');

				$cart_item = array();
				$cart_item['Student'] = $this->Students->save($this->Students->newEntity($student_data));
				$cart_item['Class'] = $this->Classes->findAllById($student_data['class_id'])->contain('Lessons', function ($q) {
					return $q->where(['lesson_date >' => time()]);
				})->contain(array(
					'Schools',
					'Teachers' => array(
						'Users'
					)
				))->first();
				$cart_item['weeks_enrolled'] = explode(',', $student_data['weeks_enrolled']);
				
				$cart_item['first_lesson'] = $this->Lessons->get($cart_item['weeks_enrolled'][0]);
				$cart_item['last_lesson'] = $this->Lessons->get($cart_item['weeks_enrolled'][count($cart_item['weeks_enrolled']) - 1]);

				$total_lesson_count = $this->Classes->Lessons->findAllByClassId($student_data['class_id'])->count();

				$cart_item['price'] = round($cart_item['Class']['price'] * count($cart_item['weeks_enrolled']), 2);

				$cart_item['action'] = 'enroll';

				array_push($cart_data['items'], $cart_item);
			}

			foreach($cart_data['items'] as $cart_item){
				$totalPrice += $cart_item['price'];
			}

			if (count($cart_data['items']) > 1 ){
				$cart_data['discountMultiplier'] = 0.9;
				$totalPrice = $totalPrice * $cart_data['discountMultiplier'];
			}

			$cart_data['total_price'] = $totalPrice;


			$this->request->session()->write("cart", $cart_data);

			/*
			$students = array();

			$students = $this->Students->newEntities($students);

		    if ($this->Students->saveMany($students)) {
				foreach($students as $student) {
					$this->loadModel('StudentContact');
					$sc = $this->StudentContact->newEntity();
					$sc->student_id = $student->id;
					$sc->user_id = $this->Auth->user('id');
					$this->StudentContact->save($sc);

					$this->loadModel('Enrolments');
					$enrolment = $this->Enrolments->newEntity();

					$this->loadModel('Lessons');

					$first_lesson = $this->Lessons->get($student['weeks_enrolled'][0]);

					$enrolment_data = array(
						'student_id' => $student->id,
						'class_id' => $student['class_id'],
						'enrolment_date' => $first_lesson->lesson_date,
						'no_of_weeks_enrolled' => count($student['weeks_enrolled'])
					);

					$enrolment = $this->Enrolments->patchEntity($enrolment, $enrolment_data);

					if ($this->Enrolments->save($enrolment)) {

						$this->loadModel('Lessons');
						$this->loadModel('LessonEnrolment');


						foreach($student['weeks_enrolled'] as $lesson_id) {

						//for($i = 0; $i < $this->request->getData('no_of_weeks_enrolled'); $i++) {
						foreach($student['weeks_enrolled'] as $lesson_id) {

							// Get the next lesson date after the start of the user's enrolment, then each week afterwards using the loop
							/*$lesson = $this->Lessons->find('all', array(
								'conditions' => array(
									'class_id' => $enrolment->class_id,
									'lesson_date <=' => $enrolment->enrolment_date->modify('+' . (7 * $i) . ' days'),
									'lesson_date >' => $enrolment->enrolment_date->modify('+' . (7 * ($i-1)) . ' days'),
								),
								'order' => array(
									'Lessons.lesson_date' => 'ASC'
								)
							))->first();* /

							$lesson = $this->Lessons->get($lesson_id);

							if ($lesson) {
								$le = $this->LessonEnrolment->newEntity();
								$le->lesson_id = $lesson->id;
								$le->enrolment_id = $enrolment->id;
								$this->LessonEnrolment->save($le);
							}




                $this->Flash->success(__('The student(s) has been saved.'));

				$this->request->session()->delete("student_id");
				$this->request->session()->delete("claas_id");


				return $this->redirect(['controller' => 'enrolments', 'action' => 'cart']);


				return $this->redirect(['controller' => 'users', 'action' => 'parent']);

				/*if($this->request->session()->check('class_id')) {
					return $this->redirect(['controller' => 'enrolments', 'action' => 'enrol']);
				} else {
					return $this->redirect(['controller' => 'classes', 'action' => 'all']);
				}* /



            $this->Flash->error(__('The student(s) could not be saved. Please, try again.'));
			*/

			   return $this->redirect(['controller' => 'enrolments', 'action' => 'cart']);
        }

        $this->set(compact('students', 'grades', 'class', 'classes'));
        $this->set('_serialize', ['student']);
}


    /**
     * Edit method
     *
     * @param string|null $id Student id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function parentEdit($id = null)
    {
        $student = $this->Students->get($id, [
            'contain' => []
        ]);

        $this->loadModel('StudentContact');

        $studentContact = $this->StudentContact->find('all', array(
          'conditions' => array(
            'student_id' => $id,
            'user_id' => $this->Auth->user('id')
          )
        ))->first();

        if ($studentContact != null){

          if ($this->request->is(['patch', 'post', 'put'])) {
              $student = $this->Students->patchEntity($student, $this->request->getData());
              if ($this->Students->save($student)) {
                  $this->Flash->success(__('Student details have been successfully updated and saved.'));

                  return $this->redirect(['controller' => 'users', 'action' => 'dashboard']);
              }
              $this->Flash->error(__('Student details were not successfully updated and saved.'));
          }
        //  $schools = $this->Students->Schools->find('list', ['limit' => 200]);
          $grades = $this->Students->Grades->find('list', ['limit' => 200]);
          $this->set(compact('student',  'grades'));
          $this->set('_serialize', ['student']);
        }
  		else {
          return $this->redirect(['controller' => 'users', 'action' => 'dashboard']);
      }
    }

	public function search() {
		$this->viewBuilder()->setLayout('ajax');
		$students = $this->Students->find()
			->where(['LOWER(first_name) LIKE' => '%' . strtolower($this->request->getQuery('term')) . '%'])
			->orWhere(['LOWER(last_name) LIKE' => '%' . strtolower($this->request->getQuery('term')) . '%'])
			->all();

		$this->set(compact('students'));
		$this->set('_serialize', false);
	}

	public function beforeFilter(Event $event){
		$this->Auth->allow(['studentEnrolment']);
		if(!$this->action_is_public($event->subject) && $this->get_current_user_role_name() === 'parent') {
			$this->Auth->allow(['studentEnrolment', 'parentEdit', 'select']);
		}
		if(!$this->action_is_public($event->subject) && $this->get_current_user_role_name() === 'teacher') {
			$this->Auth->allow(['teacherView']);
		}
		parent::beforeFilter($event);
	}

}
