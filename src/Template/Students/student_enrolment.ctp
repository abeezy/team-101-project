<div class="container">
	<?php echo $this->Form->create($students, array('class' => 'form-enrol', 'autocomplete' => 'disabled')); ?>
		<div class="row justify-content-center">
			<div class="col-auto px-4 py-2 mx-3 progress-chevron">
				Parent
			</div>
			<div class="col-auto px-4 py-2 mx-3 progress-chevron active">
				Student
			</div>
			<div class="col-auto px-4 py-2 mx-3 progress-chevron">
				Cart
			</div>
			<div class="col-auto px-4 py-2 mx-3 progress-chevron">
				Payment
			</div>
			<div class="col-auto px-4 py-2 mx-3 progress-chevron">
				Summary
			</div>
		</div>
		<div class="row justify-content-center align-items-center">
			<div class="col">
				<div class="card-deck" id="studentContainer">
					<div class="card" id="students_0">
						<div class="card-header">
							<div class="row justify-content-between align-items-center">
								<div class="col-auto">
									<h2 class="text-center mb-0">Student</h2>
								</div>
								<div class="col-auto">
									<span class="removeStudentButton"><i class="fas fa-times"></i></span>
								</div>
							</div>
						</div>
						<div class="card-body">
							<?php echo $this->Form->control('students.0.class_id', ['type' => 'select', 'options' => $classes, 'default' => $class->id, 'empty' => ['' => ' -- Please Select an Option -- '], 'disabled' => [''], 'class' => 'sClassID' ]); ?>

							<div class="form-group">
								<label>Weeks Enrolled</label>
								<select name="students[0][weeks_enrolled]" class="form-control sWeeksEnrolled">
									<option disabled="disabled" selected="selected" value=""> -- Please Select A Class -- </option>
								</select>
							</div>

							<hr/>

							<?php echo $this->Form->control('students.0.first_name',['type' => 'text', 'placeholder' => 'Sophia', 'class' => 'sFName']); ?>
							<?php echo $this->Form->control('students.0.last_name', ['type' => 'text', 'placeholder' => 'Jackson', 'class' => 'sLName']); ?>
							<?php echo $this->Form->control('students.0.student_gender', ['type'=>'select', 'options' => array('Male' => 'Male','Female'=>'Female'), 'default' => '', 'empty'=> array('' => '-- Please Select --'), 'disabled' => [''], 'class' => 'sGender']); ?>
							<?php echo $this->Form->control('students.0.grade_id', ['type' => 'select', 'options' => $grades, 'class' => 'sGradeID']); ?>
							<?php echo $this->Form->control('students.0.notes', ['type' => 'textarea', 'placeholder' => 'Please note any medical conditions and concerns', 'class' => 'sNotes']); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="card">
					<div class="card-body text-center" id="addStudent">
						+ <br/><br/>
						Add Another Student
					</div>
				</div>
			</div>
		</div>
		<div class="row justify-content-end">
			<div class="col-md-3">
				<div class="form-group">
				<p>If you have more than one child, please "+ Add Another Student" before clicking Enrol Now</p>
					<button class="btn btn-lg btn-primary btn-block" type="submit"> Enrol Now </button>
				</div>
			</div>
		</div>
	<?php echo $this->Form->end(); ?>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js" defer="defer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" />

<?php echo $this->Html->script('enrolment', array('defer' => 'defer')); ?>
