<?php
namespace App\Controller;
use Cake\Event\Event;
use App\Controller\AppController;

/**
 * Siblings Controller
 *
 * @property \App\Model\Table\SiblingsTable $Siblings
 *
 * @method \App\Model\Entity\Sibling[] paginate($object = null, array $settings = [])
 */
class SiblingsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Students']
        ];
        $siblings = $this->paginate($this->Siblings);

        $this->set(compact('siblings'));
        $this->set('_serialize', ['siblings']);
    }

    /**
     * View method
     *
     * @param string|null $id Sibling id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $sibling = $this->Siblings->get($id, [
            'contain' => ['Students']
        ]);

        $this->set('sibling', $sibling);
        $this->set('_serialize', ['sibling']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $sibling = $this->Siblings->newEntity();
        if ($this->request->is('post')) {
            $sibling = $this->Siblings->patchEntity($sibling, $this->request->getData());
            if ($this->Siblings->save($sibling)) {
                $this->Flash->success(__('The sibling has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sibling could not be saved. Please, try again.'));
        }
        $this->set(compact('sibling'));
        $students = $this->Siblings->Students->find('list', ['limit' => 200]);
        $this->set(compact('sibling', 'students'));
        $this->set('_serialize', ['sibling']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Sibling id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $sibling = $this->Siblings->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $sibling = $this->Siblings->patchEntity($sibling, $this->request->getData());
            if ($this->Siblings->save($sibling)) {
                $this->Flash->success(__('The sibling has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sibling could not be saved. Please, try again.'));
        }
        $this->set(compact('sibling'));
        $students = $this->Siblings->Students->find('list', ['limit' => 200]);
        $this->set(compact('sibling', 'students'));
        $this->set('_serialize', ['sibling']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Sibling id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $sibling = $this->Siblings->get($id);
        if ($this->Siblings->delete($sibling)) {
            $this->Flash->success(__('The sibling has been deleted.'));
        } else {
            $this->Flash->error(__('The sibling could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
	
	public function beforeFilter(Event $event){
		parent::beforeFilter($event);
	}
}
