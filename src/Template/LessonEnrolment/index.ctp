<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\LessonEnrolment[]|\Cake\Collection\CollectionInterface $lessonEnrolment
  */
?>
<div class="container">
	<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Lesson Enrolment'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Lessons'), ['controller' => 'Lessons', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Lesson'), ['controller' => 'Lessons', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Enrolments'), ['controller' => 'Enrolments', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Enrolment'), ['controller' => 'Enrolments', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="lessonEnrolment index large-9 medium-8 columns content">
    <h3><?= __('Lesson Enrolment') ?></h3>
    <table class="table table-responsive table-striped bg-white">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('lesson_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('enrolment_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('attended') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($lessonEnrolment as $lessonEnrolment): ?>
            <tr>
                <td><?= $this->Number->format($lessonEnrolment->id) ?></td>
                <td><?= $lessonEnrolment->has('lesson') ? $this->Html->link($lessonEnrolment->lesson->id, ['controller' => 'Lessons', 'action' => 'view', $lessonEnrolment->lesson->id]) : '' ?></td>
                <td><?= $lessonEnrolment->has('enrolment') ? $this->Html->link($lessonEnrolment->enrolment->id, ['controller' => 'Enrolments', 'action' => 'view', $lessonEnrolment->enrolment->id]) : '' ?></td>
                <td><?= h($lessonEnrolment->attended) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $lessonEnrolment->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $lessonEnrolment->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $lessonEnrolment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $lessonEnrolment->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator text-center">
        <ul class="pagination justify-content-center">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('<i class="fa fa-chevron-left" aria-hidden="true"></i> ' . __('Previous'), ['escape' => false]) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Next') . ' <i class="fa fa-chevron-right" aria-hidden="true"></i>', ['escape' => false]) ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p class="text-muted"><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
</div>
