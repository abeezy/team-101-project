<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="container">
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $studentContact->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $studentContact->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Student Contact'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Students'), ['controller' => 'Students', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Student'), ['controller' => 'Students', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Contact Type'), ['controller' => 'ContactType', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Contact Type'), ['controller' => 'ContactType', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="studentContact form large-9 medium-8 columns content">
    <?= $this->Form->create($studentContact) ?>
    <fieldset>
        <legend><?= __('Edit Student Contact') ?></legend>
        <?php
            echo $this->Form->control('student_id', ['options' => $students]);
            echo $this->Form->control('user_id', ['options' => $users]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
