<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\User $user
  */
?>

<div class="row justify-content-between align-items-center mb-3">
	<div class="col-auto">
		<h3 class="mb-0"><?php echo $user->full_name; ?></h3>
	</div>
	<div class="col-auto">
		<?php echo $this->Html->link('<i class="fas fa-pencil-alt"></i>&nbsp;&nbsp;Edit User', ['action' => 'edit', $user->id], ['class' => 'btn btn-outline-primary', 'escape' => false]) ?></li>
		<?php if($user->active): ?>
			<?php echo $this->Form->postLink('<i class="fas fa-power-off"></i>&nbsp;&nbsp;Deactivate User', ['action' => 'deactivate', $user->id], ['class' => 'btn btn-outline-primary', 'escape' => false, 'confirm' => 'Are you sure that you want to deactivate this user?']) ?></li>
		<?php else: ?>
			<?php echo $this->Form->postLink('<i class="fas fa-power-off"></i>&nbsp;&nbsp;Reactivate User', ['action' => 'activate', $user->id], ['class' => 'btn btn-outline-primary', 'escape' => false, 'confirm' => 'Are you sure that you want to reactivate this user?']); ?></li>
		<?php endif; ?>
	</div>
</div>



<div class="schools view large-9 medium-8 columns content">
	<div class="card mb-3">
		<table class="table table-responsive table-striped mb-0">
			<tr>
				<th>Status</th>
				<td><?php if($user->active): ?>Active<?php else: ?>Inactive<?php endif; ?></td>
			</tr>
			<tr>
				<th scope="row"><?= __('Role Name') ?></th>
				<td><?= ucwords($user->role->name) ?></td>
			</tr>
				<th scope="row"><?= __('First name') ?></th>
				<td><?= h($user->first_name) ?></td>
			</tr>
			<tr>
				<th scope="row"><?= __('Last name') ?></th>
				<td><?= h($user->last_name) ?></td>
			</tr>
			<tr>
				<th scope="row"><?= __('Phone Number') ?></th>
				<td><?= h($user->phone_number) ?></td>
			</tr>
			<tr>
				<th scope="row"><?= __('User Email') ?></th>
				<td><?= h($user->user_email) ?></td>
			</tr>
			<tr>
			<tr>
				<th scope="row"><?= __('Date Created') ?></th>
				<td><?= h($user->created->i18nFormat('dd/MM/yyyy')) ?></td>
			</tr>
			<tr>
			 <th>Notes</th>
          <td><?= $this->Text->autoParagraph(h($user->notes)); ?></td>
			</tr>
			<tr>
			 <th>Admin notes</th>
          <td><?= $this->Text->autoParagraph(h($user->admin_notes)); ?></td>
			</tr>
		</table>

	</div>

	  <div class="related mb-3">
        <div class="card">
			<div class="card-header">
				<div class="row justify-content-between align-items-center">
					<div class="col-auto">
						<h4 class="mb-0">Student</h4>
					</div>
					<div class="col-auto">
						<?php echo $this->Html->link('<i class="fas fa-plus"></i>&nbsp;&nbsp;Add New', ['controller' => 'Students', 'action' => 'add'], ['class' => 'btn btn-outline-primary', 'escape' => false]) ?></li>
					</div>
				</div>
			</div>
			<?php if (!empty($user->student_contact)): ?>
				<table class="table table-responsive table-striped mb-0">
					<thead>
						<tr>

					<th scope="col"><?= __('Student Name') ?></th>
					<th scope="col"><?= __('Actions') ?></th>

						</tr>
					</thead>
					<tbody>
						<?php foreach ($user->student_contact as $studentContact): ?>
				<tr>
					<td><?= h($studentContact->student->full_name) ?></td>
					<td><?php echo $this->Html->link('View', ['controller' => 'students', 'action' => 'view', $studentContact->student->id])?></td>

				</tr>

						<?php endforeach; ?>
					</tbody>
				</table>
			<?php endif; ?>
		</div>
    </div>

	 <?php /*<div class="related mb-3">
        <div class="card">
			<div class="card-header">
				<div class="row justify-content-between align-items-center">
					<div class="col-auto">
						<h4 class="mb-0">Second Parent</h4>
					</div>
					<div class="col-auto">
						<?php echo $this->Html->link('<i class="fas fa-plus"></i>&nbsp;&nbsp;Add New', ['controller' => 'Users', 'action' => 'add'], ['class' => 'btn btn-outline-primary', 'escape' => false]) ?></li>
					</div>
				</div>
			</div>
			<?php if (!empty($user->teachers)): ?>
				<table class="table table-responsive table-striped mb-0">
					<thead>
						<tr>

					<th scope="col"><?= __('User Id') ?></th>
					<th scope="col"><?= __('first_name') ?></th>
					<th scope="col"><?= __('Bio') ?></th>

						</tr>
					</thead>
					<tbody>
				<?php foreach ($user->teachers as $teachers): ?>
				<tr>
					<td><?= h($teachers->user_id) ?></td>
					<td><?= h($teachers->first_name) ?></td>
					<td><?= h($teachers->bio) ?></td>
				</tr>
				<?php endforeach; ?>
					</tbody>
				</table>
			<?php endif; ?>
		</div>
    </div> */ ?>
</div>
			<button type="button" class = 'btn btn-light' onclick="goBack()">Go Back</button>

<script>
function goBack() {
    window.history.back();
}
</script>
