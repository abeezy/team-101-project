<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
  */
?>
<div class="container">

<div class="btn-group mb-3">
		<?php echo $this->Html->link('<i class="fas fa-plus"></i>&nbsp;&nbsp;New User', ['action' => 'add'], ['class' => 'btn btn-outline-primary', 'escape' => false]) ?></li>
	</div>
	<div class="users index large-9 medium-8 columns content">
		<h3><?= __('Users') ?></h3>

		<table class="table table-responsive table-striped bg-white">
			<thead>
				<tr>
					<th scope="col"><?= $this->Paginator->sort('role_id') ?></th>
					<th scope="col"><?= $this->Paginator->sort('first_name') ?></th>
					<th scope="col"><?= $this->Paginator->sort('last_name') ?></th>
					<th scope="col"><?= $this->Paginator->sort('user_email') ?></th>
					<th scope="col"><?= $this->Paginator->sort('phone_number') ?></th>
					<th scope="col"><?= $this->Paginator->sort('notes') ?></th>
					<th scope="col" class="actions"><?= __('Actions') ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($users as $user): ?>
				<tr>
					<td><?= h($user->role_id) ?></td>
					<td><?= h($user->first_name) ?></td>
					<td><?= h($user->last_name) ?></td>
					<td><?= h($user->user_email) ?></td>
					<td><?= h($user->phone_number) ?></td>
					<td><?= h($user->notes) ?></td>

					<td class="actions">
						<?= $this->Html->link(__('View'), ['action' => 'view', $user->id]) ?>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		<div class="paginator text-center">
			<ul class="pagination justify-content-center">
				<?= $this->Paginator->prev('<i class="fa fa-chevron-left" aria-hidden="true"></i> ' . __('Previous'), ['escape' => false]) ?>
				<?= $this->Paginator->numbers() ?>

				<?= $this->Paginator->next(__('Next') . ' <i class="fa fa-chevron-right" aria-hidden="true"></i>', ['escape' => false]) ?>


			</ul>
			<p class="text-muted"><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
		</div>
	</div>


</div>
