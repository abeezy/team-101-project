<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Lessons Controller
 *
 * @property \App\Model\Table\LessonsTable $Lessons
 *
 * @method \App\Model\Entity\Lesson[] paginate($object = null, array $settings = [])
 */
class LessonsController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Classes', 'Teachers']
        ];
        $lessons = $this->paginate($this->Lessons);

        $this->set(compact('lessons'));
        $this->set('_serialize', ['lessons']);
    }

    public function teacherIndex() {
        $this->paginate = [
            'conditions' => [
                'Users.id' => $this->Auth->user('id')
            ],
            'contain' => [
                'Classes',
                'Teachers' => [
                    'Users'
                ]
            ]
        ];
        $lessons = $this->paginate($this->Lessons);

        $this->set(compact('lessons'));
        $this->set('_serialize', ['lessons']);
    }

    /**
     * View method
     *
     * @param string|null $id Lesson id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $lesson = $this->Lessons->get($id, [
            'contain' => array(
                'Classes' => array(
                    'Schools'
                ),
                'Teachers' => array(
                    'Users'
                ),
                'LessonEnrolment' => array(
                    'Enrolments' => array(
                        'Students'
                    )
                )
            )
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->Lessons->patchEntity($lesson, $this->request->getData(), [
                'associated' => [
                    'LessonEnrolment'
                ]
            ]);
            if ($this->Lessons->save($lesson)) {
                $this->Flash->success(__('The lesson has been saved.'));
                return $this->redirect(array('controller' => 'classes', 'action' => 'view', $lesson->class->id));
            } else {
                $this->Flash->error(__('The lesson could not be saved. Please, try again.'));
            }
        }

        $this->set('lesson', $lesson);
        $this->set('_serialize', ['lesson']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $lesson = $this->Lessons->newEntity();

        $classes = $this->Lessons->Classes->find('list', ['keyfield' => 'id', 'valueField' => function ($row) {
                return $row['Schools']['school_name'].', '. $row['name'] . ', ' . $row['class_address'] . ', ' . $row['class_day'];
            }, 'contain'=>['schools']]);
        $this->loadModel('Teachers');
        $teachers = $this->Teachers->find('list', [
            'contain' => [
                'users'
            ],
//            'conditions' => [
//                'Roles.name' => 'teacher'
//            ]
        ]);
//        debug($teachers->toarray());

        if ($this->request->is('post')) {

            $lesson = $this->Lessons->patchEntity($lesson, $this->request->getData());
            // debug($lesson);
            if ($this->Lessons->save($lesson)) {
                $this->Flash->success(__('The lesson has been saved.'));

                return $this->redirect(['action' => 'index','controller'=>'Classes']);
            } else {
                $this->Flash->error(__('The lesson could not be saved. Please, try again.'));
            }
        }

        $this->set(compact('lesson', 'classes', 'teachers'));
        $this->set('_serialize', ['lesson']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Lesson id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $lesson = $this->Lessons->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $lesson = $this->Lessons->patchEntity($lesson, $this->request->getData());
            if ($this->Lessons->save($lesson)) {
                $this->Flash->success(__('The lesson has been saved.'));

                return $this->redirect(['controller' => 'classes', 'action' => 'view', $lesson['class_id']]);
            }
            $this->Flash->error(__('The lesson could not be saved. Please, try again.'));
        }
        $classes = $this->Lessons->Classes->find('list', ['keyfield' => 'id', 'valueField' => function ($row) {
                return $row['Schools']['school_name'].', '. $row['name'] . ', ' . $row['class_address'] . ', ' . $row['class_day'];
            }, 'contain'=>['schools']]);
        $teachers = $this->Lessons->Teachers->find('list', ['limit' => 200]);
        $this->set(compact('lesson', 'classes', 'teachers'));
        $this->set('_serialize', ['lesson']);
    }

    public function teacherView($id = null) {
        $lesson = $this->Lessons->get($id, [
            'contain' => array(
                'Classes' => array(
                    'Schools'
                ),
                'Teachers',
                'LessonEnrolment' => array(
                    'Enrolments' => array(
                        'Students'
                    )
                )
            )
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->Lessons->patchEntity($lesson, $this->request->getData(), [
                'associated' => [
                    'LessonEnrolment'
                ]
            ]);
            if ($this->Lessons->save($lesson)) {
                $this->Flash->success(__('The lesson has been saved.'));
                return $this->redirect(array('controller' => 'classes', 'action' => 'teacherView', $lesson->class->id));
            } else {
                $this->Flash->error(__('The lesson could not be saved. Please, try again.'));
            }
        }

        $this->set('lesson', $lesson);
        $this->set('_serialize', ['lesson']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Lesson id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $lesson = $this->Lessons->get($id);
        if ($this->Lessons->delete($lesson)) {
            $this->Flash->success(__('The lesson has been deleted.'));
        } else {
            $this->Flash->error(__('The lesson could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function beforeFilter(Event $event) {
        if (!$this->action_is_public($event->subject) && $this->get_current_user_role_name() === 'teacher') {
            $this->Auth->allow(['teacherIndex', 'teacherView']);
        }
        parent::beforeFilter($event);
    }

}
