<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Enrolment $enrolment
  */
?>

<div class="container">
<nav

<div class="enrolments view large-9 medium-8 columns content">
    <h3><?= h($enrolment->id) ?></h3>
<table class="table table-responsive table-striped bg-white">
        <tr>
          <td class="actions text-right" colspan="2">
          <?= $this->Html->link(__('Edit'), ['action' => 'edit', $enrolment->id]) ?>
          <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $enrolment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $enrolment->id)]) ?>
          </td>
        </tr>
        <tr>
            <th scope="row"><?= __('Student') ?></th>
            <td><?= $enrolment->has('student') ? $this->Html->link($enrolment->student->first_name . ' ' . $enrolment->student->last_name, ['controller' => 'Students', 'action' => 'view', $enrolment->student->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Class') ?></th>
            <td><?= $enrolment->has('class') ? $this->Html->link($enrolment->class->name, ['controller' => 'Classes', 'action' => 'view', $enrolment->class->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('No Of Weeks Enrolled') ?></th>
            <td><?= $this->Number->format($enrolment->no_of_weeks_enrolled) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Enrolment Date') ?></th>
            <td><?= $enrolment->enrolment_date->i18nFormat('dd/MM/yyyy') ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Payment History') ?></h4>
        <?php if (!empty($enrolment->payment_history_enrolment)): ?>
        <table class="table table-responsive table-striped bg-white">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Enrolment Id') ?></th>
                <th scope="col"><?= __('Amount') ?></th>
                <th scope="col"><?= __('Datetime') ?></th>
                <th scope="col"><?= __('Payment Status') ?></th>
            </tr>
            <?php foreach ($enrolment->payment_history_enrolment as $phe): ?>
            <tr>
                <td><?= h($phe->payment_history->id) ?></td>
                <td><?= h($phe->payment_history->enrolment_id) ?></td>
                <td><?= h($phe->payment_history->amount) ?></td>
                <td><?= h($phe->payment_history->datetime) ?></td>
                <td><?= h($phe->payment_history->payment_status) ?></td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
		<div class="teachers view large-9 medium-8 columns content">
<?= $this->Html->link(__('Back'), ['controller' => 'Enrolments', 'action' => 'index'], array('class' => 'btn btn-secondary')) ?>
    </div>
</div>
</div>
</div>
