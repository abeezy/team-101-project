<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
require_once(ROOT. DS . 'plugins' . DS . "sparkpost-api.php");
use sparkPostApi; 
require_once(ROOT. DS . 'plugins' . DS . "MailChimp.php");
use DrewM\MailChimp\MailChimp;
require_once(ROOT. DS . 'plugins' . DS . "recaptcha2.php");
use GoogleRecaptcha;


/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
		//auth component for the login function, shows the user_email and password fields//
        
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
		$this->loadComponent('Cookie');
		$this->loadComponent('Auth',[
      		'authenticate'=>[
        		'Form'=>[
					'fields'=>[
						'username'=>'user_email',
						'password'=>'password'
					],
					'finder' => 'auth'
				]
			],
      		'loginAction'=>[
      		    'controller'=>'users',
      		    'action'=>'login'
      		],
			'loginRedirect' => [
				'controller' => 'users',
				'action' => 'dashboard'
			],
			'logoutRedirect' => [
				'controller' => 'pages',
				'action' => 'display',
				'home'
			]
		]);
		// $this->loadComponent('sparkPostApi');


        /*
         * Enable the following components for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
        //$this->loadComponent('Csrf');
		
		parent::initialize();
    }
	
	


    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Http\Response|null|void
     */
    public function beforeRender(Event $event)
    {
        // Note: These defaults are just to get started quickly with development
        // and should not be used in production. You should instead set "_serialize"
        // in each action as required.
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
		if ($this->request->session()->read('Auth.User')){
			$this->set('loggedIn',true);
			
			$this->set('user_first_name', $this->request->session()->read('Auth.User.first_name'));
		}else {
			$this->set('loggedIn',false);
		}
		
	}
	//Function that connects to sparkPost and is called after a user signs up so a Welcome Email will be sent to them. 
	public function send_email ($to, $from, $subject, $template, $merge_variables) {
		echo "Starting email send function<br>";
		
		
		$mail = new sparkPostApi('https://api.sparkpost.com/api/v1/transmissions','cbcd6f382ee780d01990cee71303b0db070fd498');
		echo "Constructed API object<br>";
		// $mail-> from(array(
		  // 'email' => 'noreply@email.dramatime.com.au',
		  // 'name'  => 'Drama Time'
		// ));

		//$mail-> subject($subject);
		/*========================================================================
		// TEMPLATE USAGE
		// $mail->template(template_id (String)>,Substitution Data (Array));
		/=======================================================================*/
		echo "Setting template<br>";
		$mail-> template(
			$template,
			$merge_variables
		);
		echo "Setting recipient<br>";
		$mail-> setTo($to);
		//$mail-> setReplyTo('noreply@dramatime.com.au');

		try{
			echo "Trying to send<br>";
			$mail->send();
			echo "Email sent<br>";
			print "Message Sent";
		} 
		catch (Exception $e) {
			echo json_encode($e);
			print $e;	
		}
		echo "Closing email object<br>";
		$mail->close();
		echo "Returning from email send<br>";
	}
	
	//Simple, returns the role name of a user to check what they can access when they login
	//If their authenticate is not null, it will load the Roles table and return the role name based on the id of their role
	public function get_current_user_role_name() {
		if($this->Auth->user() === null) {
			return $this->redirect(['controller' => 'users', 'action' => 'login']);
		} else {
			$this->loadModel('Roles');
			$role=$this->Roles->get($this->Auth->user('role_id'));
			return $role->name;
		}
	}
	
	//Teachers have a different dashboard and this allows them to view the pages they are allowed to view when logging in
	public function verify_at_least_teacher(Event $event) {
		if($this->action_is_public($event->subject)) {
			return;
		}
			
		$role_name = $this->get_current_user_role_name();

		if ($role_name !== 'admin' && $role_name !== 'teacher'){
			return $this->redirect(['controller' => 'users', 'action' => 'dashboard']);
		}
	}
	
	//Similar to verify_teacher, provides clearance based on security level
	public function verify_admin(Event $event) {
		if($this->action_is_public($event->subject)) {
			return;
		}
		
		$role_name = $this->get_current_user_role_name();
		
		if ($role_name !== 'admin'){
			return $this->redirect(['controller' => 'users', 'action' => 'dashboard']);
		}
	}
	
	
	public function action_is_public($controller) {
		$action = strtolower($controller->request->params['action']);
		
		//echo "Checking Action: " . $action . ", Auth: " . json_encode(isset($this->Auth)) . "<br>";
	   
		if (in_array($action, array_map('strtolower', $this->Auth->allowedActions))) {
			return true;
		}	
		return false;
	}
	
	//Code found online - created by DrewM, subscribes a user to a mailchimp list when they register an accound
	public function mailchimp_subscribe($critical, $list_id, $email_address, $variables) {
		$returnString = 'failure'; 
		
		$MailChimp = new \DrewM\MailChimp\MailChimp('4a1cf8ca0cbe84127d1f97beb31f3bcd-us7');
		
		$post_array = [
			'email_address' => $email_address, 
			'status_if_new' => 'subscribed', 
			'status' => 'subscribed',
		];
		
		if(is_array($variables) && count($variables) > 0) {
			$post_array['merge_fields'] = $variables; 
		}
		
		$subscriberHash = $MailChimp->subscriberHash($email_address);
		
		$result = $MailChimp->put("lists/$list_id/members/$subscriberHash", $post_array);
		
		if ($MailChimp->success()) {
			$returnString = 'success'; 
		} else {
			if($critical) {
				$this->Flash->error('Sorry, but there was an error subscribing you to the list. Please try agian or contact us for assistance.');
				$this->redirect($this->referer());
			}
			
			$returnString = 'failure'; 
		}
		
		return $returnString; 
	}
	
	//Creates a new segment within the subscriber list based on the School Name 
	public function mailchimp_create_segment($critical, $list_id, $school_categories, $school_name) {
		$segment = 'failure'; 
		
		$MailChimp = new \DrewM\MailChimp\MailChimp('4a1cf8ca0cbe84127d1f97beb31f3bcd-us7');
		
		$school_interest = $MailChimp->post("lists/$list_id/interest-categories/$school_categories/interests", array(
			'name' => $school_name
		));
		
		if ($MailChimp->success()) {
			$segment = $school_interest['id']; 
		} else {
			if($critical) {
				$this->Flash->error('Sorry, but there was an error creating the list segment. Please try agian or contact us for assistance.');
				$this->redirect($this->referer());
			}
			
			$segment = 'failure'; 
		}
		
		return $segment; 
		
	}
	
	//Subscribes a user to the created segment (above) 
	public function mailchimp_subscribe_interest($critical, $list_id, $email_address, $interest_id) {
		
		$returnString = 'failure'; 
		
		$MailChimp = new \DrewM\MailChimp\MailChimp('4a1cf8ca0cbe84127d1f97beb31f3bcd-us7');
		
		$subscriberHash = $MailChimp->subscriberHash($email_address);
		
		$result = $MailChimp->patch("/lists/$list_id/members/$subscriberHash", array(
			'interests' => array(
				$interest_id => true
			)
		));
		
		if ($MailChimp->success()) {
			$returnString = 'success'; 
		} else {
			
			if($critical) {
				$this->Session->setFlash('Sorry, but there was an error updating your email subscription. Please try agian or contact us for assistance.', 'flash_error');
				$this->redirect($this->referer());
			}
			
			$returnString = 'failure'; 
		}
		
		return $returnString;
	}
	
	//unsubscribes the user from the mailing list
	public function mailchimp_unsubscribe($critical, $list_id, $email_address) {
		$returnString = 'failure'; 
		
		$MailChimp = new \DrewM\MailChimp\MailChimp('4a1cf8ca0cbe84127d1f97beb31f3bcd-us7');
		
		$subscriberHash = $MailChimp->subscriberHash($email_address);
		
		$result = $MailChimp->delete("lists/$list_id/members/$subscriberHash");
		
		if ($MailChimp->success() || $result['status'] === 404) {
			$returnString = 'success'; 
		} else {
			
			if($critical) {
				$this->Session->setFlash('Sorry, but there was an error unsubscribing you to the list. Please try agian or contact us for assistance.', 'flash_error');
				$this->redirect($this->referer());
			}
			
			$returnString = 'failure'; 
		}
		
		return $returnString;
	}
	
	//recaptcha in contact us page
	public function captcha($response){
		$cap = new GoogleRecaptcha();
		return $cap->VerifyCaptcha($response);
	}
	//function that create's a csv file with content, a file name and a file type
	public function create_csv($content_array, $filename, $response) {
		
		//Create output stream
		$output_stream = fopen("php://output", "w");
		
		$first_row = $content_array->first()->toArray();
		unset($first_row['_matchingData']);
		unset($first_row['full_name']);
		
		//echo "<pre>";
		//print_r($first_row);
		//echo "</pre>";
		//exit;
		
		//Create headers
		fputcsv($output_stream, array_keys($first_row));
		
		//Loop through content
		foreach($content_array as $row) {
			$row_convert = $row->toArray();
			unset($row_convert['_matchingData']);
			unset($row_convert['full_name']);
			fputcsv($output_stream, $row_convert);
		}
		
		fclose($output_stream);
		
		//serve file, with headers
		$response = $response->withType('csv');
		$response = $response->withDownload($filename . '.csv');
		
		return $response;
	}
	
	
	
	public function beforeFilter(Event $event) {
		$this->verify_admin($event);

		if(!$this->action_is_public($event->subject) && $this->get_current_user_role_name() === 'admin') {
			$this->viewBuilder()->setLayout('admin');
		}
	}
	
}
