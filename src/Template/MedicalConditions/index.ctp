<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\MedicalCondition[]|\Cake\Collection\CollectionInterface $medicalConditions
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Medical Condition'), ['action' => 'add']) ?></li>
       
    </ul>
</nav>
<div class="medicalConditions index large-9 medium-8 columns content">
    <h3><?= __('Medical Conditions') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($medicalConditions as $medicalCondition): ?>
            <tr>
                <td><?= $this->Number->format($medicalCondition->medical_id) ?></td>
                <td><?= h($medicalCondition->medical_conditions) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $medicalCondition->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $medicalCondition->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $medicalCondition->id], ['confirm' => __('Are you sure you want to delete # {0}?', $medicalCondition->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator text-center">
        <ul class="pagination justify-content-center">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('<i class="fa fa-chevron-left" aria-hidden="true"></i> ' . __('Previous'), ['escape' => false]) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Next') . ' <i class="fa fa-chevron-right" aria-hidden="true"></i>', ['escape' => false]) ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p class="text-muted"><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
