<div class="container">
	<?php echo $this->cell('CMS', [$page]); ?>

	<div class="row">
		<div class="col-12 col-md-6">
			<h2 class="heading">Send Us a Message</h2>
				
			<?php echo $this->Form->create($contactMessage, array('class' => 'form-contact', 'autocomplete' => 'disabled')); ?>

				<?php echo $this->Form->control('first_name',['type' => 'text','placeholder'=>'John']); ?>
				<?php echo $this->Form->control('last_name', ['type' => 'text','placeholder'=>'Smith']); ?>
				<?php echo $this->Form->control('user_email', ['type' => 'email', 'placeholder' => 'JohnSmith@gmail.com']); ?>
				<?php echo $this->Form->control('message_details', ['type' => 'textarea','placeholder' => 'Enter your Message or any questions']); ?>
				
				<div class="form-group text-center">
					<div class="g-recaptcha" data-sitekey="6LcErjsUAAAAAAG48e5i6Fh8LWvOiXHM3VSgc7Xd"></div>
				</div>

				<div class="form-group">
					<button class="btn btn-lg btn-primary btn-block type="submit">Send Message&nbsp;&nbsp;<i class="fa fa-paper-plane" aria-hidden="true"></i></button>
				</div>
				
			<?php echo $this->Form->end(); ?>
		</div>
		<div class="col-12 col-sm-6">
			<div class="embed-responsive embed-responsive-1by1">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6297.4915975631275!2d145.0543061325063!3d-37.88962707973877!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad6685aac78e619%3A0x75fe65dbcff6a0d7!2sDrama+Time!5e0!3m2!1sen!2sau!4v1513761674500" class="embed-responsive-item" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</div>
	</div>
</div>
 
<script src='https://www.google.com/recaptcha/api.js'></script>
