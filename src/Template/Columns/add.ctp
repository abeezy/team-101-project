<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Columns'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Rows'), ['controller' => 'Rows', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Row'), ['controller' => 'Rows', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="columns form large-9 medium-8 columns content">
    <?= $this->Form->create($column) ?>
    <fieldset>
        <legend><?= __('Add Column') ?></legend>
        <?php
            echo $this->Form->control('row_id', ['options' => $rows]);
            echo $this->Form->control('content');
            echo $this->Form->control('column_order');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
