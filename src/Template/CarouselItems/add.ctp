<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="container">
<br></br>
<div class="row justify-content-center">
		<div class="col-md-6">
			<h2>Add Carousel Picture</h2>
			<?php echo $this->Form->create($carouselItem, ['enctype' => 'multipart/form-data']); ?>
				<?php
					echo $this->Form->control('name');
					echo $this->Form->control('webpage_id', ['type' => 'hidden', 'value' => $webpage_id]);
					echo $this->Form->control('image', ['type' => 'hidden']);
				?>
				<div class="form-group">
					<label>Image</label>
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#imageModal">Select Image</button>
				</div>
				<?php echo $this->Form->button(__('Submit'), array('class' => 'btn btn-primary')); ?>
			<?php echo $this->Form->end() ?>
		</div>
	</div>
</div>
<div class="modal" tabindex="-1" role="dialog" id="imageModal">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Select Image</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		<iframe style="width: 100%; height: 500px;" src="/team101/responsive_filemanager/filemanager/dialog.php?type=1&field_id=image"></iframe>
		<script type="text/javascript">
			function responsive_filemanager_callback(field_id){
				console.log(field_id);
				var url=jQuery('#'+field_id).val();
				//alert('update '+field_id+" with "+url);
				//your code
			}
		</script>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</div>
