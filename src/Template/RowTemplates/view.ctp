<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\RowTemplate $rowTemplate
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Row Template'), ['action' => 'edit', $rowTemplate->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Row Template'), ['action' => 'delete', $rowTemplate->id], ['confirm' => __('Are you sure you want to delete # {0}?', $rowTemplate->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Row Templates'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Row Template'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Rows'), ['controller' => 'Rows', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Row'), ['controller' => 'Rows', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="rowTemplates view large-9 medium-8 columns content">
    <h3><?= h($rowTemplate->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($rowTemplate->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($rowTemplate->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Column Count') ?></th>
            <td><?= $this->Number->format($rowTemplate->column_count) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Content') ?></h4>
        <?= $this->Text->autoParagraph(h($rowTemplate->content)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Rows') ?></h4>
        <?php if (!empty($rowTemplate->rows)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Row Template Id') ?></th>
                <th scope="col"><?= __('Webpage Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($rowTemplate->rows as $rows): ?>
            <tr>
                <td><?= h($rows->id) ?></td>
                <td><?= h($rows->row_template_id) ?></td>
                <td><?= h($rows->webpage_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Rows', 'action' => 'view', $rows->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Rows', 'action' => 'edit', $rows->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Rows', 'action' => 'delete', $rows->id], ['confirm' => __('Are you sure you want to delete # {0}?', $rows->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
