<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\ContactMessage $contactMessage
  */
?>

<div class="container">

<div class="contactMessages view large-9 medium-8 columns content">
 
   <table class="table table-responsive table-striped bg-white">
        <tr>
            <th scope="row"><?= __('User Email') ?></th>
            <td><?= h($contactMessage->user_email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Time') ?></th>
            <td><?= h($contactMessage->created->i18nFormat('dd/MM/yyyy HH:mm')) ?></td>
        </tr>
		<tr>
			<th>First Name</th>
			<td><?php echo h($contactMessage->first_name); ?></td>
		</tr>
		<tr>
			<th>Last Name</th>
			<td><?php echo h($contactMessage->last_name); ?></td>
		</tr>
		<tr>
			<th>Message</th>
			<td><?php echo $this->Text->autoParagraph(h($contactMessage->message_details)); ?></td>
		</tr>
    </table> 
</div>
					<button type="button" class = 'btn btn-light' onclick="goBack()">Go Back</button>

<script>
function goBack() {
    window.history.back();
}
</script>
</div>
