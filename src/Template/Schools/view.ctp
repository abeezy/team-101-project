<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\School $school
  */
?>
<div class="row justify-content-between align-items-center mb-3">
	<div class="col-auto">
		<h3 class="mb-0"><?php echo $school->school_name; ?></h3>
	</div>
	<div class="col-auto">
		<?php echo $this->Html->link('<i class="fas fa-pencil-alt"></i>&nbsp;&nbsp;Edit School', ['action' => 'edit', $school->id], ['class' => 'btn btn-outline-primary', 'escape' => false]) ?></li>
	</div>
</div>
<div class="schools view large-9 medium-8 columns content">
	<div class="card mb-3">
		<table class="table table-responsive table-striped mb-0">
			<tr>
				<th scope="row"><?= __('School Name') ?></th>
				<td><?= h($school->school_name) ?></td>
			</tr>
			<tr>
				<th scope="row"><?= __('School Address') ?></th>
				<td><?= h($school->school_address) ?></td>
			</tr>
			<tr>
				<th scope="row"><?= __('Notes') ?></th>
				<td><?= h($school->notes) ?></td>
			</tr>
		</table>
	</div>
    <div class="related mb-3">
        <div class="card">
			<div class="card-header">
				<div class="row justify-content-between align-items-center">
					<div class="col-auto">
						<h4 class="mb-0">Current Classes</h4>
					</div>
					<div class="col-auto">
						<?php echo $this->Html->link('<i class="fas fa-plus"></i>&nbsp;&nbsp;Add New', ['controller' => 'Classes', 'action' => 'add'], ['class' => 'btn btn-outline-primary', 'escape' => false]) ?></li>
					</div>
				</div>
			</div>
			<?php if (!empty($school->classes)): ?>
				<table class="table table-responsive table-striped mb-0">
					<thead>
						<tr>
							<th scope="col"><?= __('Term') ?></th>
							<th scope="col"><?= __('Class') ?></th>
							<th scope="col"><?= __('Teacher') ?></th>
							<th scope="col"><?= __('Period') ?></th>
							<th scope="col"><?= __('Time') ?></th>
							<th scope="col"><?= __('Address') ?></th>
						</tr>
					</thead>
					<tbody>
						<?php $currentCount = 0; ?>
						<?php foreach ($school->classes as $class): ?>
							<?php if ($class->end_date->isFuture()): $currentCount++; ?>
								<tr>
									<td><?= h($class->class_term) ?></td>
									<td><?= $this->Html->link($class->name, ['controller' => 'Classes', 'action' => 'view', $class->id]) ?></td>
									<td><?= h($class->teacher->full_name) ?></td>
									<td><?= h($class->start_date->i18nFormat('dd/MM/yyyy'))?> - <?= h($class->end_date->i18nFormat('dd/MM/yyyy')) ?></td>
									<td><?= h($class->class_day) ?> <?= h($class->class_time->i18nFormat('hh:mm a')) ?></td>
									<td><?= h($class->class_address) ?></td>
								</tr>
							<?php endif; ?>
						<?php endforeach; ?>
						<?php if ($currentCount === 0): ?>
							<tr>
								<td colspan="6">
									There are no current classes
								</td>
							</tr>
						<?php endif; ?>
					</tbody>
				</table>
			<?php endif; ?>
		</div>
    </div>
	<div class="related mb-3">
        <div class="card">
			<div class="card-header">
				<div class="row justify-content-between align-items-center">
					<div class="col-auto">
						<h4 class="mb-0">Past Classes</h4>
					</div>
				</div>
			</div>
			<?php if (!empty($school->classes)): ?>
				<table class="table table-responsive table-striped mb-0">
					<thead>
						<tr>
							<th scope="col"><?= __('Term') ?></th>
							<th scope="col"><?= __('Class') ?></th>
							<th scope="col"><?= __('Teacher') ?></th>
							<th scope="col"><?= __('Period') ?></th>
							<th scope="col"><?= __('Time') ?></th>
							<th scope="col"><?= __('Address') ?></th>
						</tr>
					</thead>
					<tbody>
						<?php $pastCount = 0; ?>
						<?php foreach ($school->classes as $class): ?>
							<?php if ($class->end_date->isPast()): $pastCount++; ?>
								<tr>
									<td><?= h($class->class_term) ?></td>
									<td><?= $this->Html->link($class->name, ['controller' => 'Classes', 'action' => 'view', $class->id]) ?></td>
									<td><?= h($class->teacher->full_name) ?></td>
									<td><?= h($class->start_date->i18nFormat('dd/MM/yyyy'))?> - <?= h($class->end_date->i18nFormat('dd/MM/yyyy')) ?></td>
									<td><?= h($class->class_day) ?> <?= h($class->class_time->i18nFormat('HH:mm a')) ?></td>
									<td><?= h($class->class_address) ?></td>
								</tr>
							<?php endif; ?>
						<?php endforeach; ?>
						<?php if ($pastCount === 0): ?>
							<tr>
								<td colspan="6">
									There are no past classes
								</td>
							</tr>
						<?php endif; ?>
					</tbody>
				</table>
			<?php endif; ?>
		</div>
    </div>
	<?php
		$currentCount = 0;
		foreach ($school->classes as $class){ 
			if($class->end_date->isFuture()){
				$currentCount += count($class->enrolments);
			}
		}
	?>
    <div class="related mb-3">
        <div class="card">
			<div class="card-header">
				<div class="row justify-content-between align-items-center">
					<div class="col-auto">
						<h4 class="mb-0">Current Students</h4>
					</div>
					<div class="col-auto">
						<?php echo $this->Html->link('<i class="fas fa-plus"></i>&nbsp;&nbsp;Add New', ['controller' => 'Students', 'action' => 'add'], ['class' => 'btn btn-outline-primary', 'escape' => false]) ?></li>
						<?php if ($currentCount !== 0): ?>
							&nbsp;
							<?php echo $this->Html->link('<i class="fas fa-download"></i> Download CSV', array('controller' => 'students', 'action' => 'currentStudentsBySchoolCsv', $school->id), array('class' => 'btn btn-primary', 'escape' => false)); ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<table class="table table-responsive table-striped mb-0">
				<thead>
					<tr>
						<th scope="col"><?= __('Name') ?></th>
						<th scope="col"><?= __('Notes') ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($school->classes as $class): ?>
						<?php if($class->end_date->isFuture()): ?>
							<?php foreach ($class->enrolments as $enrolment): ?>
								<tr>
									<td><?php echo $this->Html->link($enrolment->student->full_name, ['controller' => 'Students', 'action' => 'view', $enrolment->student->id]) ?></td>
									<td><?php if($enrolment->student->notes): ?><em>Yes</em><?php endif; ?></td>
								</tr>
							<?php endforeach; ?>
						<?php endif; ?>
					<?php endforeach; ?>
					<?php if ($currentCount === 0): ?>
						<tr>
							<td colspan="2">
								There are no current students
							</td>
						</tr>
					<?php endif; ?>
				</tbody>
			</table>
		</div>
	</div>
	<?php
		$pastCount = 0;
		foreach ($school->classes as $class){ 
			if($class->end_date->isPast()){
				$pastCount += count($class->enrolments);
			}
		}
	?>
	<div class="related mb-3">
        <div class="card">
			<div class="card-header">
				<div class="row justify-content-between align-items-center">
					<div class="col-auto">
						<h4 class="mb-0">Past Students</h4>
					</div>
					<?php if ($pastCount !== 0): ?>
						<div class="col-auto">
							<?php echo $this->Html->link('<i class="fas fa-download"></i> Download CSV', array('controller' => 'students', 'action' => 'pastStudentsBySchoolCsv', $school->id), array('class' => 'btn btn-primary', 'escape' => false)); ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
			<table class="table table-responsive table-striped mb-0">
				<thead>
					<tr>
						<th scope="col"><?= __('Name') ?></th>
						<th scope="col"><?= __('Notes') ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($school->classes as $class): ?>
						<?php if($class->end_date->isPast()): ?>
							<?php foreach ($class->enrolments as $enrolment): ?>
								<tr>
									<td><?php echo $this->Html->link($enrolment->student->full_name, ['controller' => 'Students', 'action' => 'view', $enrolment->student->id]) ?></td>
									<td><?php if($enrolment->student->notes): ?><em>Yes</em><?php endif; ?></td>
								</tr>
							<?php endforeach; ?>
						<?php endif; ?>
					<?php endforeach; ?>
					<?php if ($pastCount === 0): ?>
						<tr>
							<td colspan="2">
								There are no past students
							</td>
						</tr>
					<?php endif; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
