<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PaymentHistory Entity
 *
 * @property int $id
 * @property int $enrolment_id
 * @property string $amount
 * @property \Cake\I18n\FrozenTime $datetime
 * @property string $payment_status
 *
 * @property \App\Model\Entity\Enrolment $enrolment
 */
class PaymentHistory extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
