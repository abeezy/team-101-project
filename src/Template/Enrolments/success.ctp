<div class="container">
	<div class="row justify-content-center">
		<div class="col-auto px-4 py-2 mx-3 progress-chevron">
			Parent
		</div>
		<div class="col-auto px-4 py-2 mx-3 progress-chevron">
			Student
		</div>
		<div class="col-auto px-4 py-2 mx-3 progress-chevron">
			Cart
		</div>
		<div class="col-auto px-4 py-2 mx-3 progress-chevron">
			Payment
		</div>
		<div class="col-auto px-4 py-2 mx-3 progress-chevron active">
			Summary
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-md-6">
			<h2 class="text-center">Welcome to Drama Time!</h2>
			<p class="text-center">Your purchase has been successful ... </p>
			<p>A copy of your receipt has been sent to your email.</p>
		</div>
	</div>
	<?php foreach($purchase_data['cart']['items'] as $item): ?>
		<div class="row">
			<div class="col">
				<div class="card">
					<div class="card-header">
						<div class="row justify-content-between">
							<div class="col">
								<?php echo $item['Student']['full_name']; ?>
							</div>
							<div class="col-auto">
								X
							</div>
						</div>
					</div>
					<ul class="list-group list-group-flush">
					    <li class="list-group-item"><strong>School:</strong> <?php echo $item['Class']['school']['school_name']; ?></li>
						<li class="list-group-item"><strong>Class:</strong> <?php echo $item['Class']['name']; ?></li>
						<li class="list-group-item"><strong>Enrolment Period:</strong> From <?php echo $item['first_lesson']['lesson_date']->i18nFormat('dd/MM/yyyy'); ?> until <?php echo $item['last_lesson']['lesson_date']->i18nFormat('dd/MM/yyyy'); ?></li>
						<li class="list-group-item"><strong>Price:</strong> <?php echo $this->Number->currency($item['price']); ?> AUD</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<hr/>
			</div>
		</div>
	<?php endforeach; ?>
	<?php if(count($purchase_data['cart']['items']) > 1): ?>
		<div class="row">
			<div class="col">
				<strong>Sibling Discount:</strong>  10% 
				<br>
				<strong>Discount:</strong>  <?php echo $this->Number->currency(($purchase_data['cart']['total_price'] / 9)); ?>
			</div>
		</div>
	<?php endif; ?>
	<div class="row">
		<div class="col">
			<strong>Total Price:</strong> <?php echo $this->Number->currency($purchase_data['cart']['total_price']); ?> AUD
		</div>
	</div>
	<div class="row justify-content-end">
		<div class="col-auto">
			<?php echo $this->Html->link('Back to Dashboard', array('controller' => 'users', 'action' => 'dashboard'), array('class' => 'btn btn-primary')); ?>
		</div>
	</div>
</div>