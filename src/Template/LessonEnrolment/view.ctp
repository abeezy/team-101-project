<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\LessonEnrolment $lessonEnrolment
  */
?>
<div class="container">
	<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Lesson Enrolment'), ['action' => 'edit', $lessonEnrolment->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Lesson Enrolment'), ['action' => 'delete', $lessonEnrolment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $lessonEnrolment->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Lesson Enrolment'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Lesson Enrolment'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Lessons'), ['controller' => 'Lessons', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Lesson'), ['controller' => 'Lessons', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Enrolments'), ['controller' => 'Enrolments', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Enrolment'), ['controller' => 'Enrolments', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="lessonEnrolment view large-9 medium-8 columns content">
    <h3><?= h($lessonEnrolment->id) ?></h3>
       <table class="table table-responsive table-striped bg-white">
        <tr>
            <th scope="row"><?= __('Lesson') ?></th>
            <td><?= $lessonEnrolment->has('lesson') ? $this->Html->link($lessonEnrolment->lesson->id, ['controller' => 'Lessons', 'action' => 'view', $lessonEnrolment->lesson->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Enrolment') ?></th>
            <td><?= $lessonEnrolment->has('enrolment') ? $this->Html->link($lessonEnrolment->enrolment->id, ['controller' => 'Enrolments', 'action' => 'view', $lessonEnrolment->enrolment->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($lessonEnrolment->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Attended') ?></th>
            <td><?= $lessonEnrolment->attended ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>
</div>