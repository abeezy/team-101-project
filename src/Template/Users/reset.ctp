<div class="container">

		<div class="row justify-content-center">
			<div class="col-md-4">
<?php $this->assign('title', 'Reset Password'); ?>
<div class="users form large-9 medium-8 columns content">
    <?php echo $this->Form->create($user) ?>
    <fieldset>
        	<h2 class="text-center">Reset Password</h2>
    <?php
        echo $this->Form->control('password', ['required' => true, 'type' => 'password']); ?>

    <?php
        echo $this->Form->control('cPassword', ['minLength' => 6,'placeholder' => 'Confirm Password must match your password', 'label' => 'Confirm Password', 'type' => 'password']); 
    ?>
    </fieldset>
	<button class="btn btn-primary btn-block" type="submit">Submit</button>

    <?php echo $this->Form->end(); ?>
</div>
</div>
</div>
