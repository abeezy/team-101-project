<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="container">

<div class="row justify-content-center">
		<div class="col-md-4">
    <?= $this->Form->create($paymentHistory) ?>
    <fieldset>
        <legend><?= __('Edit Payment History') ?></legend>
        <?php
            echo $this->Form->control('enrolment_id', ['options' => $enrolments]);
            echo $this->Form->control('amount');
            echo $this->Form->control('datetime');
            echo $this->Form->control('payment_status');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
</div>