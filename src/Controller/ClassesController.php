<?php
namespace App\Controller;
use Cake\Event\Event;
use App\Controller\AppController;


/**
 * Classes Controller
 *
 * @property \App\Model\Table\ClassesTable $Classes
 *
 * @method \App\Model\Entity\Class[] paginate($object = null, array $settings = [])
 */
class ClassesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Schools', 'Teachers' => ['Users']],
			'sortWhitelist' => ['Schools.school_name'],
			'order' => ['Schools.school_name' => 'ASC'],
        ];
        $classes = $this->paginate($this->Classes);

        $this->set(compact('classes'));
        $this->set('_serialize', ['classes']);
		
    }

    /**
     * View method
     *
     * @param string|null $id Class id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */

	  public function classIndex()
    {
          $this->paginate = [
			'conditions' => [
				'Users.id' => $this->Auth->user('id')
			],
            'contain' => [
				'Schools',
				'Teachers' => [
					'Users'
				]
			]
        ];
        $classes = $this->paginate($this->Classes);

        $this->set(compact('classes'));
        $this->set('_serialize', ['classes']);
    }





    public function view($id = null)
    {
        $class = $this->Classes->get($id, [
            'contain' => [
              'Schools',
              'Teachers' => [
                'Users'
              ],
			  'Lessons' => [
				'Teachers' => [
					'Users'
				  ]
			  ]
              /*'Enrolments' => [
                'Students' => function ($q) {
					return $q
				}
				/*'LessonEnrolment' => function ($q) {
					return $q->matching('Lessons')->distinct();//->contains('Teachers.Users');
				}*/
              //],
				/*'Lessons' => function ($q) {
					return $q->matching('LessonEnrolment')->distinct();//->contains('Teachers.Users');
				}*/
          ]
        ]);
		
		$this->loadModel('Students');
		$students = $this->Students->find('all')->leftJoinWith('Enrolments.Classes')->where(['Classes.id' => $id])->contain('Enrolments.LessonEnrolment.Lessons', function($q) use ($id) {
			return $q->where(['Lessons.class_id' => $id])->distinct();
		})->distinct();
		

        $this->set('class', $class);
		$this->set('students', $students->all());

        $this->set('_serialize', ['class']);
    }



	 public function teacherView($id = null)
    {
        $class = $this->Classes->get($id, [
            'contain' => [
              'Schools',
              'Teachers' => [
                'Users'
              ],
              'Enrolments' => [
                'Students'
              ],
              'Lessons' => [
                'Teachers' => [
                  'Users'
                ]
              ]
          ]
        ]);

        $this->set('class', $class);
        $this->set('_serialize', ['class']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $class = $this->Classes->newEntity();
        if ($this->request->is('post')) {
            $class = $this->Classes->patchEntity($class, $this->request->getData());
            if ($this->Classes->save($class)) {
                $this->Flash->success(__('The class has been saved.'));

				$this->loadModel('Lessons');
				
							
			

				
				
				

				$date = $class->start_date;

				// Create lessons for eack week of the class
				while($date <= $class->end_date) {

					// Get the next lesson date after the start of the user's enrolment, then each week afterwards using the loop

					$lesson = $this->Lessons->newEntity();
					$lesson->class_id = $class->id;
					$lesson->teacher_id = $class->teacher_id;
					$lesson->lesson_date = $date;

					$this->Lessons->save($lesson);

					$date = $date->modify('+1 week');
				}

                return $this->redirect(['controller' => 'schools', 'action' => 'view', $class->school_id]);
            }
            $this->Flash->error(__('The class could not be saved. Please, try again.'));
        }
        $schools = $this->Classes->Schools->find('list', ['limit' => 200]);
        	$this->loadModel('Teachers');
			$teachers = $this->Teachers->find('list');
			


        $this->set(compact('class', 'schools', 'teachers'));
        $this->set('_serialize', ['class']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Class id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $class = $this->Classes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $class = $this->Classes->patchEntity($class, $this->request->getData());
            if ($this->Classes->save($class)) {
                $this->Flash->success(__('The class has been saved.'));

                return $this->redirect(['controller' => 'classes', 'action' => 'view', $id]);
            }
            $this->Flash->error(__('The class could not be saved. Please, try again.'));
        }
        $schools = $this->Classes->Schools->find('list', ['limit' => 200]);
        $this->loadModel('Teachers');
		$teachers = $this->Teachers->find('list');
		
		$this->set(compact('class', 'schools', 'teachers'));
        $this->set('_serialize', ['class']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Class id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $class = $this->Classes->get($id);
        if ($this->Classes->delete($class)) {
            $this->Flash->success(__('The class has been deleted.'));
        } else {
            $this->Flash->error(__('The class could not be deleted. Please, try again.'));
        }

         return $this->redirect(['controller' => 'schools', 'action' => 'view', $class->school_id]);

    }

	public function all() {
        $this->loadModel('Schools');

		$schools = $this->Schools->find()->matching('Classes', function ($q) {
			return $q->where(array(
				'Classes.end_date >' => time()
			))->matching('Lessons', function ($q2) {
				return $q2->where(array(
					'Lessons.lesson_date >' => time()
				));
			});
		})->contain('Classes', function ($q) {
			return $q->where(array(
				'Classes.end_date >' => time()
			))->contain('Lessons', function ($q2) {
				return $q2->where(array(
					'Lessons.lesson_date >' => time()
				));
			})->contain(array(
				'Teachers.Users'
			));
		//})->distinct('Classes.id')->all();
		})->distinct(['Schools.id'])->all();

        $this->set(compact('schools'));
    }

	public function upcomingLessons($id = null) {
		$this->viewBuilder()->setLayout('ajax');
		if($id !== null) {
            $class = $this->Classes->get($id, array('contain' => array('Lessons')));
           
			echo json_encode(array('price_per_lesson' => $class->price, 'lessons' => $class->lessons));
			return $this->response;
		}
	}
	
	public function search() {
		$this->viewBuilder()->setLayout('ajax');
		$classes = $this->Classes->find()
			->where(['LOWER(name) LIKE' => '%' . strtolower($this->request->getQuery('term')) . '%'])
			->orWhere(['LOWER(class_address) LIKE' => '%' . strtolower($this->request->getQuery('term')) . '%'])
			->contain(['Schools'])
			->all();
			
		$this->set(compact('classes'));
		$this->set('_serialize', false);
	}
	
	

	public function beforeFilter(Event $event){
		$this->Auth->allow(['all']);
		if(!$this->action_is_public($event->subject) && $this->get_current_user_role_name() === 'parent') {
			$this->Auth->allow(['upcomingLessons']);
		}

		if(!$this->action_is_public($event->subject) && $this->get_current_user_role_name() === 'teacher') {
			$this->Auth->allow(['classIndex','teacherView']);
		}

		parent::beforeFilter($event);
	}
}
