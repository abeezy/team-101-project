<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LessonEnrolmentTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LessonEnrolmentTable Test Case
 */
class LessonEnrolmentTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\LessonEnrolmentTable
     */
    public $LessonEnrolment;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.lesson_enrolment',
        'app.lessons',
        'app.classes',
        'app.schools',
        'app.students',
        'app.grades',
        'app.attendences',
        'app.enrolments',
        'app.payment_history',
        'app.student_contact',
        'app.users',
        'app.roles',
        'app.teachers',
        'app.contact_type'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('LessonEnrolment') ? [] : ['className' => LessonEnrolmentTable::class];
        $this->LessonEnrolment = TableRegistry::get('LessonEnrolment', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->LessonEnrolment);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
