$(function() {
  function showMsg(title,text) {
    $('#messageModalTitle').text(title);
    if(text[0] !== '<'){
      text = '<p>' + text + '</p>';
    }
    $('#messageModalContent').html(text);
    $('#messageModal').modal();
  }
  //Add custom event handler for info modals
  $('[data-target="#messageModal"]').on('click', function(event, element) {
    showMsg($(this).attr('title'), $(this).data('content'));
  });

  $('[data-toggle="tooltip"]').tooltip();
  
  $('.datepicker-input').each(function (e) {
	 $(this).datepicker({
		format: 'yyyy-mm-dd',
		autoclose: true
	 });
  });

  goBack = function() {
      window.history.back();
  }

  $('.content-editor').each(function (e) {
	 CKEDITOR.replace($(this).attr('id'), {
		autoGrow_onStartup: true,
		extraAllowedContent: '*[id];iframe(*){*}[*];',
		baseHref : document.getElementsByTagName('base')[0].href,
		filebrowserBrowseUrl : document.getElementsByTagName('base')[0].href + 'responsive_filemanager/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
		filebrowserUploadUrl : document.getElementsByTagName('base')[0].href + 'responsive_filemanager/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
		filebrowserImageBrowseUrl : document.getElementsByTagName('base')[0].href + 'responsive_filemanager/filemanager/dialog.php?type=1&editor=ckeditor&fldr=',
		image_prefillDimensions : false,
		removeDialogTabs : 'image:Upload',
		on: {
			instanceReady: function() {
				this.dataProcessor.htmlFilter.addRules( {
					elements: {
						img: function( el ) {
							// Add some class.
							el.addClass( 'img-fluid');
						},
						a: function( el ) {
							// Add some class.
							el.addClass( 'btn btn-secondary');
						}
					}
				} );
			}
		}
	 });
  });

  if( $.isFunction( $.fn.autocomplete ) ){

	  $.widget('custom.catComplete', $.ui.autocomplete, {
		_renderMenu: function(ul, items) {
		  var self = this, currentCategory = "";
		  $.each(items, function(index, item) {
			if (item.category != currentCategory) {
			  ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
			  currentCategory = item.category;
			}
			self._renderItemData(ul, item);
		  });
		}
	  });

	  var users, students, classes, schools;

	  $('.quickSearch').each(function (e) {

		//Get paths from data variables
		var users_path = $(this).data('users-path');
		var students_path = $(this).data('students-path');
		var classes_path = $(this).data('classes-path');
		var schools_path = $(this).data('schools-path');

		$(this).catComplete({
		  source: function(request, response) {
			$.when(
			  $.ajax({
				url: users_path,
				dataType: 'json',
				data: {
				  term: request.term
				},
				success: function(data) {
				  users = data;
				}
			  }),
			  $.ajax({
				url: students_path,
				dataType: 'json',
				data: {
				  term: request.term
				},
				success: function(data) {
				  students = data;
				}
			  }),
			  $.ajax({
				url: classes_path,
				dataType: 'json',
				data: {
				  term: request.term
				},
				success: function(data) {
				  classes = data;
				}
			  }),
			  $.ajax({
				url: schools_path,
				dataType: 'json',
				data: {
				  term: request.term
				},
				success: function(data) {
				  schools = data;
				}
			  })
			).then(function() {
			  var results = users.concat(students).concat(classes).concat(schools);
			  console.log('Results:', results);
			  response(results);
			})
		  },
		  minLength: 2,
		  delay: 10,
		  select: function(event, ui) {
			window.location.assign(ui.item.url);
		  },
		  open: function (){$('.ui-autocomplete-category').removeClass('ui-menu-item');}
		});
	  });
  }

});
