<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MedicalConditions Model
 *
 * @property |\Cake\ORM\Association\BelongsTo $Medicals
 *
 * @method \App\Model\Entity\MedicalCondition get($primaryKey, $options = [])
 * @method \App\Model\Entity\MedicalCondition newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MedicalCondition[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MedicalCondition|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MedicalCondition patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MedicalCondition[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MedicalCondition findOrCreate($search, callable $callback = null, $options = [])
 */
class MedicalConditionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('medical_conditions');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

         $this->hasMany('Students', [
            'foreignKey' => 'student_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('medical_conditions')
            ->requirePresence('medical_conditions', 'create')
            ->notEmpty('medical_conditions');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['medical_id'], 'Medicals'));

        return $rules;
    }
}
