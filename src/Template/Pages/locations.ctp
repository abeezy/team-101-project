<div class="container">
	<div class="row">
		<div class="col-12 col-sm-6">
			<div class="embed-responsive embed-responsive-16by9">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3148.7457987815637!2d145.05649481495922!3d-37.88962707973877!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad6685aac78e619%3A0x75fe65dbcff6a0d7!2sDrama+Time!5e0!3m2!1sen!2sau!4v1513761098993" class="embed-responsive-item" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</div>
	  
			
		<div class="col-12 col-sm-3">
			<h2>Hours</h2>
			<p>Monday-Friday: 9am - 5pm</p>
		</div>
		<div class="col-12 col-sm-3">
			<h2>Address</h2>
			<p>Address: 333 Neerim Rd, Carnegie VIC 3163</p>
		</div>
	</div>
	<div class="row"> 
		<div class="col-12 col-sm-6">
			<h2>Schools</h2>
			<p>Brighton Beach Primary School</p>
			<ul>230 Outer Crescent</ul>
			<p>Auburn Primary School</p>
			<ul>19 Aubrun Road</ul>
			<p>Richmond Primary School</p>
			<ul>988 Victoria Street</ul>
		</div>
   </div>
</div>
