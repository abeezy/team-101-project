<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="container">
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Contact Messages'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="contactMessages form large-9 medium-8 columns content">
    <?= $this->Form->create($contactMessage) ?>
    <fieldset>
        <legend><?= __('Add Contact Message') ?></legend>
        <?php
            echo $this->Form->control('first_name');
            echo $this->Form->control('last_name');
            echo $this->Form->control('user_email');
            echo $this->Form->control('time', ['empty' => true]);
            echo $this->Form->control('message_details');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
