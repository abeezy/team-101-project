	<?php
	/**
	  * @var \App\View\AppView $this
	  * @var \App\Model\Entity\Testimonial[]|\Cake\Collection\CollectionInterface $testimonials
	  */
	?>
<div class="container">
	<nav class="large-3 medium-4 columns" id="actions-sidebar">
		<ul class="side-nav">
			<li class="heading"><?= __('Actions') ?></li>
			<li><?= $this->Html->link(__('New Testimonial'), ['action' => 'add']) ?></li>
		</ul>
	</nav>
	<div class="testimonials index large-9 medium-8 columns content">
		<h3><?= __('Testimonials') ?></h3>
		<table cellpadding="0" cellspacing="0">
			<thead>
				<tr>
					<th scope="col"><?= $this->Paginator->sort('id') ?></th>
					<th scope="col"><?= $this->Paginator->sort('name') ?></th>
					<th scope="col"><?= $this->Paginator->sort('school') ?></th>
					<th scope="col"><?= $this->Paginator->sort('content') ?></th>
					<th scope="col" class="actions"><?= __('Actions') ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($testimonials as $testimonial): ?>
				<tr>
					<td><?= $this->Number->format($testimonial->id) ?></td>
					<td><?= h($testimonial->name) ?></td>
					<td><?= h($testimonial->school) ?></td>
					<td><?= h($testimonial->content) ?></td>
					<td class="actions">
						<?= $this->Html->link(__('View'), ['action' => 'view', $testimonial->id]) ?>
						<?= $this->Html->link(__('Edit'), ['action' => 'edit', $testimonial->id]) ?>
						<?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $testimonial->id], ['confirm' => __('Are you sure you want to delete # {0}?', $testimonial->id)]) ?>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		<div class="paginator text-center">
			<ul class="pagination justify-content-center">
				<?= $this->Paginator->first('<< ' . __('first')) ?>
				<?= $this->Paginator->prev('<i class="fa fa-chevron-left" aria-hidden="true"></i> ' . __('Previous'), ['escape' => false]) ?>
				<?= $this->Paginator->numbers() ?>
				<?= $this->Paginator->next(__('Next') . ' <i class="fa fa-chevron-right" aria-hidden="true"></i>', ['escape' => false]) ?>
				<?= $this->Paginator->last(__('last') . ' >>') ?>
			</ul>
			<p class="text-muted"><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
		</div>
	</div>
</div>