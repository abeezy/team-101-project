<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="container">
<div class="students form large-9 medium-8 columns content">
<h2>Edit User</h2>
		<?= $this->Form->create($user) ?>
		<fieldset>
		
			<?php
				echo $this->Form->control('role_id'); 
				echo $this->Form->control('first_name',['type' => 'text', 'placeholder' => 'John']); 
				echo $this->Form->control('last_name', ['type' => 'text', 'placeholder' => 'Smith']); 
				echo $this->Form->control('user_email', ['type' => 'email', 'placeholder' => 'JohnSmith@gmail.com']);
				echo $this->Form->control('phone_number', ['type' => 'tel', 'placeholder' => '0400 000 123']); 
				echo $this->Form->control('notes', ['type' => 'textarea','placeholder' => 'Other Requirements or Concerns']);
				echo $this->Form->control('admin_notes', ['type' => 'textarea','placeholder' => 'Other Requirements or Concerns']);

			?>
		</fieldset>
		 <?php echo $this->Form->button(__('Save'), array('class' => 'btn btn-light')); ?>
		<button type="button" class = 'btn btn-light' onclick="goBack()">Go Back</button>

<script>
function goBack() {
    window.history.back();
}
</script>
		<?= $this->Form->end() ?>
	</div>
</div>
</div>
