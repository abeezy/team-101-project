<div class="container">
  <div class="row">
    <div class="col-12 col-md-9">
      <div class="jumbotron">
        <h1>Hello <?php echo $parent['first_name']; ?>,</h1>
        <p>View your details below.</p>
      </div>

      <div class="row">
        <div class="col">
          <?php $parent; ?>
        </div>
      </div>
    <div class="row">
      <div class="col">
        <h3><?= __('My Details') ?></h3>
        <!--add parent form redirect button -->
        <?php echo $this->Html->link('Edit Details', array('controller' => 'users', 'action' => 'user_edit'), array('class' => 'btn btn-primary')); ?>
      <div class="card-deck">
          <div class="card">
            <div class="card-body">
              <ul class="list-group list-group-flush">
                <li class="list-group-item"><b>Name: </b> <?php echo $parent['first_name']. ' ' .  $parent['last_name']?></li>
                <li class="list-group-item"><b>Email: </b><?php echo $parent['user_email'] ?></li>
                <li class="list-group-item"><b>Mobile Number: </b><?php echo $parent['phone_number']?></li>
              </ul>
             </div>
          </div>
        </div>
      </div>
    </div>

      <div class="row">
        <div class="col">
          <h3><?= __('Students') ?></h3>
            <?php echo $this->Html->link('Add Student', array('controller' => 'classes', 'action' => 'all'), array('class' => 'btn btn-primary')); ?>
          <!--<div class="card-deck">-->
		  <div class="row">
            <?php foreach ($parent['student_contact'] as $student_contact):
              $student = $student_contact['student']; ?>
			  <div class="col-12 mb-3">
              <div class="card">
                <div class="card-body">
					<h4 class="card-title"><?php echo $student['first_name'] . ' ' . $student['last_name']; ?></h4>

					<p><strong>Student Details</strong></p>
					<?php echo $this->Html->link('Edit Student', array('controller' => 'students', 'action' => 'parentEdit', $student->id), array('class' => 'btn btn-primary')); ?>
					<?php echo $this->Html->link('Re-enrol', array('controller' => 'enrolments', 'action' => 'reenroll', $student->id), array('class' => 'btn btn-primary')); ?>
                </div>






				  <ul class="list-group list-group-flush">
                    <li class="list-group-item"><b>Grade: </b><?php echo $student['grade']['name'] ?></li>
                    <li class="list-group-item"><b>Notes: </b><?php echo $student['notes']?></li>
                  </ul>
				<div class="card-body">
                  <h4>Enrolments</h4>
				</div>
                  <table class="table table-responsive table-striped">
                    <thead>
                      <tr>
                        <th>Class Name</th>
                        <th>Start Date</th>
                        <th>Weeks Enrolled</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($student['enrolments'] as $enrolment): ?>
                        <tr>
                          <td><?php echo h($enrolment['class']['name']); ?></td>
                          <td><?php echo $enrolment->enrolment_date->i18nFormat('dd/MM/yy'); ?></td>
                          <td><?php echo $enrolment->no_of_weeks_enrolled ?></td>
                        </tr>
                      <?php endforeach; ?>
                    </tbody>
                  </table>
              </div>
			  </div>
            <?php endforeach; ?>
          </div>
			</div>
        </div>
      </div>

       
			   <div class="col-12 col-md-9">        
			   <div class="card">
                <div class="card-body">
			  <h3><?= __('Payment History') ?></h3>
			  <table class="table table-striped table-responsive bg-white">
					  <thead>
                      <tr>
                          <th scope="col">Student</th>
                          <th scope="col">Amount</th>
                          <th scope="col">Date</th>
                          <th scope="col">Status</th>
                      </tr>
                  </thead>
                  <tbody>
                    <?php
						foreach($payments as $payment):
							$student_names = [];
							foreach($payment['payment_history_enrolment'] as $phe):
								array_push($student_names, $phe['enrolment']['student']['full_name']);
							endforeach;
                      ?>
						  <tr>
							  <td><?php echo implode(', ', $student_names); ?></td>
							  <td><?= $this->Number->currency($payment->amount) ?></td>
							  <td><?= h($payment->datetime->i18nFormat('dd/MM/yyyy')) ?></td>
							  <td><?= h($payment->payment_status) ?></td>
						  </tr>
                      <?php endforeach; ?>


                  </tbody>
            </table>
        </div>
      </div>

  </div><!--/span-->
</div><!--/.container-->
</div>
