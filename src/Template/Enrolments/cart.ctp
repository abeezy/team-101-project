<div class="container">

	<div class="row justify-content-center">
		<div class="col-auto px-4 py-2 mx-3 progress-chevron">
			Parent
		</div>
		<div class="col-auto px-4 py-2 mx-3 progress-chevron">
			Student
		</div>
		<div class="col-auto px-4 py-2 mx-3 progress-chevron active">
			Cart
		</div>
		<div class="col-auto px-4 py-2 mx-3 progress-chevron">
			Payment
		</div>
		<div class="col-auto px-4 py-2 mx-3 progress-chevron">
			Summary
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-md-4">
			<h2 class="text-center">Your Cart</h2>
		</div>
	</div>
	<?php if(!empty($cart_data['items'])): ?>
		<?php foreach($cart_data['items'] as $index => $item): ?>
			<div class="row">
				<div class="col">
					<div class="card">
						<div class="card-header">
							<div class="row justify-content-between">
								<div class="col">
									<?php echo $item['Student']['full_name']; ?>
								</div>
								<div class="col-auto">
									<?php echo $this->Html->link('<i class="fas fa-times"></i>', array('action' => 'removeFromCart', $index), array('escape' => false)); ?>
								</div>
							</div>
						</div>
						<ul class="list-group list-group-flush">
							<li class="list-group-item"><strong>Class:</strong> <?php echo $item['Class']['name']; ?></li>
							<li class="list-group-item"><strong>Enrolment Period:</strong> From <?php echo $item['first_lesson']['lesson_date']->i18nFormat('dd/MM/yyyy'); ?> until <?php echo $item['last_lesson']['lesson_date']->i18nFormat('dd/MM/yyyy'); ?></li>
							<li class="list-group-item"><strong>Price:</strong> <?php echo $this->Number->currency($item['price']); ?> AUD <small>(inc. GST)</small></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<hr/>
				</div>
			</div>
		<?php endforeach; ?>
		<?php if(count($cart_data['items']) > 1): ?>
			<div class="row">
				<div class="col">
					<strong>Sibling Discount:</strong>  10% 
					<br>
					<strong>Discount:</strong>  <?php echo $this->Number->currency(($cart_data['total_price'] / 9)); ?>
				</div>
			</div>
		<?php endif; ?>
		<div class="row">
			<div class="col">
				<strong>Total Price:</strong> <?php echo $this->Number->currency($cart_data['total_price']); ?> AUD <small>(inc. GST)</small>
			</div>
		</div>
		<div class="row justify-content-end">
			<div class="col-auto">
				<?php echo $this->Html->link('Continue', array('action' => 'payment'), array('class' => 'btn btn-primary')); ?>
			</div>
		</div>
	<?php else: ?>
		<div class="row">
			<div class="col text-center">
				There are no items in your cart - click the "Schools" button above to start the enrolment process
			</div>
		</div>
	<?php endif; ?>
</div>
