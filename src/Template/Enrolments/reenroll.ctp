<div class="container">
	<?php echo $this->Form->create($student, array('class' => 'form-enrol', 'autocomplete' => 'disabled')); ?>
		<div class="row justify-content-center">
			<div class="col-md-4">
				
					
						<h2 class="text-center">Student</h2>
							
							<?php echo $this->Form->control('class_id', ['type' => 'select', 'options' => $classes, 'empty' => ['' => ' -- Please Select an Option -- '], 'disabled' => [''], 'class' => 'sClassID' ]); ?>
							
							<div class="form-group">
								<label>Weeks Enrolled</label>
								<select name="weeks_enrolled" class="form-control sWeeksEnrolled">
									<option disabled="disabled" selected="selected" value=""> -- Please Select A Class -- </option>
								</select>
							</div>
							
							
							
							<?php echo $this->Form->control('first_name',['type' => 'text', 'placeholder' => 'Sophia']); ?>
							<?php echo $this->Form->control('last_name', ['type' => 'text', 'placeholder' => 'Jackson']); ?>
							<?php echo $this->Form->control('grade_id', ['type' => 'select', 'options' => $grades]); ?>
							<?php echo $this->Form->control('notes', ['type' => 'textarea', 'placeholder' => 'Please note any medical conditions and concerns']); ?>
					
			

		
			
				<div class="form-group">
					<button class="btn btn-lg btn-primary btn-block" type="submit" name="submitButton"> Re-enrol </button>	
					
				</div>
				
		</div>
	</div>
		
	<?php echo $this->Form->end(); ?>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js" defer="defer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" />

<?php echo $this->Html->script('enrolment', array('defer' => 'defer')); ?>
