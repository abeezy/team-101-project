<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Student[]|\Cake\Collection\CollectionInterface $students
  */
?>
<div class="container">

	
	<div class="btn-group mb-3">
		<?php echo $this->Html->link('View All Students', ['action' => 'index'], ['class' => 'btn btn-primary']) ?>
		<?php echo $this->Html->link('View Current Students Only', ['action' => 'current'], ['class' => 'btn btn-outline-primary']) ?>
		<?php echo $this->Html->link('View Past Students Only', ['action' => 'past'], ['class' => 'btn btn-outline-primary']) ?>
	</div>

<div class="students index large-9 medium-8 columns content">
    <h3><?= __('All Students') ?></h3>

    <table class="table table-responsive table-striped bg-white">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('first_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('last_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('student_gender') ?></th>
				<th>Enrolment Date</th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($students as $student): ?>
            <tr>
                <td><?= h($student->first_name) ?></td>
                <td><?= h($student->last_name) ?></td>
				<td><?= h($student->student_gender) ?></td>
				<td><?= h($student->signup_date->i18nFormat('dd/MM/yyyy')) ?></td>
   
				<td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $student->id]) ?>
					<?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $student->id], ['confirm' => __('Are you sure you want to delete # {0}?', $student->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator text-center">
        <ul class="pagination justify-content-center">

            <?= $this->Paginator->prev('<i class="fa fa-chevron-left" aria-hidden="true"></i> ' . __('Previous'), ['escape' => false]) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Next') . ' <i class="fa fa-chevron-right" aria-hidden="true"></i>', ['escape' => false]) ?>

        </ul>
        <p class="text-muted"><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>

    </div>
</div>

</div>
