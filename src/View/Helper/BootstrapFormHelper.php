<?php
	namespace App\View\Helper;
	
	use Cake\View\Helper\FormHelper;

	class BootstrapFormHelper extends FormHelper{
		public function control($fieldName, array $options = []){
			if($this->request->is('post') && !$this->isFieldError($fieldName)){
				if(isset($options['class'])) {
					$options['class'] .= ' form-control is-valid';
				} else {
					$options['class'] = 'form-control is-valid';
				}
			}else{
				if(isset($options['class'])) {
					$options['class'] .= ' form-control';
				} else {
					$options['class'] = 'form-control';
				}
			}
			return parent::control($fieldName, $options);
		}
	}
?>