<?php
namespace App\Controller;
use Cake\Event\Event;
use App\Controller\AppController;

/**
 * Columns Controller
 *
 * @property \App\Model\Table\ColumnsTable $Columns
 *
 * @method \App\Model\Entity\Column[] paginate($object = null, array $settings = [])
 */
class ColumnsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Rows']
        ];
        $columns = $this->paginate($this->Columns);

        $this->set(compact('columns'));
        $this->set('_serialize', ['columns']);
    }

    /**
     * View method
     *
     * @param string|null $id Column id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $column = $this->Columns->get($id, [
            'contain' => ['Rows']
        ]);

        $this->set('column', $column);
        $this->set('_serialize', ['column']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $column = $this->Columns->newEntity();
        if ($this->request->is('post')) {
            $column = $this->Columns->patchEntity($column, $this->request->getData());
            if ($this->Columns->save($column)) {
                $this->Flash->success(__('The column has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The column could not be saved. Please, try again.'));
        }
        $rows = $this->Columns->Rows->find('list', ['limit' => 200]);
        $this->set(compact('column', 'rows'));
        $this->set('_serialize', ['column']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Column id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $column = $this->Columns->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $column = $this->Columns->patchEntity($column, $this->request->getData());
            if ($this->Columns->save($column)) {
                $this->Flash->success(__('The column has been saved.'));

                return $this->redirect(['controller' => 'Rows', 'action' => 'view', $column->row_id]);
            }
            $this->Flash->error(__('The column could not be saved. Please, try again.'));
        }
        $rows = $this->Columns->Rows->find('list', ['limit' => 200]);
        $this->set(compact('column', 'rows'));
        $this->set('_serialize', ['column']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Column id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $column = $this->Columns->get($id);
        if ($this->Columns->delete($column)) {
            $this->Flash->success(__('The column has been deleted.'));
        } else {
            $this->Flash->error(__('The column could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
	
	public function beforeFilter(Event $event){
		parent::beforeFilter($event);
	}
}
