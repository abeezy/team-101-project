<?php
/**
  * @var \App\View\AppView $this
  * @var \Cake\Datasource\EntityInterface $class
  */
?>

<div class="container">
  <button onclick="goBack()">Go Back</button>

  <script>
  function goBack() {
      window.history.back();
  }
  </script>

<div class="classes view large-9 medium-8 columns content">
    <h3><?= h($class->name) ?></h3>
    <table class="table table-responsive table-striped bg-white">
      <tr>
        <td class="actions text-right" colspan="2">
          <?= $this->Html->link(__('Edit'), ['action' => 'edit', $class->id], ['class' => 'btn btn-primary']) ?>
          <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $class->id], ['class' => 'btn btn-primary', 'confirm' => __('Are you sure you want to delete # {0}?', $class->id)]) ?>
        </td>
      </tr>
        <tr>
            <th scope="row"><?= __('Class Name') ?></th>
            <td><?= h($class->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('School') ?></th>
            <td><?= $class->has('school') ? $this->Html->link($class->school->school_name, ['controller' => 'Schools', 'action' => 'view', $class->school->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Class Location') ?></th>
            <td><?= h($class->class_address) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Teacher') ?></th>
            <td><?= $class->has('teacher') ? $this->Html->link($class->teacher->user->first_name . ' ' . $class->teacher->user->last_name, ['controller' => 'Teachers', 'action' => 'view', $class->teacher->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Class Time') ?></th>
            <td><?= $class->class_time->i18nFormat('HH:mm a'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Class Day') ?></th>
            <td><?= h($class->class_day) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Start Date') ?></th>
            <td><?= h($class->start_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('End Date') ?></th>
            <td><?= h($class->end_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Class Term') ?></th>
            <td><?= $this->Number->format($class->class_term) ?></td>
        </tr>
    </table><div id="accordion" role="tablist">
  <div class="card">
    <div class="card-header" role="tab" id="headingOne">
      <h5 class="mb-0">
        <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Lessons
        </a>
      </h5>
    </div>

    <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
        <div class="related">
            <h4><?= __('Lessons') ?></h4>
            <?php if (!empty($class->lessons)): ?>
         <table class="table table-responsive table-striped bg-white">
                <tr>
                    <th scope="col"><?= __('Teacher') ?></th>
                    <th scope="col"><?= __('Lesson Date') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($class->lessons as $lessons): ?>
                <tr>
                    <td><?= h($lessons->teacher->user->first_name . ' ' . $lessons->teacher->user->last_name) ?></td>
                    <td><?= h($lessons->lesson_date) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['controller' => 'Lessons', 'action' => 'view', $lessons->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['controller' => 'Lessons', 'action' => 'edit', $lessons->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['controller' => 'Lessons', 'action' => 'delete', $lessons->id], ['confirm' => __('Are you sure you want to delete # {0}?', $lessons->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="headingTwo">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Enrolments
        </a>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body">
        <div class="related">
            <h4><?= __('Enrolments') ?></h4>
            <?php if (!empty($class->enrolments)): ?>
            <table class="table table-responsive table-striped bg-white">
                <tr>
                    <th scope="col"><?= __('Student') ?></th>
                    <th scope="col"><?= __('Enrolment Date') ?></th>
                    <th scope="col"><?= __('No Of Weeks Enrolled') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($class->enrolments as $enrolments): ?>
                <tr>
                    <td><?= h($enrolments->student->first_name . ' ' . $enrolments->student->last_name) ?></td>
                    <td><?= h($enrolments->enrolment_date->i18nFormat('dd/M/yy')) ?></td>
                    <td><?= h($enrolments->no_of_weeks_enrolled) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['controller' => 'Enrolments', 'action' => 'view', $enrolments->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['controller' => 'Enrolments', 'action' => 'edit', $enrolments->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['controller' => 'Enrolments', 'action' => 'delete', $enrolments->id], ['confirm' => __('Are you sure you want to delete # {0}?', $enrolments->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
