<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\PaymentHistory $paymentHistory
  */
?>

<div class="container">
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        ?', $payments->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Payment History'), ['action' => 'index']) ?> </li>
        
        <li><?= $this->Html->link(__('List Enrolments'), ['controller' => 'Enrolments', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Enrolment'), ['controller' => 'Enrolments', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="paymentHistory view large-9 medium-8 columns content">
    <h3><?= h($payments->id) ?></h3>
    <table class="table table-responsive table-striped bg-white">
        <tr>
            <th scope="row"><?= __('Enrolment') ?></th>
            <td><?= $payments->has('enrolment') ? $this->Html->link($payments->enrolment->id, ['controller' => 'Enrolments', 'action' => 'view', $payments->enrolment->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Amount') ?></th>
            <td><?= h($payments->amount) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Payment Status') ?></th>
            <td><?= h($payments->payment_status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($payments->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Datetime') ?></th>
            <td><?= h($payments->datetime) ?></td>
        </tr>
    </table>
</div>
