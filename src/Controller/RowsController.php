<?php
namespace App\Controller;
use Cake\Event\Event;
use App\Controller\AppController;

/**
 * Rows Controller
 *
 * @property \App\Model\Table\RowsTable $Rows
 *
 * @method \App\Model\Entity\Row[] paginate($object = null, array $settings = [])
 */
class RowsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['RowTemplates', 'Webpages']
        ];
        $rows = $this->paginate($this->Rows);

        $this->set(compact('rows'));
        $this->set('_serialize', ['rows']);
    }

    /**
     * View method
     *
     * @param string|null $id Row id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $row = $this->Rows->get($id, [
            'contain' => ['RowTemplates', 'Webpages', 'Columns']
        ]);

        $this->set('row', $row);
        $this->set('_serialize', ['row']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($webpage_id = null)
    {
        $row = $this->Rows->newEntity();
		$row->webpage_id = $webpage_id;
        if ($this->request->is('post')) {
            $row = $this->Rows->patchEntity($row, $this->request->getData());
            if ($this->Rows->save($row)) {

				$row_template = $this->Rows->RowTemplates->get($row->row_template_id);

				$this->loadModel('Columns');


				for($i=0; $i<$row_template->column_count; $i++) {

					// Create new columns for each row template
					$column = $this->Columns->newEntity();
					$column->row_id = $row->id;
					$column->content = '';
					$column->column_order = $i;

					$this->Columns->save($column);

				}

                $this->Flash->success(__('The row has been saved.'));

                return $this->redirect(['action' => 'view', $row->id]);
            }
            $this->Flash->error(__('The row could not be saved. Please, try again.'));
        }
        $rowTemplates = $this->Rows->RowTemplates->find('list', ['limit' => 200]);
        $webpages = $this->Rows->Webpages->find('list', ['limit' => 200]);
        $this->set(compact('row', 'rowTemplates', 'webpages'));
        $this->set('_serialize', ['row']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Row id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $row = $this->Rows->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $row = $this->Rows->patchEntity($row, $this->request->getData());
            if ($this->Rows->save($row)) {
                $this->Flash->success(__('The row has been saved.'));

                return $this->redirect(['controller' => 'webpages', 'action' => 'view', $row->webpage_id]);
            }
            $this->Flash->error(__('The row could not be saved. Please, try again.'));
        }
        $rowTemplates = $this->Rows->RowTemplates->find('list', ['limit' => 200]);
        $webpages = $this->Rows->Webpages->find('list', ['limit' => 200]);
        $this->set(compact('row', 'rowTemplates', 'webpages'));
        $this->set('_serialize', ['row']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Row id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $row = $this->Rows->get($id);
        if ($this->Rows->delete($row)) {
            $this->Flash->success(__('The row has been deleted.'));
        } else {
            $this->Flash->error(__('The row could not be deleted. Please, try again.'));
        }

        return $this->redirect(['controller' => 'webpages', 'action' => 'view', $row->webpage_id]);
    }

	public function beforeFilter(Event $event){
		parent::beforeFilter($event);
	}
}
