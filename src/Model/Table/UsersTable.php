<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\Rule\IsUnique;

/**
 * Users Model
 *
 * @property |\Cake\ORM\Association\BelongsTo $Roles
 * @property \App\Model\Table\StudentContactTable|\Cake\ORM\Association\HasMany $StudentContact
 * @property \App\Model\Table\TeachersTable|\Cake\ORM\Association\HasMany $Teachers
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('full_name');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');


        $this->belongsTo('Schools', [
            'foreignKey' => 'school_id'
        ]);

        $this->belongsTo('Roles', [
            'foreignKey' => 'role_id',
            'joinType' => 'INNER'
		    ]);
        $this->hasMany('StudentContact', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Teachers', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('user_email')
            ->requirePresence('user_email', 'create')
            ->notEmpty('user_email')
			->add('user_email', 'validFormat', [
				'rule'=>'email',
				'message'=>"Must enter a valid email",
				'unique' => [
				'rule' => 'validateUnique',
				'provider' => 'table',
				'message' => 'Not unique']

			]);

        $validator
            ->scalar('password')
            ->requirePresence('password', 'create')
            ->notEmpty('password')
			->add('password', [
				'size' => ['rule' => ['lengthBetween', 6, 50], 'message' => 'Password must at least 6 characters'],
				//'hasSpecialCharacter' => ['rule' => 'containsNonAlphaNumeric', 'message' => 'Please use at least one special character']
			]);
			
		$validator
            ->requirePresence('cPassword', 'create', 'Password must be required!')
            ->notEmpty('cPassword', 'Confirm password must be required!')
            ->add(
                'cPassword',
                'custom',
                [
                    'rule' => function ($value, $context) {
                            if (isset($context['data']['password']) && $value == $context['data']['password']) {
                                return true;
                            }
                            return false;
                        },
                    'message' => 'Sorry, password and confirm password does not match'
                ]
            );


        $validator
            ->scalar('phone_number')
            ->requirePresence('phone_number', 'create')
            ->notEmpty('phone_number')
			->add('phone_number', [
				'size' => ['rule' => ['minlength', 10], 'message' => 'Phone Number must be 10 characters'],
				'type'=>['rule'=>'numeric','message'=>'Numbers only']
			]);

        $validator
            ->scalar('first_name')
            ->requirePresence('first_name', 'create')
            ->notEmpty('first_name');

        $validator
            ->scalar('last_name')
            ->requirePresence('last_name', 'create')
            ->notEmpty('last_name');

        return $validator;
    }

	/**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */

    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['role_id'], 'Roles'));
		$rules->add($rules->existsIn(['school_id'],'Schools'));
		 $rules->add($rules->isUnique(['user_email']));

        return $rules;
    }

    public function beforeFind ($event, $query, $options, $primary) {
      $order = $query->clause('order');
      if ($order === null || !count($order)) {
          $query->order( [$this->alias() . '.last_name' => 'ASC'] );
      }
    }
	
	public function findAuth(\Cake\ORM\Query $query, array $options) {
		$query
			//->select(['Users.*'])
			->where(['Users.active' => 1]);

		return $query;
	}
}
