<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Enrolment[]|\Cake\Collection\CollectionInterface $enrolments
  */
?>

<div class="container">
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Enrolment'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="enrolments index large-9 medium-8 columns content">
    <h3><?= __('Enrolments') ?></h3>
    <table class="table table-responsive table-striped bg-white">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('student_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('class_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('enrolment_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('no_of_weeks_enrolled') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($enrolments as $enrolment): ?>
            <tr>
                <td><?= $enrolment->has('student') ? $this->Html->link($enrolment->student->first_name . ' ' . $enrolment->student->last_name, ['controller' => 'Students', 'action' => 'view', $enrolment->student->id]) : '' ?></td>
                <td><?= $enrolment->has('class') ? $this->Html->link($enrolment->class->name, ['controller' => 'Classes', 'action' => 'view', $enrolment->class->id]) : '' ?></td>
				        <td><?= $enrolment->enrolment_date->i18nFormat('dd/MM/yyyy') ?></td>
                <td><?= $this->Number->format($enrolment->no_of_weeks_enrolled) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $enrolment->id]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator text-center">
        <ul class="pagination justify-content-center">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('<i class="fa fa-chevron-left" aria-hidden="true"></i> ' . __('Previous'), ['escape' => false]) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Next') . ' <i class="fa fa-chevron-right" aria-hidden="true"></i>', ['escape' => false]) ?>
        </ul>
        <p class="text-muted"><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
</div>


	