<?php
	$lastColourIndex = null;
	$lastRotation = null;

	function randomBg() {
		global $lastColourIndex;
		$colours = ['menu-dark-orange', 'menu-orange', 'menu-yellow', 'menu-green', 'menu-light-blue', 'menu-dark-blue'];

		$random = rand(0, count($colours)-1);

		while($random === $lastColourIndex) {
			$random = rand(0, count($colours)-1);
		}

		$lastColourIndex = $random;
		echo $colours[$random];
	}

	function randomRotation() {
		global $lastRotation;

		$random = rand(-7, 7);

		while($random === $lastRotation || ($random < 2 && $random > -2)) {
			$random = rand(-7, 7);
		}

		$lastRotation = $random;
		echo ' menu-rotate' . $random;
	}

	function derotate() {
		global $lastRotation;
		return ' menu-rotate' . ($lastRotation * -1);
	}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-110655822-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-110655822-1');
	</script>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<?php echo $this->Html->tag('base', null, ['href'=>$this->Url->build('/', true)]); ?>

		<title>Drama Time</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto|Open+Sans" rel="stylesheet">
		<script src="https://use.fontawesome.com/releases/v5.0.2/js/all.js" defer="defer"></script>

    <?php echo $this->Html->css("custom");?>
  </head>
  <body>
	<div class="headerBackground">
		<div class="container-fluid orangeBand">
			<div class="container">
				<div class="row justify-content-end">
					<div class="col-auto">
						<?php if ($loggedIn):
							if ($this->request->session()->check('cart')){
								echo $this->Html->link('<i class="fa fa-shopping-cart" aria-hidden="true"></i> Cart', array('controller' => 'enrolments', 'action' => 'cart'), array('class' => 'btn btn-link text-white', 'escape' => false));
							}
							echo $this->Html->link('<i class="fa fa-user" aria-hidden="true"></i> ' . $user_first_name . '\'s Account', array('controller' => 'users', 'action' => 'dashboard'), array('class' => 'btn btn-link text-white', 'escape' => false));
							echo $this->Html->link('<i class="fas fa-sign-out-alt" aria-hidden="true"></i> Log out', array('controller' => 'users', 'action' => 'logout'), array('class' => 'btn btn-link text-white', 'escape' => false));
						else:
							
						echo $this->Html->link('<i class="fas fa-sign-in-alt" aria-hidden="true"></i> Log In (No account? Please click Enrol Now)', array('controller' => 'users', 'action' => 'login'), array('class' => 'btn btn-link text-white', 'escape' => false));
						
						endif; ?>
					</div>
				</div>
			</div>
		</div>
  		<div class="container">
  			<div class="row align-items-end justify-content-between">
  				<div class="col-12 col-md-auto">
			      <?php echo $this->Html->image('Drama-Time-Crop.png', ['class' => 'headerImage mx-auto d-block mt-3', 'alt' => 'Drama', 'url' => ['controller' => 'pages', 'action' => 'home']]);?>
  				</div>
				<div class="col-12 col-md navbar-container">
					<nav class="navbar navbar-expand-lg navbar-light">
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon"></span>
						</button>

						<div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
							<ul class="navbar-nav">
								<li class="nav-item active <?php randomBg(); randomRotation(); ?>">
									<?php echo $this->Html->link('Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('class' => 'nav-link' . derotate())); ?>
								</li>
								<li class="nav-item <?php randomBg(); randomRotation(); ?>">
									<?php echo $this->Html->link('Schools', array('controller' => 'classes', 'action' => 'all'), array('class' => 'nav-link' . derotate())); ?>
								</li>
								<li class="nav-item <?php randomBg(); randomRotation(); ?>">
									<?php echo $this->Html->link('Programs', array('controller' => 'pages', 'action' => 'display', 'programs'), array('class' => 'nav-link' . derotate())); ?>
								</li>
								<li class="nav-item <?php randomBg(); randomRotation(); ?>">
									<?php echo $this->Html->link('Testimonials', array('controller' => 'pages', 'action' => 'display', 'testimonials'), array('class' => 'nav-link' . derotate())); ?>
								</li>
								<li class="nav-item <?php randomBg(); randomRotation(); ?>">
									<?php echo $this->Html->link('Our Team', array('controller' => 'pages', 'action' => 'display', 'our-team'), array('class' => 'nav-link' . derotate())); ?>
								</li>
								<li class="nav-item <?php randomBg(); randomRotation(); ?>">
									<?php echo $this->Html->link('Contact Us', array('controller' => 'contactMessages', 'action' => 'contactUs'), array('class' => 'nav-link' . derotate())); ?>
								</li>
							</ul>
						</div>
					</nav>
				</div>
  			</div>
  		</div>
  	</div>
  <!--
    <div class="headerBackground">
		<div class="container-fluid orangeBand">
			<div class="container">
				<div class="row justify-content-end">
					<div class="col-auto">
						<?php if ($loggedIn):
							if ($this->request->session()->check('cart')){
								echo $this->Html->link('<i class="fa fa-shopping-cart" aria-hidden="true"></i> Cart', array('controller' => 'enrolments', 'action' => 'cart'), array('class' => 'btn btn-link text-white', 'escape' => false));
							}
							echo $this->Html->link('<i class="fa fa-user" aria-hidden="true"></i> My Account', array('controller' => 'users', 'action' => 'dashboard'), array('class' => 'btn btn-link text-white', 'escape' => false));
							echo $this->Html->link('<i class="fa fa-sign-out" aria-hidden="true"></i> Log out', array('controller' => 'users', 'action' => 'logout'), array('class' => 'btn btn-link text-white', 'escape' => false));
						else:
							echo $this->Html->link('<i class="fa fa-sign-in" aria-hidden="true"></i> Log In', array('controller' => 'users', 'action' => 'login'), array('class' => 'btn btn-link text-white', 'escape' => false));
						endif; ?>
					</div>
				</div>
			</div>
		</div>
  		<div class="container">
  			<div class="row">
  				<div class="col">
			      <?php echo $this->Html->image('Drama-Time.png', ['class' => 'headerImage mx-auto d-block', 'alt' => 'Drama', 'url' => ['controller' => 'pages', 'action' => 'home']]);?>
  				</div>
  			</div>
  		</div>
  	</div>

	<div class="container">
		<nav class="navbar navbar-expand-lg navbar-light">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active <?php randomBg(); randomRotation(); ?>">
						<?php echo $this->Html->link('Home', array('controller' => 'pages', 'action' => 'display', 'home'), array('class' => 'nav-link' . derotate())); ?>
					</li>
					<li class="nav-item <?php randomBg(); randomRotation(); ?>">
						<?php echo $this->Html->link('Schools', array('controller' => 'classes', 'action' => 'all'), array('class' => 'nav-link' . derotate())); ?>
					</li>
					<li class="nav-item <?php randomBg(); randomRotation(); ?>">
						<?php echo $this->Html->link('Programs', array('controller' => 'pages', 'action' => 'display', 'programs'), array('class' => 'nav-link' . derotate())); ?>
					</li>
					<li class="nav-item <?php randomBg(); randomRotation(); ?>">
						<?php echo $this->Html->link('Testimonials', array('controller' => 'pages', 'action' => 'display', 'testimonials'), array('class' => 'nav-link' . derotate())); ?>
					</li>
					<li class="nav-item <?php randomBg(); randomRotation(); ?>">
						<?php echo $this->Html->link('Our Team', array('controller' => 'pages', 'action' => 'display', 'our-team'), array('class' => 'nav-link' . derotate())); ?>
					</li>
					<li class="nav-item <?php randomBg(); randomRotation(); ?>">
						<?php echo $this->Html->link('Contact Us', array('controller' => 'contactMessages', 'action' => 'contactUs'), array('class' => 'nav-link' . derotate())); ?>
					</li>
					<li class="nav-item <?php randomBg(); ?>">
						<?php echo $this->Html->link('Terms & Conditions', array('controller' => 'pages', 'action' => 'display', 'conditions'), array('class' => 'nav-link')); ?>
					</li>
				</ul>
			</div>
		</nav>
     </div>
	-->

    <?php echo $this->Flash->render(); ?>

    <?php echo $this->fetch('content'); ?>


    <footer class="footer">
      <div class="container">
        <div class="row justify-content-between">
          <div class="col-auto">
			<h3 class="footer"> Visit </h3>
            <a href="https://www.facebook.com/Drama-Time-271445892993065/">Find us on Facebook!</a>

            <ul class="list-unstyled">
              <li class="nav-footer">
                <?php echo $this->Html->link('Enrolments', array('controller' => 'classes', 'action' => 'all')); ?>
              </li>
              <li class="nav-footer">
                <?php echo $this->Html->link('Programs', array('controller' => 'pages', 'action' => 'display', 'programs')); ?>
              </li>
              <li class="nav-footer">
                <?php echo $this->Html->link('Locations', array('controller' => 'pages', 'action' => 'display', 'locations')); ?>
              </li>
              <li class="nav-footer">
                <?php echo $this->Html->link('Our Team', array('controller' => 'pages', 'action' => 'display', 'our-team')); ?>
              </li>
              <li class="nav-footer">
                <?php echo $this->Html->link('Contact Us', array('controller' => 'contactMessages', 'action' => 'contactUs')); ?>
              </li>
              <li class="nav-footer">
                <?php echo $this->Html->link('Testimonials', array('controller' => 'pages', 'action' => 'display', 'testimonials')); ?>
              </li>
			  <li class="nav-footer">
                <?php echo $this->Html->link('Terms & Conditions', array('controller' => 'pages', 'action' => 'display', 'conditions')); ?>
              </li>
            </ul>
          </div>
          <div id="footer" class="col-auto">
            <p>&nbsp;</p>
			<h3 class="contactUs"> Contact Us </h3>
            PO Box 2442<br>
            Caulfield Junction<br>
            Victoria 3162<br>
            Australia<br>
            <a href="mailto:contact@dramatime.com.au">contact@dramatime.com.au</a><br>
            03 9525 9977
          </div>
        </div>
		<div class="push-base clear"></div>

          <div class="col text-center">
            <span class="text-unmuted">&copy; Dramatime 2008-<?php echo date("Y");?></span>
          </div>

      </div>
    </footer>

    <div class="modal fade" id="messageModal">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="messageModalTitle"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" id="messageModalContent">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
    <?php echo $this->Html->script("custom", array('defer' => 'defer'));?>
  </body>
</html>
