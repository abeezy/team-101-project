<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="container">

<div class="students form large-9 medium-8 columns content">
<h2>Edit Student</h2>

    <?= $this->Form->create($student) ?>
    <fieldset>

        <?php

            echo $this->Form->control('first_name');
            echo $this->Form->control('last_name');
            echo $this->Form->control('student_gender', array('type'=>'select', 'options' => array('Male' => 'Male','Female'=>'Female'), 'default' => '', 'empty'=> array('' => '-- Please Select --'), 'disabled' => ['']));
           	echo $this->Form->control('grade_id', ['type' => 'select', 'options' => $grades, 'class' => 'sGradeID']);
		   echo $this->Form->control('notes');

        ?>
    </fieldset>
    <?php echo $this->Form->button(__('Save'), array('class' => 'btn btn-light')); ?>
		<button type="button" class = 'btn btn-light' onclick="goBack()">Go Back</button>

<script>
function goBack() {
    window.history.back();
}
</script>
    <?= $this->Form->end() ?>
</div>
</div>
