<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PaymentHistoryTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PaymentHistoryTable Test Case
 */
class PaymentHistoryTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PaymentHistoryTable
     */
    public $PaymentHistory;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.payment_history',
        'app.enrolments',
        'app.students',
        'app.classes',
        'app.schools',
        'app.teachers',
        'app.lessons',
        'app.attendences'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PaymentHistory') ? [] : ['className' => PaymentHistoryTable::class];
        $this->PaymentHistory = TableRegistry::get('PaymentHistory', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PaymentHistory);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
