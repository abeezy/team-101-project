<?php

	// Controls the default form templates - in this case, we're going to default the forms to Bootstrap's standard

	return [
		'timeFormGroup' => '<div class="form-group">{{label}}<div class="input-group">{{input}}</div></div>',
		'inputContainer' => '<div class="form-group">{{content}}</div>',
		'input' => '<input type="{{type}}" name="{{name}}" {{attrs}} />',
		'inputContainerError' => '<div class="form-group">{{content}}<div class="invalid-feedback">{{error}}</div></div>',
    'select' => '<select name="{{name}}" {{attrs}}>{{content}}</select>',
    'textarea' => '<textarea name="{{name}}" {{attrs}}>{{value}}</textarea>',
		'radioWrapper' => '<div class="form-check form-check-inline">{{label}}</div>',
		'radio' => '<input type="radio" class="form-check-input" name="{{name}}" value="{{value}}"{{attrs}}>',
		'nestingLabel' => '{{hidden}}<label class="form-check-label"{{attrs}}>{{input}} {{text}}  </label>',
		

  ];

?>
