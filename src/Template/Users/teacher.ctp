<div class="container">
  <div class="row">
    <div class="col">
      <div class="jumbotron">
          <h1>Hello <?php echo $teacher['first_name']; ?>,</h1>
        <p>View your details below.</p>
      </div>

        <div class="row">
            <div class="col">
                <?php $teacher; ?>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col">
                <h3><?= __('My Details') ?></h3>
                <!--add teacher form redirect button -->
                <p><?php echo $this->Html->link('Edit Details', array('controller' => 'users', 'action' => 'user_edit'), array('class' => 'btn btn-primary')); ?></p>
                <div class="card-deck">
                    <div class="card">
                        <div class="card-body">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item"><b>Name: </b> <?php echo $teacher['first_name']. ' ' .  $teacher['last_name']?></li>
                                <li class="list-group-item"><b>Email: </b><?php echo $teacher['user_email'] ?></li>
                                <li class="list-group-item"><b>Mobile Number: </b><?php echo $teacher['phone_number']?></li>
								 <li class="list-group-item"><b>Notes: </b><?php echo $teacher['notes']?></li></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<div class="row">
			<div class="col">
				<h3>Classes</h3>
				<table class="table table-responsive table-striped bg-white">
					<thead>
						<tr>
							<th scope="col"><?= $this->Paginator->sort('school_id') ?></th>
							<th scope="col"><?= $this->Paginator->sort('class_name') ?></th>
							<th scope="col"><?= $this->Paginator->sort('teacher') ?></th>
							<th scope="col"><?= $this->Paginator->sort('class_time') ?></th>
							<th scope="col"><?= $this->Paginator->sort('class_day') ?></th>
							<th scope="col"><?= $this->Paginator->sort('class_term') ?></th>
							<th scope="col" class="actions"><?= __('Actions') ?></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($classes as $class): ?>
						<tr>
							<td><?php echo $class->school->school_name; ?></td>
							<td><?= h($class->name) ?></td>
							<td><?php echo $class->teacher->user->full_name; ?></td>
							
						  <td><?= $class->class_time->i18nFormat('dd/MM/yyyy') ?></td>
							<td><?= h($class->class_day) ?></td>
							<td><?= $this->Number->format($class->class_term) ?></td>
							<td class="actions">
								<?= $this->Html->link(__('View'), ['controller' => 'classes', 'action' => 'teacherView', $class->id]) ?>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
    </div><!--/span-->
    </div>
</div>
