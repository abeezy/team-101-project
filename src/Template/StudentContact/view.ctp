<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\StudentContact $studentContact
  */
?>

<div class="container">
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Student Contact'), ['action' => 'edit', $studentContact->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Student Contact'), ['action' => 'delete', $studentContact->id], ['confirm' => __('Are you sure you want to delete # {0}?', $studentContact->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Student Contact'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Student Contact'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Students'), ['controller' => 'Students', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Student'), ['controller' => 'Students', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Contact Type'), ['controller' => 'ContactType', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Contact Type'), ['controller' => 'ContactType', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="studentContact view large-9 medium-8 columns content">
    <h3><?= h($studentContact->id) ?></h3>
    <table class="table table-responsive table-striped bg-white">
        <tr>
            <th scope="row"><?= __('Student') ?></th>
            <td><?= $studentContact->has('student') ? $this->Html->link($studentContact->student->id, ['controller' => 'Students', 'action' => 'view', $studentContact->student->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $studentContact->has('user') ? $this->Html->link($studentContact->user->id, ['controller' => 'Users', 'action' => 'view', $studentContact->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($studentContact->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Contact Type') ?></h4>
        <?php if (!empty($studentContact->contact_type)): ?>
        <table class="table table-responsive table-striped bg-white">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Student Contact Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($studentContact->contact_type as $contactType): ?>
            <tr>
                <td><?= h($contactType->id) ?></td>
                <td><?= h($contactType->student_contact_id) ?></td>
                <td><?= h($contactType->name) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ContactType', 'action' => 'view', $contactType->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ContactType', 'action' => 'edit', $contactType->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ContactType', 'action' => 'delete', $contactType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $contactType->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
