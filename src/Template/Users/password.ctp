<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-4">
<?php $this->assign('title', 'Request Password Reset'); ?>
<div class="users content">
	<h3><?php echo __('Forgot Password'); ?></h3>
	<?php
    	echo $this->Form->create();
        echo $this->Form->input('user_email', ['autofocus' => true, 'label' => 'Email address', 'required' => true]);
	?>
	<button class="btn btn-primary btn-block" type="submit">Request Reset Password</button>
	<?php
    	echo $this->Form->end();
	?>
	
</div>
</div>
</div>
</div>