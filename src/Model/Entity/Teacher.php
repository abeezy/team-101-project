<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;


/**
 * Teacher Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $tfn
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Class[] $classes
 * @property \App\Model\Entity\Lesson[] $lessons
 */
class Teacher extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    protected $_virtual = ['full_name'];

    protected function _getFullName() {
      $Users = TableRegistry::get('Users');
      $user = $Users->get($this->_properties['user_id']);
      return $user['first_name'] . ' ' . $user['last_name'];
    }
}
