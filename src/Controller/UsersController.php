<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Entity\Role;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\Mailer\Email;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    public function parents()
    {
          $users = $this->Users->find()->matching('Roles', function ($q) {
			return $q->where(array(
				'Roles.name' => 'Parent'
		    ));
		});
		$this->paginate($users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    public function teachers()
    {
          $users = $this->Users->find()->matching('Roles', function ($q) {
			return $q->where(array(
				'Roles.name' => 'Teacher'
		    ));
		});
		
		$this->paginate($users);
  
        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }
	
	public function activeTeachers()
    {
        $users = $this->Users->find()->where(array(
			'Users.active' => true
		))->matching('Roles', function ($q) {
			return $q->where(array(
				'Roles.name' => 'Teacher'
		    ));
		});
		$this->paginate($users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }
	
	public function inactiveTeachers()
    {
        $users = $this->Users->find()->where(array(
			'Users.active' => false
		))->matching('Roles', function ($q) {
			return $q->where(array(
				'Roles.name' => 'Teacher'
		    ));
		});
		$this->paginate($users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }
	
	
	public function activeParents()
    {
        $users = $this->Users->find()->where(array(
			'Users.active' => true
		))->matching('Roles', function ($q) {
			return $q->where(array(
				'Roles.name' => 'Parent'
		    ));
		});
		$this->paginate($users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }
	
	public function inactiveParents()
    {
        $users = $this->Users->find()->where(array(
			'Users.active' => false
		))->matching('Roles', function ($q) {
			return $q->where(array(
				'Roles.name' => 'Parent'
		    ));
		});
		$this->paginate($users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }
	

    public function admins()
    {
           $users = $this->Users->find()->matching('Roles', function ($q) {
			return $q->where(array(
				'Roles.name' => 'Admin'
		    ));
		});
		$this->paginate($users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }


    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['StudentContact' => ['Students'], 'Teachers', 'Roles'] 
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

				if($user->role_id==1){
					$this->loadModel('Teachers');
					$teacher=$this->Teachers->newEntity(array(
					'user_id'=>$user->id));
					$this ->Teachers->save($teacher);
				}
                switch ($user->role_id) {
					case '1' : 
						return $this->redirect(['action' => 'teachers']);
						break; 
					case '2' :
						return $this->redirect(['action' => 'parents']);
						break;
					case '3' : 
						return $this->redirect(['action' => 'admins']);
						break; 
				}
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
		$roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $this->set(compact('user', 'roles'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Roles']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('User updated successfully.'));

                switch ($user->role->name) {
					case 'teacher' : 
						return $this->redirect(['action' => 'teachers']);
						break; 
					case 'parent' :
						return $this->redirect(['action' => 'parents']);
						break;
					case 'admin' : 
						return $this->redirect(['action' => 'admins']);
						break; 
				}
				
            }
            $this->Flash->error(__('The user could not be updated. Please, try again.'));
        }
		$roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $this->set(compact('user', 'roles'));
        $this->set('_serialize', ['user']);
    }
	
	public function activate($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Roles']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, array('active' => true));
            if ($this->Users->save($user)) {
                $this->Flash->success(__('User updated successfully.'));

                return $this->redirect(array('action' => 'view', $user->id));
				
            }
            $this->Flash->error(__('The user could not be updated. Please, try again.'));
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }
	
	public function deactivate($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Roles']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, array('active' => false));
            if ($this->Users->save($user)) {
                $this->Flash->success(__('User updated successfully.'));

				return $this->redirect(array('action' => 'view', $user->id));
				
            }
            $this->Flash->error(__('The user could not be updated. Please, try again.'));
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }
	
	
    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function userEdit()
    {
        $user = $this->Users->get($this->Auth->user('id'), [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Details successfully updated.'));

                return $this->redirect(['action' => 'dashboard']);
            }
            $this->Flash->error(__('Details could not be succesfully updated. Please try again.'));
        }
		$roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $this->set(compact('user', 'roles'));
        $this->set('_serialize', ['user']);
    }
    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

		switch ($user->role_id) {
			case '1' : 
				return $this->redirect(['action' => 'teachers']);
				break; 
			case '2' :
				return $this->redirect(['action' => 'parents']);
				break;
			case '3' : 
				return $this->redirect(['action' => 'admins']);
				break; 
		}
		
        return $this->redirect(['action' => 'index']);
    }

	//the login function allows users to access their specific dashboard depending on what type of user they are (or their role)//
	//sets the new user as a new User entity if login is correct

	public function login(){
		//check if a user is logged in
		if ($this->Auth->user('id')){
			$this->Flash->error('You are already logged in');
			 return $this->redirect(['controller' => 'pages', 'action' => 'home']);
		}
		if ($this->request->is ('post')){
			$user=$this->Auth->identify();
			if ($user){
				$this->Auth->setUser($user);
				
				if ($this->request->session()->check("Auth.redirect")) {
					return $this->redirect($this->request->session()->consume("Auth.redirect"));
				}
				
				return $this->redirect($this->Auth->redirectUrl());
			}

			$this->Flash->error('Incorrect user email or password. Type the correct user email and password, and try again.');
			$user = $this->Users->newEntity();
			$this->set(compact('user'));

		}
		else {

			$user = $this->Users->newEntity();
			$this->set(compact('user'));
		}
	}

	//log out function only displays once a user is logged in and redirects them back to the home page//
	public function logout(){
		$this->Flash->success ('You have successfully logged out.');
		return $this->redirect($this->Auth->logout());
	}
	
	public function signupLogin() {
		$this->request->session()->write("Auth.redirect", array('controller' => 'students', 'action' => 'student_enrolment'));
		return $this->redirect(array('action' => 'login'));
	}

	//parent signup function is very similar to the add new function

	public function parentsignup($class_id = null) {

		$this->request->session()->write("class_id", $class_id);

		if($this->Auth->user('id')){
			return $this->redirect(['controller' => 'students', 'action' => 'student_enrolment']);
		} else {


			$user = $this->Users->newEntity();

			if ($this->request->is('post')) {
				$user = $this->Users->newEntity($this->request->getData());

				$this->loadModel('Roles');
				$role=$this->Roles->findByName('parent')->first();
				$user->role_id=$role->id;


				if ($this->Users->save($user)) {
					$this->Auth->setUser($user->toArray());


					$login_link = Router::url(['controller' => 'Users', 'action' => 'login'], true);

					$to = array($user->user_email);
					$subject = 'Welcome To Drama Time';
					$template = 'welcome-to-drama-time';
					$from = 'Drama Time';
					$merge_variables = array(
						'name'    => $user->first_name,
						'email' => $user->user_email,
						'login_link' => $login_link
					);
					$this->send_email($to, $from, $subject, $template, $merge_variables);


					$critical = false; //if this process fails, what should happen, true = redirect to previous page, false = still continues the register process
					$list_id = '75f7313bbd';
					$email_address = $user->user_email;
					$variables = array (
						'FNAME' => $user->first_name,
						'LNAME' => $user->last_name,
						'email_address' => $user->user_email,
						'status' => 'subscribed',
						'status_if_new' => 'subscribed'
					);
					$this->mailchimp_subscribe($critical, $list_id, $email_address, $variables);


					return $this->redirect(['controller' => 'students', 'action' => 'student_enrolment']);

				}

				// echo "<pre>";
				// print_r($user);
				// echo "</pre>";

				// exit;

				$this->Flash->error(__('Your details could not be saved. Please, try again.'));
			}
			$this->set(compact('user', 'enrolment', 'classes'));
			$this->set('_serialize', ['user']);
		}
	}

  public function parent() {
    $parent = $this->Users->get($this->Auth->user('id'), array(
      'contain' => array(
        'StudentContact' => array(
          'Users' => array(
            'Schools'
          ),
          'Students' => array(
            'Enrolments' => array(
              'Classes',
			  'PaymentHistoryEnrolment' => array(
				'PaymentHistory'
				)
            ),
			'Grades'
          )
        )
      )
    ));

	$this->loadModel('PaymentHistory');

	$payments = $this->PaymentHistory->find('all')->innerJoinWith('PaymentHistoryEnrolment.Enrolments.Students.StudentContact.Users', function ($q) {
		return $q->where(['Users.id' => $this->Auth->user('id')]);
	})->contain(array(
		'PaymentHistoryEnrolment' => array(
			'Enrolments' => array(
				'Students'
			)
		)
	))->distinct()->all();

    $this->set(compact('parent', 'payments'));
  }

  public function admin() {
	  $this->loadModel('Roles');
	  $role_id=$this->Roles->findByName('admin')->first()['id'];

	  if ($this->Auth->user('role_id') !== $role_id) {
  		$role=$this->Roles->get($this->Auth->user('role_id'));

      if ($role->name == 'teacher'){
        return $this->redirect(['controller' => 'users', 'action' => 'teacher']);
      }
      if ($role->name == 'parent'){
        return $this->redirect(['controller' => 'users', 'action' => 'parent']);
  		}


		return $this->redirect(['controller' => 'pages', 'action' => 'display', 'home']);

	}
	$admin = $this->Users->get($this->Auth->user('id'));

	$this->LoadModel('contactMessages');
	$messages=$this->contactMessages->find()->all();

	$this->set(compact('admin', 'messages'));



  }

  public function teacher() {
	$this->loadModel('Roles');
	  $role_id=$this->Roles->findByName('teacher')->first()['id'];

	  if ($this->Auth->user('role_id') !== $role_id) {
		$role=$this->Roles->get($this->Auth->user('role_id'));

        if ($role->name == 'admin'){
          return $this->redirect(['controller' => 'users', 'action' => 'admin']);
        }
        if ($role->name == 'parent'){
          return $this->redirect(['controller' => 'users', 'action' => 'parent']);

		}
		return $this->redirect(['controller' => 'pages', 'action' => 'display', 'home']);
		}

		$teacher=$this->Users->get($this->Auth->user('id'));
		$this->set(compact('teacher'));
		
		$this->loadModel('Classes');
		
		$classes = $this->Classes->find('all')->matching('Teachers.Users', function ($q) {
			return $q->where(array('Users.id' => $this->Auth->user('id')));
		})->contain(array(
			'Schools',
			'Teachers' => [
				'Users'
			]
		))->all();
		
		/*$this->paginate = [
			'conditions' => [
				'Users.id' => $this->Auth->user('id')
			],
            'contain' => [
				'Schools',
				'Teachers' => [
					'Users'
				]
			]
        ];
        $classes = $this->paginate($this->Classes);*/

        $this->set(compact('classes'));
 }


  public function dashboard() {
    $role_name = $this->get_current_user_role_name();

    if ($role_name == 'admin'){
      return $this->redirect(['controller' => 'users', 'action' => 'admin']);
    }
    if ($role_name == 'teacher'){
      return $this->redirect(['controller' => 'users', 'action' => 'teacher']);
    }
    if ($role_name == 'parent'){
      return $this->redirect(['controller' => 'users', 'action' => 'parent']);
    }
    return $this->redirect(['controller' => 'pages', 'action' => 'display', 'home']);

  }

	 public function password()
    {
        if ($this->request->is('post')) {
            $query = $this->Users->findByUserEmail($this->request->data['user_email']);
            $user = $query->first();
            if (is_null($user)) {
                $this->Flash->error('Email address does not exist. Please try again');
            } else {
                $passkey = uniqid();
                $url = Router::Url(['controller' => 'users', 'action' => 'reset'], true) . '/' . $passkey;
                $timeout = time() + DAY;
                 if ($this->Users->updateAll(['reset_token' => $passkey, /*'timeout' => $timeout*/], ['id' => $user->id])){

					$to = array($user->user_email);
					$subject = 'Drama Time - Password Reset';
					$template = 'reset-password';
					$merge_variables = array(
						'name'    => $user->first_name,
						'email' => $user->user_email,
						'reset_link' => $url
					);
					$this->send_email($to, 'Drama Time', $subject, $template, $merge_variables);

					$this->Flash->set('Please check your email for further instructions. Please ensure to check your junk mail or spam mail folder.');

					 $this->redirect(['controller' => 'users', 'action' => 'login']);
                } else {
                    $this->Flash->error('Error saving reset passkey/timeout');
                }
            }
        }
    }

    public function reset($reset_token = null) {
        if ($reset_token) {
            $query = $this->Users->find('all', ['conditions' => ['reset_token' => $reset_token, /*'timeout >' => time()*/]]);
            $user = $query->first();
            if ($user) {
				if ($this->request->is('put')) {
                    // Clear reset_token and timeout
                    $this->request->data['reset_token'] = null;
                    //$this->request->data['timeout'] = null;
                    $user = $this->Users->patchEntity($user, $this->request->data);
                    if ($this->Users->save($user)) {
                        $this->Flash->set(__('Your password has been updated.'));
                        return $this->redirect(array('controller' => 'users', 'action' => 'login'));
                    } else {
                        $this->Flash->error(__('The password could not be updated. Please, try again.'));
                    }
                }
            } else {
                $this->Flash->error('Invalid or expired reset_token. Please check your email or try again');
                $this->redirect(['action' => 'password']);
            }
            unset($user->password);
            $this->set(compact('user'));
        } else {
            $this->redirect('/');
        }
    }


	public function teachersignup() {
	$user = $this->Users->newEntity();

        if ($this->request->is('post')) {
            $user = $this->Users->newEntity($this->request->getData());

			$this->loadModel('Roles');
			$role=$this->Roles->findByName('teacher')->first();
			$user->role_id=$role->id;



		    if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
				$this->Auth->setUser($user->toArray());

                return $this->redirect(['controller' => 'teachers', 'action' => 'addteachers']);
            }



            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
	}


	public function search() {
		$this->viewBuilder()->setLayout('ajax');
		$users = $this->Users->find()
			->where(['LOWER(first_name) LIKE' => '%' . strtolower($this->request->getQuery('term')) . '%'])
			->orWhere(['LOWER(last_name) LIKE' => '%' . strtolower($this->request->getQuery('term')) . '%'])
			->orWhere(['LOWER(user_email) LIKE' => '%' . strtolower($this->request->getQuery('term')) . '%'])
			->orWhere(['LOWER(phone_number) LIKE' => '%' . strtolower($this->request->getQuery('term')) . '%'])
			->contain(['Roles'])
			->order(['FIELD(Roles.name,"parent","teacher","admin")']) // Sort by privilege level
			->all();

		$this->set(compact('users'));
		$this->set('_serialize', false);
	}




	public function beforeFilter(Event $event){
		$this->Auth->allow(['parentsignup', 'logout', 'login', 'signupLogin', 'password', 'reset']);

		if(!$this->action_is_public($event->subject) && $this->get_current_user_role_name() === 'parent') {
			$this->Auth->allow(['parent', 'userEdit', 'dashboard']);
		}
		if(!$this->action_is_public($event->subject) && $this->get_current_user_role_name() === 'teacher') {
			$this->Auth->allow(['teacher', 'userEdit', 'dashboard','teacher_view','class-index']);
		}
		parent::beforeFilter($event);

	}


}
