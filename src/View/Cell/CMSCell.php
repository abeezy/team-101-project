<?php
		namespace App\View\Cell;
		use Cake\View\Cell;
		
		class CMSCell extends Cell {
			public function display($page) {
				$this->loadModel('Webpages');
				$webpage = $this->Webpages->find('all', array(
					'conditions'=>array(
						'url'=>$page
					), 
					'contain'=>array(
						'Rows'=>array(
							'RowTemplates',
							'Columns'
						)
					)
				))->first();
				
				// echo "<pre>";
				// print_r($webpage);
				// echo "</pre>";
				// exit;
				$rendered = '';
				
				foreach ($webpage->rows as $row) {
					$template = $row['row_template']['content'];
					
					for($i=0; $i<$row['row_template']['column_count']; $i++) {
						$columnContent = $row['columns'][$i]['content'];
						$columnName = '{{column_' . ($i+1) . '}}';
						$template = str_replace($columnName, $columnContent, $template);
					} 
					$rendered .= $template;					
				}
				
				$this->set('CMS_content', $rendered);
				
			}
		}
	
?>