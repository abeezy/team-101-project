<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\TableRegistry;

/**
 * User Entity
 *
 * @property int $id
 * @property int $role_id
 * @property string $user_email
 * @property string $password
 * @property \Cake\I18n\FrozenDate $date_created
 * @property string $phone_number
 * @property string $first_name
 * @property string $last_name
 *
 * @property \App\Model\Entity\StudentContact[] $student_contact
 * @property \App\Model\Entity\Teacher[] $teachers
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];

    protected function _setPassword($password) {
		if (strlen($password)>0) {
			return (new DefaultPasswordHasher)->hash($password);
		}
	}
	
	protected $_virtual = ['full_name'];

    protected function _getFullName() {
      $Users = TableRegistry::get('Users');
      $user = $Users->get($this->_properties['id']);
      return $user['first_name'] . ' ' . $user['last_name'];
    }
}
