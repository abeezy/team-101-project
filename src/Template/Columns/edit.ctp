<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="container">
	<h2>Edit Column</h2>

			<?= $this->Form->create($column) ?>
			<fieldset>
				<?php
					echo $this->Form->control('row_id', ['type' => 'hidden', 'options' => $rows]);
					echo $this->Form->control('content', array('class' => 'content-editor'));
					echo $this->Form->control('column_order');
				?>
			</fieldset>
				<?php echo $this->Form->button(__('Save'), array('class' => 'btn btn-light')); ?>
		<button type="button" class = 'btn btn-light' onclick="goBack()">Go Back</button>

<script>
function goBack() {
    window.history.back();
}
</script>
			<?= $this->Form->end() ?>
		</div>
	
<?php echo $this->Html->script('ckeditor/ckeditor'); ?>