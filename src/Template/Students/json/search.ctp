<?php
	$output_array = [];
	foreach($students as $student){
		array_push($output_array, array(
			'label' => $student->full_name,
			'value' => $student->id,
			'category' => 'Students',
			'url' => $this->Url->build(['controller' => 'students', 'action' => 'view', $student->id], true)
		));
	}
	echo json_encode($output_array);
?>