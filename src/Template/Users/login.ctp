<div class="container">
	<?php echo $this->Form->create($user, array('class' => 'form-signin', 'autocomplete' => 'disabled')); ?>
		<div class="row justify-content-center">
			<div class="col-md-4">
				<h2 class="text-center">Please log in</h2>
				<?php echo $this->Form->control('user_email'); ?>
				<?php echo $this->Form->control('password'); ?>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-md-4">
				<div class="row">
					<div class="col">	
						<div class="form-group">
						 <button class="btn btn-primary btn-block" type="submit">Sign in</button>

						</div>
					</div>
					<div class="col">	
						<div class="form-group">
						<?php echo $this->Html->link('Forgot your password', array('action' => 'password'), array('class' => 'btn btn-light btn-block')); ?>

						</div>
					</div>
				</div>
			</div>
		</div>
    <?php echo $this->Form->end(); ?>
</div> <!-- /container -->
