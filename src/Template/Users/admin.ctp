<div class="row">
	<div class="col">
        <div class="jumbotron">
            <h1>Hello <?php echo $admin['first_name']; ?>,</h1>
            <p>View details below.</p>
        </div>
	</div>
</div>
<div class="row">
	<div class="col">
		<h2><?= __('Messages') ?></h2>

		<?php foreach ($messages as $message): ?>
			<h4 class="card-title"><?php echo $message['first_name'] . ' ' . $message['last_name']; ?></h4>
			<table class="table table-responsive table-striped bg-white">
				<thead>
					<tr>
						<th>Message Details</th>
						<th>Email</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><?php echo $message['message_details']; ?></td>
						<td><?php echo $message['user_email']; ?></td>
					</tr>
				</tbody>
			</table>
		<?php endforeach; ?>
	</div>
</div>