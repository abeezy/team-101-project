<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Class Entity
 *
 * @property int $id
 * @property int $school_id
 * @property int $teacher_id
 * @property \Cake\I18n\FrozenDate $start_date
 * @property \Cake\I18n\FrozenDate $end_date
 * @property \Cake\I18n\FrozenTime $class_time
 * @property string $class_day
 * @property string $class_address
 * @property int $class_term
 *
 * @property \App\Model\Entity\School $school
 * @property \App\Model\Entity\Teacher $teacher
 * @property \App\Model\Entity\Enrolment[] $enrolments
 * @property \App\Model\Entity\Lesson[] $lessons
 */
class Classes extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
	
	// Boolean to store if this class is now completely in the past, so it should not be available for enrolment
	protected $_virtual = ['finished'];

    protected function _getFinished() {
      //$Classes = TableRegistry::get('Classes');
      //$class = $Classes->get($this->_properties['id']);
	  
	  echo "<pre>";
	  print_r($this->_properties);
	  echo "</pre>";
	  exit;
	  
      return ($this->_properties['end_date'] < time());
    }
}
