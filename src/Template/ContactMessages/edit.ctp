<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="container">

<div class="row justify-content-center">
		<div class="col-md-4">
    <?= $this->Form->create($contactMessage) ?>
    <fieldset>
        <legend><?= __('Edit Contact Message') ?></legend>
        <?php
            echo $this->Form->control('first_name');
            echo $this->Form->control('last_name');
            echo $this->Form->control('user_email');
            echo $this->Form->control('time', ['empty' => true]);
            echo $this->Form->control('message_details');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
</div>