<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ContactType Model
 *
 * @property \App\Model\Table\StudentContactTable|\Cake\ORM\Association\BelongsTo $StudentContact
 *
 * @method \App\Model\Entity\ContactType get($primaryKey, $options = [])
 * @method \App\Model\Entity\ContactType newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ContactType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ContactType|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ContactType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ContactType[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ContactType findOrCreate($search, callable $callback = null, $options = [])
 */
class ContactTypeTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('contact_type');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('StudentContact', [
            'foreignKey' => 'student_contact_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['student_contact_id'], 'StudentContact'));

        return $rules;
    }
}
