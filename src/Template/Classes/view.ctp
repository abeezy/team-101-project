<?php
/**
  * @var \App\View\AppView $this
  * @var \Cake\Datasource\EntityInterface $class
  */
?>

<div class="container">

<div class="classes view large-9 medium-8 columns content">
    <h3><?php echo $class->name . ', ' . $class->school->school_name; ?></h3>

    <table class="table table-responsive table-striped bg-white">
      <tr>
        <td class="actions text-right" colspan="2">
          <?= $this->Html->link(__('Edit'), ['action' => 'edit', $class->id], ['class' => 'btn btn-primary']) ?>
          <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $class->id], ['class' => 'btn btn-primary', 'confirm' => __('Are you sure you want to delete # {0}?', $class->id)]) ?>
        </td>
      </tr>
        <tr>
            <th scope="row"><?= __('Class Name') ?></th>
            <td><?= h($class->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Class Term') ?></th>
            <td><?= $this->Number->format($class->class_term) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Class Location') ?></th>
            <td><?= h($class->class_address) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Teacher') ?></th>
            <td><?= $class->has('teacher') ? $this->Html->link($class->teacher->user->first_name . ' ' . $class->teacher->user->last_name, ['controller' => 'Teachers', 'action' => 'view', $class->teacher->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Class Time') ?></th>
            <td><?= $class->class_time->i18nFormat('hh:mm a'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Class Day') ?></th>
            <td><?= h($class->class_day) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Start Date') ?></th>
            <td><?= h($class->start_date->i18nFormat('dd/MM/yyyy')) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('End Date') ?></th>
            <td><?= h($class->end_date->i18nFormat('dd/MM/yyyy')) ?></td>
        </tr>
		    <tr>
            <th scope="row"><?= __('Price per lesson') ?></th>
            <td>$<?= $class->price ?></td>
        </tr>
		<tr>
            <th scope="row"><?= __('Notes') ?></th>
            <td><?= $class->notes ?></td>
        </tr>
    </table>

    <div class="card mb-3">
      <div class="card-header" role="tab" id="headingStudents">
        <div class="row justify-content-between align-items-center">
          <div class="col" data-toggle="collapse" href="#collapseStudents" aria-expanded="false" aria-controls="collapseStudents">
            <h5 class="mb-0">
              Students
            </h5>
          </div>
		  <?php if(count($students) > 0): ?>
			  <div class="col-auto">
				<?php echo $this->Html->link('<i class="fas fa-download"></i> Download CSV', array('controller' => 'students', 'action' => 'studentsByClassCsv', $class->id), array('class' => 'btn btn-primary', 'escape' => false)); ?>
			  </div>
		  <?php endif; ?>
        </div>
      </div>

      <div id="collapseStudents" class="collapse" role="tabpanel" aria-labelledby="headingStudents">
        <table class="table table-responsive table-striped bg-white mb-0">
          <thead>
            <tr>
              <th>Name</th>
              <?php foreach($class->lessons as $lesson): ?>
                <th><?php echo $lesson->lesson_date->i18nFormat('dd/MM'); ?></th>
              <?php endforeach; ?>
            </tr>
          </thead>
          <tbody>
				<?php foreach($students as $student): ?>
					<tr>
						<td><?php echo $this->Html->link($student->full_name, array('controller' => 'students', 'action' => 'view', $student->id)); ?></td>
						<?php foreach($student['enrolments'] as $enrolment): ?>
							<!--<td><pre><?php //print_r($enrolment['lesson_enrolment']); ?></pre></td>-->
							<?php foreach($class->lessons as $lesson): ?>
								<td>
									<?php foreach($enrolment['lesson_enrolment'] as $le): ?>
										<?php if($lesson->lesson_date == $le['lesson']['lesson_date']): ?>
											<?php if($le['attended']): ?>
												Y
											<?php elseif ($le['attended'] !== null):  ?>
												N
											<?php endif; ?>
											<?php break; ?>
										<?php endif; ?>
									<?php endforeach; ?>
								</td>
							<?php endforeach; ?>
						<?php endforeach; ?>
					</tr>
					<tr>
						<th>Notes: </th>
						<td colspan="100">
							<?php echo h($student->notes); ?>
						</td>
					</tr>
				<?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>

   <div class="card mb-3">
      <div class="card-header" role="tab" id="headingLessons">
        <div class="row justify-content-between align-items-center">
          <div class="col" data-toggle="collapse" href="#collapseLessons" aria-expanded="false" aria-controls="collapseLessons">
            <h5 class="mb-0">
              Lessons
            </h5>
          </div>
          <div class="col-auto">
		    <?php echo $this->Html->link('<i class="fas fa-plus"></i>&nbsp;&nbsp;Add Lesson', array('controller'=>'Lessons','action' => 'add',$lesson->id), ['class' => 'btn btn-outline-primary', 'escape' => false]) ?></li>
          </div>
        </div>
      </div>
    <div id="collapseLessons" class="collapse" role="tabpanel" aria-labelledby="headingLessons">
      <?php if (!empty($class->lessons)): ?>
         <table class="table table-responsive table-striped bg-white mb-0">
                <tr>
                    <th scope="col"><?= __('Teacher') ?></th>
                    <th scope="col"><?= __('Lesson Date') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($class->lessons as $lessons): ?>
                <tr>
                    <td><?= h($lessons->teacher->user->first_name . ' ' . $lessons->teacher->user->last_name) ?></td>
                    <td><?= h($lessons->lesson_date->i18nFormat('dd/MM/yyyy')) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['controller' => 'Lessons', 'action' => 'view', $lessons->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['controller' => 'Lessons', 'action' => 'edit', $lessons->id]) ?>
			<?= $this->Form->postLink(__('Delete'), ['action' => 'delete','controller'=>'Lessons', $lessons->id], ['confirm' => __('Are you sure you want to delete the lesson starting on # {0}?', date("d/m/Y",strtotime($lessons->lesson_date)))]) ?>

                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            <?php endif; ?>
    </div>
  </div>
  <?php /*
  <div class="card">
    <div class="card-header" role="tab" id="headingEnrolments">
		<div class="row justify-content-between align-items-center">
          <div class="col" data-toggle="collapse" href="#collapseEnrolments" aria-expanded="false" aria-controls="collapseEnrolments">
            <h5 class="mb-0">
              Enrolments
            </h5>
          </div>
        </div>
    </div>
    <div id="collapseEnrolments" class="collapse" role="tabpanel" aria-labelledby="headingEnrolments">
            <?php if (!empty($class->enrolments)): ?>
            <table class="table table-responsive table-striped bg-white mb-0">
                <tr>
                    <th scope="col"><?= __('Student') ?></th>
                    <th scope="col"><?= __('Enrolment Date') ?></th>
                    <th scope="col"><?= __('No Of Weeks Enrolled') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($class->enrolments as $enrolments): ?>
                <tr>
                    <td><?= h($enrolments->student->first_name . ' ' . $enrolments->student->last_name) ?></td>
                    <td><?= h($enrolments->enrolment_date->i18nFormat('dd/MM/yyyy')) ?></td>
                    <td><?= h($enrolments->no_of_weeks_enrolled) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['controller' => 'Enrolments', 'action' => 'view', $enrolments->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['controller' => 'Enrolments', 'action' => 'edit', $enrolments->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['controller' => 'Enrolments', 'action' => 'delete', $enrolments->id], ['confirm' => __('Are you sure you want to delete # {0}?', $enrolments->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
			<?php else: ?>
				<table class="table table-responsive table-striped bg-white mb-0">
					<tr>
                    <th>There are currently no enrolments for this class.</th>
					</tr>
				</table>
            <?php endif; ?>
    </div>
  </div>*/?>
</div>
</div>

</br>
			<button type="button" class = 'btn btn-light' onclick="goBack()">Go Back</button>

<script>
function goBack() {
    window.history.back();
}
</script>
