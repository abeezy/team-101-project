<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Row[]|\Cake\Collection\CollectionInterface $rows
  */
?>
<div class="container">
	<nav class="large-3 medium-4 columns" id="actions-sidebar">
		<ul class="side-nav">
			<li class="heading"><?= __('Actions') ?></li>
			<li><?= $this->Html->link(__('New Row'), ['action' => 'add']) ?></li>
			<li><?= $this->Html->link(__('List Row Templates'), ['controller' => 'RowTemplates', 'action' => 'index']) ?></li>
			<li><?= $this->Html->link(__('New Row Template'), ['controller' => 'RowTemplates', 'action' => 'add']) ?></li>
			<li><?= $this->Html->link(__('List Webpages'), ['controller' => 'Webpages', 'action' => 'index']) ?></li>
			<li><?= $this->Html->link(__('New Webpage'), ['controller' => 'Webpages', 'action' => 'add']) ?></li>
			<li><?= $this->Html->link(__('List Columns'), ['controller' => 'Columns', 'action' => 'index']) ?></li>
			<li><?= $this->Html->link(__('New Column'), ['controller' => 'Columns', 'action' => 'add']) ?></li>
		</ul>
	</nav>
	<div class="rows index large-9 medium-8 columns content">
		<h3><?= __('Rows') ?></h3>
		<table cellpadding="0" cellspacing="0">
			<thead>
				<tr>
					<th scope="col"><?= $this->Paginator->sort('id') ?></th>
					<th scope="col"><?= $this->Paginator->sort('row_template_id') ?></th>
					<th scope="col"><?= $this->Paginator->sort('webpage_id') ?></th>
					<th scope="col" class="actions"><?= __('Actions') ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($rows as $row): ?>
				<tr>
					<td><?= $this->Number->format($row->id) ?></td>
					<td><?= $row->has('row_template') ? $this->Html->link($row->row_template->name, ['controller' => 'RowTemplates', 'action' => 'view', $row->row_template->id]) : '' ?></td>
					<td><?= $row->has('webpage') ? $this->Html->link($row->webpage->id, ['controller' => 'Webpages', 'action' => 'view', $row->webpage->id]) : '' ?></td>
					<td class="actions">
						<?= $this->Html->link(__('View'), ['action' => 'view', $row->id]) ?>
						<?= $this->Html->link(__('Edit'), ['action' => 'edit', $row->id]) ?>
						<?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $row->id], ['confirm' => __('Are you sure you want to delete # {0}?', $row->id)]) ?>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		<div class="paginator text-center">
			<ul class="pagination justify-content-center">
				<?= $this->Paginator->first('<< ' . __('first')) ?>
				<?= $this->Paginator->prev('<i class="fa fa-chevron-left" aria-hidden="true"></i> ' . __('Previous'), ['escape' => false]) ?>
				<?= $this->Paginator->numbers() ?>
				<?= $this->Paginator->next(__('Next') . ' <i class="fa fa-chevron-right" aria-hidden="true"></i>', ['escape' => false]) ?>
				<?= $this->Paginator->last(__('last') . ' >>') ?>
			</ul>
			<p class="text-muted"><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
		</div>
	</div>
</div>