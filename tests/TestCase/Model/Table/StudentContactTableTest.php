<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StudentContactTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StudentContactTable Test Case
 */
class StudentContactTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\StudentContactTable
     */
    public $StudentContact;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.student_contact',
        'app.students',
        'app.schools',
        'app.classes',
        'app.teachers',
        'app.enrolments',
        'app.payment_history',
        'app.lessons',
        'app.attendences',
        'app.users',
        'app.contact_type'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('StudentContact') ? [] : ['className' => StudentContactTable::class];
        $this->StudentContact = TableRegistry::get('StudentContact', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->StudentContact);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
