<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="container">
<div class="lessons form large-9 medium-8 columns content">
    <?= $this->Form->create($lesson) ?>
    <fieldset>
        <legend><?= __('Edit Lesson') ?></legend>
        <?php
            echo $this->Form->control('class_id', ['options' => $classes]);
            echo $this->Form->control('teacher_id', ['options' => $teachers]);

            echo $this->Form->input('lesson_date', ['class' => 'datepicker-input', 'data-date-start-date' => '-0d', 'type' => 'text', 'format' => 'Y-m-d', 'default' => date('Y-m-d'), 'value' => !empty($lesson->lesson_date) ? $lesson->lesson_date->format('Y-m-d') : date('Y-m-d')]);

			echo $this->Form->control('notes');
        ?>
      
    </fieldset>
   	<?php echo $this->Form->button(__('Save'), array('class' => 'btn btn-light')); ?>
						<button type="button" class = 'btn btn-light' onclick="goBack()">Go Back</button>
                     

<script>
function goBack() {
    window.history.back();
}
</script>

    <?= $this->Form->end() ?>


</div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js" defer="defer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" />

